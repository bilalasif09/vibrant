import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import SelectField from 'material-ui/SelectField';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import AddBtn from 'material-ui/svg-icons/action/done';

const styles = {
  customWidth: {
    width: '100%',
  },
};

// Adduser.defaultProps = {
//   firstname:
// }

class Adduser extends Component {

  constructor(props) {
    super(props);
    this.state = {
      usertoadd: [],
      allmanagers: [],
      value: "",
      type: "",
      manager: "",
      firstname: "",
      lastname: "",
      email: "",
      contact: "",
      username: "",
      password: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleManagerChange = this.handleManagerChange.bind(this)
    console.log("USER DATA:---", JSON.parse(this.props.location.state.user))
  }

  componentDidMount(){
    this.setState({
      usertoadd: JSON.parse(this.props.location.state.user)
    }, () => {
      if (this.state.usertoadd.type == "Representative"){
        this.setState({
          value: 're',
          manager: this.state.usertoadd.manager,
          type: this.state.usertoadd.type,
          managername: this.state.usertoadd.managername,
          firstname: this.state.usertoadd.firstname,
          lastname: this.state.usertoadd.lastname,
          email: this.state.usertoadd.email,
          contact: this.state.usertoadd.contact,
        })
      }else if(this.state.usertoadd.type == "Corporate Employee"){
        this.setState({
          value: 'cp',
          manager: this.state.usertoadd.manager,
          type: this.state.usertoadd.type,
          managername: this.state.usertoadd.managername,
          firstname: this.state.usertoadd.firstname,
          lastname: this.state.usertoadd.lastname,
          email: this.state.usertoadd.email,
          contact: this.state.usertoadd.contact,
        })
      }else{
        this.setState({
          value: 'ma',
          manager: this.state.usertoadd.manager,
          type: this.state.usertoadd.type,
          managername: this.state.usertoadd.managername,
          firstname: this.state.usertoadd.firstname,
          lastname: this.state.usertoadd.lastname,
          email: this.state.usertoadd.email,
          contact: this.state.usertoadd.contact,
        })
      }
      // console.log(this.refs.firstname.getValue())
      // this.refs.firstname.value = "xyz"
      // this.refs.lastname.getInputNode().value = this.state.usertoadd.lastname
      // this.refs.firstname.getInputNode().value = this.state.usertoadd.firstname
      // this.refs.email.getInputNode().value = this.state.usertoadd.email
      // this.refs.contact.getInputNode().value = this.state.usertoadd.contact
      fetch("http://localhost:8008/api/allmanagers", {
      method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      })
      .then(res => res.json())
      .then((result)=>{
        console.log("ALL MANAGERS:----", result[0].users)
        this.setState({
          allmanagers: result[0].users
        }, () => {
          console.log("STATE ALL MANAGER:---", this.state.allmanagers)
        })
      }),
      (error) => {
        console.log(error)
      }

    })
  }

  handleChange(event, index, value){
    event.preventDefault()
    this.setState({
      value, 
      type: event.target.textContent, 
      manager: null, 
      managername: null 
    })
  }

  handleManagerChange(event, index, value){
    event.preventDefault()
    this.setState({
      manager: value, 
      managername: event.target.textContent
    })

  }

  handleInputChange(event){
    let name = event.target.name
    this.setState({
      [name]: event.target.value
    })
  }

  handleSubmit(e){
    e.preventDefault()
    console.log("First Name:---", this.state.firstname, "\n",
                "Last Name:---", this.state.lastname, "\n",
                "User Name:---", this.state.username, "\n",
                "PASSWORD:---", this.state.password, "\n",
                "Type:---", this.state.type, "\n",
                "manager:---", this.state.manager, "\n",
                "manager name:---", this.state.managername, "\n",
                "EMAIL:---", this.state.email, "\n",
                "CONTACT:---", this.state.contact, "\n",
                )
    fetch("http://localhost:8008/api/adduser", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({firstname: this.state.firstname, 
                              lastname: this.state.lastname, 
                              username: this.state.username,
                              password: this.state.password,
                              type: this.state.type, 
                              email: this.state.email,
                              contact: this.state.contact, 
                              role: "user", 
                              managername: this.state.managername, 
                              manager: this.state.manager})
    })
    .then(res => res.json())
    .then((result)=>{
      console.log("RESULT FROM ADD USER:------", result)
      fetch("http://localhost:8008/api/activateregister", {
        method: 'post',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({registerid: this.state.usertoadd._id})
      })
      .then(res => res.json())
      .then((result) => {
        console.log("RESULT FROM ACTIVATE REGISTER:---", result)
      }),(error) => {
        console.log(err)
      }
    }),
    (error) => {
      console.log(error)
    }
  }

  render() {
    
    const { usertoadd } = this.state
    const {  firstname, lastname, email, contact, username, password } = this.props
    let type = null

    // if (this.state.type === 'Representative'){

    //   type = <SelectField
    //     floatingLabelText="Manager"
    //     value={this.state.manager}
    //     onChange={this.handleManagerChange}
    //     style={styles.customWidth}
    //   >
    //     <MenuItem value={null} primaryText="" />
    //     <MenuItem value={2} primaryText="Usman Ali Khan" />
    //     <MenuItem value={3} primaryText="Arslan Ali Khan" />
    //     <MenuItem value={4} primaryText="Hussnain Malik" />
    //   </SelectField>;
    // }else{
    //   type = null
    // }


    return (
      <div>
        <h1>ADD USER</h1>
        <form onSubmit={this.handleSubmit}>
          <SelectField
            floatingLabelText="Register As"
            value={this.state.value}
            onChange={this.handleChange}
            style={styles.customWidth}
          >
            <MenuItem value={null} primaryText="" />
            <MenuItem value={'re'} primaryText="Representative" />
            <MenuItem value={'ma'} primaryText="Manager" />
            <MenuItem value={'cp'} primaryText="Corporate Employee" />
          </SelectField>

          {(this.state.value === 're') ? 
          <SelectField
            floatingLabelText="Manager"
            value={this.state.manager}
            onChange={this.handleManagerChange}
            style={styles.customWidth}
          >
            <MenuItem value={null} primaryText="" />
              {this.state.allmanagers.map((obj, i) => {
                return (<MenuItem key={obj._id} value={obj._id} primaryText={obj.firstname} />)
              })}
          </SelectField> : ""}

          <TextField
            id="username"
            name="username"
            onChange={this.handleInputChange}
            floatingLabelText="Username"
            value={this.state.username}
            style={styles.customWidth}
            required
          />
          <TextField
            id="password"
            name="password"
            type="password"
            onChange={this.handleInputChange}
            floatingLabelText="Password"
            value={this.state.password}
            style={styles.customWidth}
            required
          />
          <TextField
            id="firstname"
            name="firstname"
            onChange={this.handleInputChange}
            floatingLabelText="First Name"
            value={this.state.firstname}
            style={styles.customWidth}
            required
          />

          <TextField
            id="lastname"
            name="lastname"
            onChange={this.handleInputChange}
            floatingLabelText="Last Name"
            value={this.state.lastname}
            style={styles.customWidth}
            required
          />

          <TextField
            id="email"
            name="email"
            type=" email"
            onChange={this.handleInputChange}
            floatingLabelText="Email"
            value={this.state.email}
            style={styles.customWidth}
            required
          />

          <TextField
            id="contact"
            name="contact"
            onChange={this.handleInputChange}
            floatingLabelText="Contact"
            value={this.state.contact}
            style={styles.customWidth}
            required
          />


          <RaisedButton
            label="ADD"
            labelPosition="after"
            style={styles.customWidth}
            type="submit"
            primary={true}
            icon={<AddBtn />}
          />
        </form>
      </div>
    );
  }
}


export default withRouter(Adduser);
