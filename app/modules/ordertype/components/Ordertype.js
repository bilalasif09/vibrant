import React, { Component } from 'react';
import TextField from 'material-ui/TextField'
// import SelectField from 'material-ui/SelectField';
// import MenuItem from 'material-ui/MenuItem';
import AddBtn from 'material-ui/svg-icons/action/done';
import RaisedButton from 'material-ui/RaisedButton';
import { PropTypes } from 'prop-types'


const styles = {
  customWidth: {
    width: '100%',
  },
};


class Ordertype extends Component {

  constructor(props) {
      super(props);
      this.state = {
        name: '',
      }
      this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(event){
    event.preventDefault()
    console.log("NAME ENTERED:----", this.refs.name.getValue())
    fetch("http://localhost:8008/api/addordertype", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({name: this.refs.name.getValue()})
    })
    .then(res => res.json())
    .then((result)=>{
      console.log("RESULT FROM ADDING TYPE:----", result)
      this.refs.name.getInputNode().value = ''
    }),
    (error) => {
      console.log(error)
    }
  }

  render() {
    return (
      <div>
        <h3>Add Order Type</h3>
        <form onSubmit={this.handleSubmit}>
          <TextField
            id="name"
            floatingLabelText="Name"
            defaultValue={name}
            ref="name"
            style={styles.customWidth}
            required
          />
          <RaisedButton
            label="ADD"
            labelPosition="after"
            style={styles.customWidth}
            type="submit"
            primary={true}
            icon={<AddBtn />}
          />
        </form>
      </div>
    );
  }
}


Ordertype.defaultProps = {
  name: '',
}

export default Ordertype;
