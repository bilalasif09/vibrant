import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import AddBtn from 'material-ui/svg-icons/action/done';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
  customWidth: {
    width: '100%',
  },
};


class Register extends Component {

  constructor(props){
    super(props)
    this.state = {
      value: null,
      allmanagers: [],
      type: null,
      managername: null,
      manager: null
    };
    this.handleChange = this.handleChange.bind(this)
    this.handleManagerChange = this.handleManagerChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount(){
    fetch("http://localhost:8008/api/allmanagers", {
    method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(res => res.json())
    .then((result)=>{
      console.log("ALL MANAGERS:----", result[0].users)
      this.setState({
        allmanagers: result[0].users
      }, () => {
        console.log("STATE ALL MANAGER:---", this.state.allmanagers)
      })
    }),
    (error) => {
      console.log(error)
    }
  }

  handleChange(event, index, value){
    this.setState({value, type: event.target.textContent, manager: null, managername: null});
  }

  handleManagerChange(event, index, value){
    this.setState({manager: value, managername: event.target.textContent});
  }





  handleSubmit(event){
    event.preventDefault()
    console.log("First Name: ", this.refs.firstname.getValue())
    console.log("Last Name: ", this.refs.lastname.getValue())
    console.log("Type: ", this.state.type)
    console.log("Manager: ", this.state.manager)
    console.log("Manager Name: ", this.state.managername)
    console.log("Email: ", this.refs.email.getValue())
    console.log("Contact: ", this.refs.contact.getValue())
    if (this.refs.busname){
      console.log("BUSINESS NAME: ", this.refs.busname.getValue())
    }
    fetch("http://localhost:8008/api/addregister", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({firstname: this.refs.firstname.getValue(),
                              lastname: this.refs.lastname.getValue(),
                              email: this.refs.email.getValue(),
                              type: this.state.type,
                              manager: this.state.manager,
                              managername: this.state.managername,
                              busname: this.state.busname,
                              contact: this.refs.contact.getValue()})
    })
    .then(res => res.json())
    .then((result)=>{
      if (result.length != 0){
        console.log(result)
        this.refs.firstname.getInputNode().value = null
        this.refs.lastname.getInputNode().value = null
        this.refs.email.getInputNode().value = null
        this.refs.contact.getInputNode().value = null
        this.setState({value: null})
        this.setState({manager: null})
        this.setState({type: null})
        this.setState({managername: null})
      }
    }),
    (error) => {
      console.log(error)
    }

  }

  render() {

    let type = null;

    const { firstname, lastname, email, contact, manager, busname } = this.props

    // if (this.state.type === 'Representative'){

    //   type = <SelectField
    //     floatingLabelText="Manager"
    //     value={this.state.manager}
    //     onChange={this.handleManagerChange}
    //     style={styles.customWidth}
    //   >
    //     <MenuItem value={null} primaryText="" />
    //   {this.state.allmanagers.map((obj, i) => {
    //           return (<MenuItem key={i} value={obj._id} primaryText={obj.firstname} />)
    //         })}

    //   </SelectField>;
    // }else{
    //   type = null
    // }

    return (

      <div className="loginform">
        <form onSubmit={this.handleSubmit}>
          <SelectField
            floatingLabelText="Register As"
            value={this.state.value}
            onChange={this.handleChange}
            style={styles.customWidth}
          >
            <MenuItem value={null} primaryText="" />
            <MenuItem value={'re'} primaryText="Representative" />
            <MenuItem value={'ma'} primaryText="Manager" />
            <MenuItem value={'cp'} primaryText="Corporate Employee" />
          </SelectField>

          {(this.state.value === 're') ?
          <SelectField
            floatingLabelText="Manager"
            value={this.state.manager}
            onChange={this.handleManagerChange}
            style={styles.customWidth}
          >
            <MenuItem value={null} primaryText="" />
              {this.state.allmanagers.map((obj, i) => {
                return (<MenuItem key={obj._id} value={obj._id} primaryText={obj.firstname} />)
              })}
          </SelectField> : ""}

          <TextField
            id="firstname"
            floatingLabelText="First Name"
            defaultValue={firstname}
            ref="firstname"
            style={styles.customWidth}
            required
          />
          <TextField
            id="lastname"
            floatingLabelText="Last Name"
            defaultValue={lastname}
            ref="lastname"
            style={styles.customWidth}
            required
          />

          <TextField
            id="email"
            floatingLabelText="Email"
            defaultValue={email}
            ref="email"
            style={styles.customWidth}
            required
          />

          {(this.state.value === 'cp') ?
          <TextField
            id="busname"
            floatingLabelText="Business Name"
            defaultValue={busname}
            ref="busname"
            style={styles.customWidth}
            required
          /> : ""}


          <TextField
            id="contact"
            floatingLabelText="Contact"
            defaultValue={contact}
            ref="contact"
            style={styles.customWidth}
            required
          />



          <RaisedButton
            label="ADD"
            labelPosition="after"
            style={styles.customWidth}
            type="submit"
            primary={true}
            icon={<AddBtn />}
          />
        </form>
      </div>
    );
  }
}


Register.defaultProps = {
  firstname: "",
  lastname: "",
  type: "",
  email: "",
  contact: "",
  busname: "",
  manager: ""
}


export default Register;
