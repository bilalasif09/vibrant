import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom'
import {AppTemplate} from '../../../commons/ui/components/AppTemplate';
import Link from 'react-router-dom';

import RaisedButton from 'material-ui/RaisedButton';
import IconButton from '@material-ui/core/IconButton';

import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Icon from '@material-ui/core/Icon';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import * as _ from 'lodash';

// PLAY ICONs
import MdTv from 'react-icons/lib/md/tv'
import MdPhoneAndroid from 'react-icons/lib/md/phone-android'
import MdWifi from 'react-icons/lib/md/wifi'
import Badge from '@material-ui/core/Badge';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Divider from '@material-ui/core/Divider';

import TrippleIcon from 'react-icons/lib/md/filter-3'
import DoubleIcon from 'react-icons/lib/md/filter-2'
import SingleIcon from 'react-icons/lib/md/filter-1'
import AllPlays from 'react-icons/lib/md/video-collection'

import CancelIcon from 'react-icons/lib/md/close';
import InstallIcon from 'react-icons/lib/md/check';
import PendingIcon from 'react-icons/lib/md/info';
import LogOutIcon from 'react-icons/lib/fa/sign-out';
import PaperWork from 'react-icons/lib/ti/business-card';

import IoEject from "react-icons/lib/io/eject"
import Tooltip from '@material-ui/core/Tooltip';

import moment from 'moment'


const styles = theme => ({
  root: {
    // flexGrow: 1,
    width: '100%',
  },
  // paper: {
  //   padding: theme.spacing.unit * 2,
  //   textAlign: 'center',
  //   color: theme.palette.text.secondary,
  // },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
    marginRight: 15,
  },
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    gridGap: `${theme.spacing.unit * 3}px`,
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    whiteSpace: 'nowrap',
    marginBottom: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
          loggedInUser: "",
          expandedrep: null,
          expandedteam: null,
          topreps: [],
          topteams: [],
          selectedrep: [],
          reptripple: 0,
          repdoubletvint: 0,
          repdoubletvphone: 0,
          repdoubleintphone: 0,
          repsingletv: 0,
          repsinglephone: 0,
          repsingleint: 0,
          teamstats: [],
          teamtripple: 0,
          teamdoubletvint: 0,
          teamdoubletvphone: 0,
          teamdoubleintphone: 0,
          teamsingletv: 0,
          teamsinglephone: 0,
          teamsingleint: 0,

          trippleplays: 0,
          doubleplays: 0,
          tvintplays: 0,
          intphoneplays: 0,
          tvphoneplays: 0,
          singleplays: 0,
          tvplays: 0,
          phoneplays: 0,
          intplays: 0,

          ordersstatus: [],
          cancel: 0,
          installed: 0,
          pending: 0,
          chargedback: 0,

          trippleinstall: 0,
          tripplechargeback: 0,
          tvintinstall: 0,
          tvintchargeback: 0,
          tvphoneinstall: 0,
          tvphonechargeback: 0,
          intphoneinstall: 0,
          intphonechargeback: 0,
          tvinstall: 0,
          tvchargeback: 0,
          phoneinstall: 0,
          phonechargeback: 0,
          intinstall: 0,
          intchargeback: 0,

          trippleComission: 0,
          tvInternetComission: 0,
          tvPhoneComission: 0,
          internetPhoneComission: 0,
          internetComission: 0,
          phoneComission: 0,
          tvComission: 0,
        }
        this.handlelogout = this.handlelogout.bind(this)
        this.gotohireform = this.gotohireform.bind(this)
        this.gotoorderentry = this.gotoorderentry.bind(this)
        // console.log(this.props.location.state.detail[0])
        // console.log(this.props.location.state.hello)
    }

    componentDidMount(){
      console.log("WEEK AND MONTH:==", new Date().getMonth(), new Date(), moment('2018-01-01', 'YYYY-MM-DD').format('w'))
      fetch("http://localhost:8008/api/user", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({userId: localStorage.getItem('userId')})
      })
      .then(res => res.json())
      .then((result)=>{
        console.log("RESULT FROM SINGLE USER:***********", result)
        this.setState({loggedInUser: result[0].user[0]}, () => {
          console.log("SETTING STATE:***********", this.state.loggedInUser)
          fetch("http://localhost:8008/api/toprepsorderswise", {
            method: 'GET',
          })
          .then(res => res.json())
          .then((result)=>{
            let topreps = result[0].topreps
            console.log("TOP REPS RESULT:***********", result[0].topreps)
              fetch("http://localhost:8008/api/topteamsorderswise", {
                method: 'GET',
              })
              .then(res => res.json())
              .then((result)=>{
                let topteams = result[0].topteams
                console.log("TOP TEAMS RESULT:***********", result[0].topteams)
                  // fetch("http://localhost:8008/api/orderstats", {
                  //   method: 'GET',
                  // })
                  // .then(res => res.json())
                  // .then((result)=>{
                  //   var objstats = {
                  //     trippleplays: 0,
                  //     doubleplays: 0,
                  //     singleplays: 0,
                  //   }
                  //     if (result[0].stats[0].tripple.length != 0){
                  //       objstats.trippleplays = result[0].stats[0].tripple[0].count
                  //     }
                  //     if (result[0].stats[0].double.length != 0){
                  //       objstats.doubleplays = result[0].stats[0].double[0].count
                  //     }
                  //     if (result[0].stats[0].single.length != 0){
                  //       objstats.singleplays = result[0].stats[0].single[0].count
                  //     }
                      fetch("http://localhost:8008/api/ordersstatus", {
                        method: 'GET',
                      })
                      .then(res => res.json())
                      .then((result)=>{
                        var objstatus = {
                          cancel: 0,
                          pending: 0,
                          installed: 0,
                          chargedback: 0,
                        }
                        if (result[0].stats[0].cancel.length != 0){
                          objstatus.cancel = result[0].stats[0].cancel[0].count
                        }
                        if (result[0].stats[0].installed.length != 0){
                          objstatus.installed = result[0].stats[0].installed[0].count
                        }
                        if (result[0].stats[0].pending.length != 0){
                          objstatus.pending = result[0].stats[0].pending[0].count
                        }
                        if (result[0].stats[0].chargedback.length != 0){
                          objstatus.chargedback = result[0].stats[0].chargedback[0].count
                        }
                        this.setState({
                          ordersstatus: result[0].stats[0],
                          topreps: topreps,
                          topteams: topteams,
                          cancel: objstatus.cancel,
                          installed: objstatus.installed,
                          chargedback: objstatus.chargedback,
                          pending: objstatus.pending,
                          // trippleplays: objstats.trippleplays,
                          // doubleplays: objstats.doubleplays,
                          // singleplays: objstats.singleplays,
                        }, () => {
                          console.log("******INSTALLED >><< CANCEL >><< PENDING:*****", this.state.ordersstatus,
                                      "'\nOBJECT CREATED FROM CANCEL INSTALL PENDING:---", objstatus,
                                      // "'\nTRIPPLE DOUBLE SINGLE STATS:---", objstats,
                                      )
                          this.calcComission()

                        })
                      }),
                      (error) => {
                        console.log(error)
                      }
                  // }),
                  // (error) => {
                  //   console.log(error)
                  // }
              }),
              (error) => {
                console.log(error)
              }
          }),
          (error) => {
            console.log(error)
          }
        })
      }),
      (error) => {
        console.log(error)
      }
    }

    calcComission(){
      fetch("http://localhost:8008/api/tripplecomission", {
        method: 'GET',
      })
      .then(res => res.json())
      .then((result)=>{
        var tripplecomission = {
          trippleinstall: 0,
          tripplechargeback: 0,
          tripplecancel: 0,
        }
        if (result[0].stats[0].trippleinstall.length){
          tripplecomission.trippleinstall = result[0].stats[0].trippleinstall[0].count
        }
        if (result[0].stats[0].tripplechargeback.length){
          tripplecomission.tripplechargeback = result[0].stats[0].tripplechargeback[0].count
        }
        if (result[0].stats[0].tripplecancel.length){
          tripplecomission.tripplecancel = result[0].stats[0].tripplecancel[0].count
        }

        console.log("TRIPPLE COMISSION RESULTS:--", tripplecomission)

        fetch("http://localhost:8008/api/doublecomission", {
          method: 'GET',
        })
        .then(res => res.json())
        .then((result)=>{
          console.log("DOUBLE COMISSION RESULTS:--", result[0].stats[0])
          var doublecomission = {
            tvintinstall: 0,
            tvintchargeback: 0,
            tvintcancel: 0,
            tvphoneinstall: 0,
            tvphonechargeback: 0,
            tvphonecancel: 0,
            intphoneinstall: 0,
            intphonechargeback: 0,
            intphonecancel: 0,

          }
          // TV INTERNET
          if (result[0].stats[0].tvintinstall.length != 0){
            doublecomission.tvintinstall = result[0].stats[0].tvintinstall[0].count
          }
          if (result[0].stats[0].tvintchargeback.length != 0){
            doublecomission.tvintchargeback = result[0].stats[0].tvintchargeback[0].count
          }
          if (result[0].stats[0].tvintcancel.length != 0){
            doublecomission.tvintcancel = result[0].stats[0].tvintcancel[0].count
          }
          // TV PHONE
          if (result[0].stats[0].tvphoneinstall.length != 0){
            doublecomission.tvphoneinstall = result[0].stats[0].tvphoneinstall[0].count
          }
          if (result[0].stats[0].tvphonechargeback.length != 0){
            doublecomission.tvphonechargeback = result[0].stats[0].tvphonechargeback[0].count
          }
          if (result[0].stats[0].tvphonecancel.length != 0){
            doublecomission.tvphonecancel = result[0].stats[0].tvphonecancel[0].count
          }
          // INTERNET PHONE
          if (result[0].stats[0].intphoneinstall.length != 0){
            // console.log("YEHOO IN INT OHINE")
            doublecomission.intphoneinstall = result[0].stats[0].intphoneinstall[0].count
          }else{
            console.log("YEHOO IN INT OHINE")
            doublecomission.intphoneinstall = 0
          }
          if (result[0].stats[0].intphonechargeback.length){
            doublecomission.intphonechargeback = result[0].stats[0].intphonechargeback[0].count
          }
          if (result[0].stats[0].intphonecancel.length != 0){
            doublecomission.intphonecancel = result[0].stats[0].intphonecancel[0].count
          }

          console.log("DOUBLE COMISSION RESULTS:--", doublecomission)

          fetch("http://localhost:8008/api/singlecomission", {
            method: 'GET',
          })
          .then(res => res.json())
          .then((result)=>{
            // console.log("SINGLE COMISSION RESULTS:--", result[0].stats[0])
            var singlecomission = {
              tvinstall: 0,
              tvchargeback: 0,
              tvcancel: 0,
              phoneinstall: 0,
              phonechargeback: 0,
              phonecancel: 0,
              intinstall: 0,
              intchargeback: 0,
              intcancel: 0,
            }
            // TV
            if (result[0].stats[0].tvinstall.length){
              singlecomission.tvinstall = result[0].stats[0].tvinstall[0].count
            }
            if (result[0].stats[0].tvchargeback.length){
              singlecomission.tvchargeback = result[0].stats[0].tvchargeback[0].count
            }
            if (result[0].stats[0].tvcancel.length){
              singlecomission.tvcancel = result[0].stats[0].tvcancel[0].count
            }
            // PHONE
            if (result[0].stats[0].phoneinstall.length){
              singlecomission.phoneinstall = result[0].stats[0].phoneinstall[0].count
            }
            if (result[0].stats[0].phonechargeback.length){
              singlecomission.phonechargeback = result[0].stats[0].phonechargeback[0].count
            }
            if (result[0].stats[0].phonecancel.length){
              singlecomission.phonecancel = result[0].stats[0].phonecancel[0].count
            }
            // INTERNET
            if (result[0].stats[0].intinstall.length){
              singlecomission.intinstall = result[0].stats[0].intinstall[0].count
            }
            if (result[0].stats[0].intchargeback.length){
              singlecomission.intchargeback = result[0].stats[0].intchargeback[0].count
            }
            if (result[0].stats[0].intcancel.length){
              singlecomission.intcancel = result[0].stats[0].intcancel[0].count
            }

            console.log("SINGLE COMISSION RESULTS:--", singlecomission)

            if (tripplecomission.trippleinstall == 0 && tripplecomission.tripplechargeback != 0){
              tripplecomission.trippleinstall = tripplecomission.tripplechargeback
              // tripplecomission.tripplecancel = tripplecomission.tripplecancel - tripplecomission.trippleinstall
            }
            if (doublecomission.tvintinstall == 0 && doublecomission.tvintchargeback != 0){
              doublecomission.tvintinstall = doublecomission.tvintchargeback
              // doublecomission.tvintcancel = doublecomission.tvintcancel - doublecomission.tvintinstall
            }
            if (doublecomission.tvphoneinstall == 0 && doublecomission.tvphonechargeback != 0){
              doublecomission.tvphoneinstall = doublecomission.tvphonechargeback
              // doublecomission.tvphonecancel = doublecomission.tvphonecancel - doublecomission.tvphoneinstall
            }
            if (doublecomission.intphoneinstall == 0 && doublecomission.intphonechargeback != 0){
              doublecomission.intphoneinstall = doublecomission.intphonechargeback
              // doublecomission.intphonecancel = doublecomission.intphonecancel - doublecomission.intphoneinstall
            }
            if (singlecomission.intinstall == 0 && singlecomission.intchargeback != 0){
              singlecomission.intinstall = singlecomission.intchargeback
              // singlecomission.intcancel = singlecomission.intcancel - singlecomission.intinstall
            }
            if (singlecomission.phoneinstall == 0 && singlecomission.phonechargeback != 0){
              singlecomission.phoneinstall = singlecomission.phonechargeback
              // singlecomission.phonecancel = singlecomission.phonecancel - singlecomission.phoneinstall
            }
            if (singlecomission.tvinstall == 0 && singlecomission.tvchargeback != 0){
              singlecomission.tvinstall = singlecomission.tvchargeback
            }


            this.setState({
              trippleinstall: tripplecomission.trippleinstall,
              tripplechargeback: tripplecomission.tripplechargeback,
              trippleplays: tripplecomission.tripplecancel,

              tvintinstall: doublecomission.tvintinstall,
              tvintchargeback: doublecomission.tvintchargeback,
              tvintplays: doublecomission.tvintcancel,

              tvphoneinstall: doublecomission.tvphoneinstall,
              tvphonechargeback: doublecomission.tvphonechargeback,
              tvphoneplays: doublecomission.tvphonecancel,

              intphoneinstall: doublecomission.intphoneinstall,
              intphonechargeback: doublecomission.intphonechargeback,
              intphoneplays: doublecomission.intphonecancel,

              doubleplays: doublecomission.tvintcancel + doublecomission.tvphonecancel + doublecomission.intphonecancel,

              tvinstall: singlecomission.tvinstall,
              tvchargeback: singlecomission.tvchargeback,
              tvplays: singlecomission.tvcancel,

              phoneinstall: singlecomission.phoneinstall,
              phonechargeback: singlecomission.phonechargeback,
              phoneplays: singlecomission.phonecancel,

              intinstall: singlecomission.intinstall,
              intchargeback: singlecomission.intchargeback,
              intplays: singlecomission.intcancel,

              singleplays: singlecomission.tvcancel + singlecomission.phonecancel + singlecomission.intcancel,

            }, () => {

              let trippleComission = 0
              let tvInternetComission = 0
              let tvPhoneComission = 0
              let internetPhoneComission = 0
              let tvComission = 0
              let internetComission = 0
              let phoneComission = 0

              if (this.state.trippleinstall > 0 && this.state.trippleinstall <= 19){
                if (this.state.tripplechargeback){
                  if (this.state.tripplechargeback > 0 && this.state.tripplechargeback <=19){
                    trippleComission = (100 * this.state.trippleinstall) - 0.15*100
                  }
                }else{
                  trippleComission = (100 * this.state.trippleinstall)
                }
              }else if(this.state.trippleinstall >= 20 && this.state.trippleinstall <= 99){
                if (this.state.tripplechargeback){
                  if (this.state.tripplechargeback > 0 && this.state.tripplechargeback <=19){
                    trippleComission = (190 * this.state.trippleinstall) - 0.15*100
                  }else if (this.state.tripplechargeback >=20 && this.state.tripplechargeback <=99){
                    trippleComission = (190 * this.state.trippleinstall) - 0.15*190
                  }
                }else{
                  trippleComission = (190 * this.state.trippleinstall)
                }
              }else if(this.state.trippleinstall >= 100 && this.state.trippleinstall <= 299){
                if (this.state.tripplechargeback){
                  if (this.state.tripplechargeback > 0 && this.state.tripplechargeback <=19){
                    trippleComission = (220 * this.state.trippleinstall) - 0.15*100
                  }else if (this.state.tripplechargeback >=20 && this.state.tripplechargeback <=99){
                    trippleComission = (220 * this.state.trippleinstall) - 0.15*190
                  }else if (this.state.tripplechargeback >=100 && this.state.tripplechargeback <=299){
                    trippleComission = (220 * this.state.trippleinstall) - 0.15*220
                  }
                }else{
                  trippleComission = (220 * this.state.trippleinstall)
                }
              }else if(this.state.trippleinstall >= 300 && this.state.trippleinstall <= 999){
                if (this.state.tripplechargeback){
                  if (this.state.tripplechargeback > 0 && this.state.tripplechargeback <=19){
                    trippleComission = (250 * this.state.trippleinstall) - 0.15*100
                  }else if (this.state.tripplechargeback >=20 && this.state.tripplechargeback <=99){
                    trippleComission = (250 * this.state.trippleinstall) - 0.15*190
                  }else if (this.state.tripplechargeback >=100 && this.state.tripplechargeback <=299){
                    trippleComission = (250 * this.state.trippleinstall) - 0.15*220
                  }else if (this.state.tripplechargeback >=300 && this.state.tripplechargeback <=999){
                    trippleComission = (250 * this.state.trippleinstall) - 0.15*250
                  }
                }else{
                  trippleComission = (250 * this.state.trippleinstall)
                }
              }else if(this.state.trippleinstall >= 1000 && this.state.trippleinstall <= 2499){
                if (this.state.tripplechargeback){
                  if (this.state.tripplechargeback > 0 && this.state.tripplechargeback <=19){
                    trippleComission = (275 * this.state.trippleinstall) - 0.15*100
                  }else if (this.state.tripplechargeback >=20 && this.state.tripplechargeback <=99){
                    trippleComission = (275 * this.state.trippleinstall) - 0.15*190
                  }else if (this.state.tripplechargeback >=100 && this.state.tripplechargeback <=299){
                    trippleComission = (275 * this.state.trippleinstall) - 0.15*220
                  }else if (this.state.tripplechargeback >=300 && this.state.tripplechargeback <=999){
                    trippleComission = (275 * this.state.trippleinstall) - 0.15*250
                  }else if (this.state.tripplechargeback >= 1000 && this.state.tripplechargeback <= 2499){
                    trippleComission = (275 * this.state.trippleCount) - 0.15*275
                  }
                }else{
                  trippleComission = (275 * this.state.trippleinstall)
                }
              }else if(this.state.trippleinstall >= 2500){
                if (this.state.tripplechargeback){
                  if (this.state.tripplechargeback > 0 && this.state.tripplechargeback <=19){
                    trippleComission = (300 * this.state.trippleinstall) - 0.15*100
                  }else if (this.state.tripplechargeback >=20 && this.state.tripplechargeback <=99){
                    trippleComission = (300 * this.state.trippleinstall) - 0.15*190
                  }else if (this.state.tripplechargeback >=100 && this.state.tripplechargeback <=299){
                    trippleComission = (300 * this.state.trippleinstall) - 0.15*220
                  }else if (this.state.tripplechargeback >=300 && this.state.tripplechargeback <=999){
                    trippleComission = (300 * this.state.trippleinstall) - 0.15*250
                  }else if (this.state.tripplechargeback >= 1000 && this.state.tripplechargeback <= 2499){
                    trippleComission = (300 * this.state.trippleinstall) - 0.15*275
                  }else if (this.state.tripplechargeback >= 2500){
                    trippleComission = (300 * this.state.trippleinstall) - 0.15*300
                  }
                }else{
                  trippleComission = (300 * this.state.trippleinstall)
                }
              }
              console.log("TRIPPLE COMISSION:--", trippleComission)
              // INTERNET TV COMISSION
              if (this.state.tvintinstall > 0 && this.state.tvintinstall <= 19){
                if (this.state.tvintchargeback){
                  if (this.state.tvintchargeback > 0 && this.state.tvintchargeback <= 19){
                    tvInternetComission = 60 * this.state.tvintinstall - 0.15*60
                  }
                }else{
                  tvInternetComission = 60 * this.state.tvintinstall
                }
              }else if(this.state.tvintinstall >= 20 && this.state.tvintinstall <= 99){
                if (this.state.tvintchargeback){
                  if (this.state.tvintchargeback > 0 && this.state.tvintchargeback <= 19){
                    tvInternetComission = 110 * this.state.tvintinstall - 0.15*60
                  }else if (this.state.tvintchargeback >= 20 && this.state.tvintchargeback <= 99){
                    tvInternetComission = 110 * this.state.tvintinstall - 0.15*110
                  }
                }else{
                  tvInternetComission = 110 * this.state.tvintinstall
                }
              }else if(this.state.tvintinstall >= 100 && this.state.tvintinstall <= 299){
                // CHARGE BACK
                if (this.state.tvintchargeback){
                  if (this.state.tvintchargeback > 0 && this.state.tvintchargeback <= 19){
                    tvInternetComission = 130 * this.state.tvintinstall - 0.15*60
                  }else if (this.state.tvintchargeback >= 20 && this.state.tvintchargeback <= 99){
                    tvInternetComission = 130 * this.state.tvintinstall - 0.15*110
                  }else if (this.state.tvintchargeback >= 100 && this.state.tvintchargeback <= 299){
                    tvInternetComission = 130 * this.state.tvintinstall - 0.15*130
                  }
                }else{
                  // NO CHARGE BACK
                  tvInternetComission = 130 * this.state.tvintinstall
                }
              }else if(this.state.tvintinstall >= 300 && this.state.tvintinstall <= 999){
                if (this.state.tvintchargeback){
                  if (this.state.tvintchargeback > 0 && this.state.tvintchargeback <= 19){
                    tvInternetComission = 150 * this.state.tvintinstall - 0.15*60
                  }else if (this.state.tvintchargeback >= 20 && this.state.tvintchargeback <= 99){
                    tvInternetComission = 150 * this.state.tvintinstall - 0.15*110
                  }else if (this.state.tvintchargeback >= 100 && this.state.tvintchargeback <= 299){
                    tvInternetComission = 150 * this.state.tvintinstall - 0.15*130
                  }else if (this.state.tvintchargeback >= 300 && this.state.tvintchargeback <= 999){
                    tvInternetComission = 150 * this.state.tvintinstall - 0.15*150
                  }
                }else{
                  // NO CHARGE BACK
                  tvInternetComission = 150 * this.state.tvintinstall
                }
              }else if(this.state.tvintinstall >= 1000 && this.state.tvintinstall <= 2499){
                if (this.state.tvintchargeback){
                  if (this.state.tvintchargeback > 0 && this.state.tvintchargeback <= 19){
                    tvInternetComission = 160 * this.state.tvintinstall - 0.15*60
                  }else if (this.state.tvintchargeback >= 20 && this.state.tvintchargeback <= 99){
                    tvInternetComission = 160 * this.state.tvintinstall - 0.15*110
                  }else if (this.state.tvintchargeback >= 100 && this.state.tvintchargeback <= 299){
                    tvInternetComission = 160 * this.state.tvintinstall - 0.15*130
                  }else if (this.state.tvintchargeback >= 300 && this.state.tvintchargeback <= 999){
                    tvInternetComission = 160 * this.state.tvintinstall - 0.15*150
                  }else if (this.state.tvintchargeback >= 1000 && this.state.tvintchargeback <= 2499){
                    tvInternetComission = 160 * this.state.tvintinstall - 0.15*160
                  }
                }else{
                  // NO CHARGE BACK
                  tvInternetComission = 160 * this.state.tvintinstall
                }
              }else if(this.state.tvintinstall >= 2500){
                if (this.state.tvintchargeback){
                  if (this.state.tvintchargeback > 0 && this.state.tvintchargeback <= 19){
                    tvInternetComission = 170 * this.state.tvintinstall - 0.15*60
                  }else if (this.state.tvintchargeback >= 20 && this.state.tvintchargeback <= 99){
                    tvInternetComission = 170 * this.state.tvintinstall - 0.15*110
                  }else if (this.state.tvintchargeback >= 100 && this.state.tvintchargeback <= 299){
                    tvInternetComission = 170 * this.state.tvintinstall - 0.15*130
                  }else if (this.state.tvintchargeback >= 300 && this.state.tvintchargeback <= 999){
                    tvInternetComission = 170 * this.state.tvintinstall - 0.15*150
                  }else if (this.state.tvintchargeback >= 1000 && this.state.tvintchargeback <= 2499){
                    tvInternetComission = 170 * this.state.tvintinstall - 0.15*160
                  }else if (this.state.tvintchargeback >= 2500){
                    tvInternetComission = 170 * this.state.tvintinstall - 0.15*170
                  }
                }else{
                  // NO CHARGE BACK
                  tvInternetComission = 170 * this.state.tvintinstall
                }
              }
              console.log("TV INTERNET COMISSION:--", tvInternetComission)
              // TV PHONE COMISSION
              if (this.state.tvphoneinstall > 0 && this.state.tvphoneinstall <= 19){
                if (this.state.tvphonechargeback){
                  if (this.state.tvphonechargeback > 0 && this.state.tvphonechargeback <= 19){
                    tvPhoneComission = 60 * this.state.tvphoneinstall - 0.15*60
                  }
                }else{
                  tvPhoneComission = 60 * this.state.tvphoneinstall
                }
              }else if(this.state.tvphoneinstall >= 20 && this.state.tvphoneinstall <= 99){
                if (this.state.tvphonechargeback){
                  if (this.state.tvphonechargeback > 0 && this.state.tvphonechargeback <= 19){
                    tvPhoneComission = 110 * this.state.tvphoneinstall - 0.15*60
                  }else if (this.state.tvphonechargeback >= 20 && this.state.tvphonechargeback <= 99){
                    tvPhoneComission = 110 * this.state.tvphoneinstall - 0.15*110
                  }
                }else{
                  tvPhoneComission = 110 * this.state.tvphoneinstall
                }
              }else if(this.state.tvphoneinstall >= 100 && this.state.tvphoneinstall <= 299){
                // CHARGE BACK
                if (this.state.tvphonechargeback){
                  if (this.state.tvphonechargeback > 0 && this.state.tvphonechargeback <= 19){
                    tvPhoneComission = 130 * this.state.tvphoneinstall - 0.15*60
                  }else if (this.state.tvphonechargeback >= 20 && this.state.tvphonechargeback <= 99){
                    tvPhoneComission = 130 * this.state.tvphoneinstall - 0.15*110
                  }else if (this.state.tvphonechargeback >= 100 && this.state.tvphonechargeback <= 299){
                    tvPhoneComission = 130 * this.state.tvphoneinstall - 0.15*130
                  }
                }else{
                  // NO CHARGE BACK
                  tvPhoneComission = 130 * this.state.tvphoneinstall
                }
              }else if(this.state.tvphoneinstall >= 300 && this.state.tvphoneinstall <= 999){
                if (this.state.tvphonechargeback){
                  if (this.state.tvphonechargeback > 0 && this.state.tvphonechargeback <= 19){
                    tvPhoneComission = 150 * this.state.tvphoneinstall - 0.15*60
                  }else if (this.state.tvphonechargeback >= 20 && this.state.tvphonechargeback <= 99){
                    tvPhoneComission = 150 * this.state.tvphoneinstall - 0.15*110
                  }else if (this.state.tvphonechargeback >= 100 && this.state.tvphonechargeback <= 299){
                    tvPhoneComission = 150 * this.state.tvphoneinstall - 0.15*130
                  }else if (this.state.tvphonechargeback >= 300 && this.state.tvphonechargeback <= 999){
                    tvPhoneComission = 150 * this.state.tvphoneinstall - 0.15*150
                  }
                }else{
                  // NO CHARGE BACK
                  tvPhoneComission = 150 * this.state.tvphoneinstall
                }
              }else if(this.state.tvphoneinstall >= 1000 && this.state.tvphoneinstall <= 2499){
                if (this.state.tvphonechargeback){
                  if (this.state.tvphonechargeback > 0 && this.state.tvphonechargeback <= 19){
                    tvPhoneComission = 160 * this.state.tvphoneinstall - 0.15*60
                  }else if (this.state.tvphonechargeback >= 20 && this.state.tvphonechargeback <= 99){
                    tvPhoneComission = 160 * this.state.tvphoneinstall - 0.15*110
                  }else if (this.state.tvphonechargeback >= 100 && this.state.tvphonechargeback <= 299){
                    tvPhoneComission = 160 * this.state.tvphoneinstall - 0.15*130
                  }else if (this.state.tvphonechargeback >= 300 && this.state.tvphonechargeback <= 999){
                    tvPhoneComission = 160 * this.state.tvphoneinstall - 0.15*150
                  }else if (this.state.tvphonechargeback >= 1000 && this.state.tvphonechargeback <= 2499){
                    tvPhoneComission = 160 * this.state.tvphoneinstall - 0.15*160
                  }
                }else{
                  // NO CHARGE BACK
                  tvPhoneComission = 160 * this.state.tvphoneinstall
                }
              }else if(this.state.tvphoneinstall >= 2500){
                if (this.state.tvphonechargeback){
                  if (this.state.tvphonechargeback > 0 && this.state.tvphonechargeback <= 19){
                    tvPhoneComission = 170 * this.state.tvphoneinstall - 0.15*60
                  }else if (this.state.tvphonechargeback >= 20 && this.state.tvphonechargeback <= 99){
                    tvPhoneComission = 170 * this.state.tvphoneinstall - 0.15*110
                  }else if (this.state.tvphonechargeback >= 100 && this.state.tvphonechargeback <= 299){
                    tvPhoneComission = 170 * this.state.tvphoneinstall - 0.15*130
                  }else if (this.state.tvphonechargeback >= 300 && this.state.tvphonechargeback <= 999){
                    tvPhoneComission = 170 * this.state.tvphoneinstall - 0.15*150
                  }else if (this.state.tvphonechargeback >= 1000 && this.state.tvphonechargeback <= 2499){
                    tvPhoneComission = 170 * this.state.tvphoneinstall - 0.15*160
                  }else if (this.state.tvphonechargeback >= 2500){
                    tvPhoneComission = 170 * this.state.tvphoneinstall - 0.15*170
                  }
                }else{
                  // NO CHARGE BACK
                  tvPhoneComission = 170 * this.state.tvphoneinstall
                }
              }
              console.log("TV PHONE COMISSION:--", tvPhoneComission)
              if (this.state.intphoneinstall >= 0 && this.state.intphoneinstall <= 19){
                if (this.state.intphonechargeback){
                  if (this.state.intphonechargeback > 0 && this.state.intphonechargeback <= 19){
                    internetPhoneComission = 45 * this.state.intphoneinstall - 0.15*45
                  }
                }else{
                  internetPhoneComission = 45 * this.state.intphoneinstall
                }
              }else if(this.state.intphoneinstall >= 20 && this.state.intphoneinstall <= 99){
                if (this.state.intphonechargeback){
                  if (this.state.intphonechargeback > 0 && this.state.intphonechargeback <= 19){
                    internetPhoneComission = 85 * this.state.intphoneinstall - 0.15*45
                  }else if (this.state.intphonechargeback >= 20 && this.state.intphonechargeback <= 99){
                    internetPhoneComission = 85 * this.state.intphoneinstall - 0.15*85
                  }
                }else{
                  internetPhoneComission = 85 * this.state.intphoneinstall
                }
              }else if(this.state.intphoneinstall >= 100 && this.state.intphoneinstall <= 299){
                if (this.state.intphonechargeback){
                  if (this.state.intphonechargeback > 0 && this.state.intphonechargeback <= 19){
                    internetPhoneComission = 100 * this.state.intphoneinstall - 0.15*45
                  }else if (this.state.intphonechargeback >= 20 && this.state.intphonechargeback <= 99){
                    internetPhoneComission = 100 * this.state.intphoneinstall - 0.15*85
                  }else if (this.state.intphonechargeback >= 100 && this.state.intphonechargeback <= 299){
                    internetPhoneComission = 100 * this.state.intphoneinstall - 0.15*100
                  }

                }else{
                  internetPhoneComission = 100 * this.state.intphoneinstall
                }
              }else if(this.state.intphoneinstall >= 300 && this.state.intphoneinstall <= 999){
                if (this.state.intphonechargeback){
                  if (this.state.intphonechargeback > 0 && this.state.intphonechargeback <= 19){
                    internetPhoneComission = 115 * this.state.intphoneinstall - 0.15*45
                  }else if (this.state.intphonechargeback >= 20 && this.state.intphonechargeback <= 99){
                    internetPhoneComission = 115 * this.state.intphoneinstall - 0.15*85
                  }else if (this.state.intphonechargeback >= 100 && this.state.intphonechargeback <= 299){
                    internetPhoneComission = 115 * this.state.intphoneinstall - 0.15*100
                  }else if (this.state.intphonechargeback >= 300 && this.state.intphonechargeback <= 999){
                    internetPhoneComission = 115 * this.state.intphoneinstall - 0.15*115
                  }
                }else{
                  internetPhoneComission = 115 * this.state.intphoneinstall
                }
              }else if(this.state.intphoneinstall >= 1000 && this.state.intphoneinstall <= 2499){
                if (this.state.intphonechargeback){
                  if (this.state.intphonechargeback > 0 && this.state.intphonechargeback <= 19){
                    internetPhoneComission = 125 * this.state.intphoneinstall - 0.15*45
                  }else if (this.state.intphonechargeback >= 20 && this.state.intphonechargeback <= 99){
                    internetPhoneComission = 125 * this.state.intphoneinstall - 0.15*85
                  }else if (this.state.intphonechargeback >= 100 && this.state.intphonechargeback <= 299){
                    internetPhoneComission = 125 * this.state.intphoneinstall - 0.15*100
                  }else if (this.state.intphonechargeback >= 300 && this.state.intphonechargeback <= 999){
                    internetPhoneComission = 125 * this.state.intphoneinstall - 0.15*115
                  }else if (this.state.intphonechargeback >= 1000 && this.state.intphonechargeback <= 2499){
                    internetPhoneComission = 125 * this.state.intphoneinstall - 0.15*125
                  }
                }else{
                  internetPhoneComission = 125 * this.state.intphoneinstall
                }
              }else if(this.state.intphoneinstall >= 2500){
                if (this.state.intphonechargeback){
                  if (this.state.intphonechargeback > 0 && this.state.intphonechargeback <= 19){
                    internetPhoneComission = 135 * this.state.intphoneinstall - 0.15*45
                  }else if (this.state.intphonechargeback >= 20 && this.state.intphonechargeback <= 99){
                    internetPhoneComission = 135 * this.state.intphoneinstall - 0.15*85
                  }else if (this.state.intphonechargeback >= 100 && this.state.intphonechargeback <= 299){
                    internetPhoneComission = 135 * this.state.intphoneinstall - 0.15*100
                  }else if (this.state.intphonechargeback >= 300 && this.state.intphonechargeback <= 999){
                    internetPhoneComission = 135 * this.state.intphoneinstall - 0.15*115
                  }else if (this.state.intphonechargeback >= 1000 && this.state.intphonechargeback <= 2499){
                    internetPhoneComission = 135 * this.state.intphoneinstall - 0.15*125
                  }else if (this.state.intphonechargeback >= 2500){
                    internetPhoneComission = 135 * this.state.intphoneinstall - 0.15*135
                  }
                }else{
                  internetPhoneComission = 135 * this.state.intphoneinstall
                }
              }
              console.log("INTERNET PHONE COMISSION:---", internetPhoneComission)
              if (this.state.tvinstall > 0 && this.state.tvinstall <= 19){
                if (this.state.tvchargeback){
                  if (this.state.tvchargeback > 0 && this.state.tvchargeback <= 19){
                    tvComission = 35 * this.state.tvinstall - 0.15*35
                  }
                }else{
                  tvComission = 35 * this.state.tvinstall
                }
              }else if(this.state.tvinstall >= 20 && this.state.tvinstall <= 99){
                if (this.state.tvchargeback){
                  if (this.state.tvchargeback > 0 && this.state.tvchargeback <= 19){
                    tvComission = 60 * this.state.tvinstall - 0.15*35
                  }else if (this.state.tvchargeback >= 20 && this.state.tvchargeback <= 99){
                    tvComission = 60 * this.state.tvinstall - 0.15*60
                  }
                }else{
                  tvComission = 60 * this.state.tvinstall
                }
              }else if(this.state.tvinstall >= 100 && this.state.tvinstall <= 299){
                if (this.state.tvchargeback){
                  if (this.state.tvchargeback > 0 && this.state.tvchargeback <= 19){
                    tvComission = 65 * this.state.tvinstall - 0.15*35
                  }else if (this.state.tvchargeback >= 20 && this.state.tvchargeback <= 99){
                    tvComission = 65 * this.state.tvinstall - 0.15*60
                  }else if (this.state.tvchargeback >= 100 && this.state.tvchargeback <= 299){
                    tvComission = 65 * this.state.tvinstall - 0.15*65
                  }
                }else{
                  tvComission = 65 * this.state.tvinstall
                }
              }else if(this.state.tvinstall >= 300 && this.state.tvinstall <= 999){
                if (this.state.tvchargeback){
                  if (this.state.tvchargeback > 0 && this.state.tvchargeback <= 19){
                    tvComission = 70 * this.state.tvinstall - 0.15*35
                  }else if (this.state.tvchargeback >= 20 && this.state.tvchargeback <= 99){
                    tvComission = 70 * this.state.tvinstall - 0.15*60
                  }else if (this.state.tvchargeback >= 100 && this.state.tvchargeback <= 299){
                    tvComission = 70 * this.state.tvinstall - 0.15*65
                  }else if (this.state.tvchargeback >= 300 && this.state.tvchargeback <= 999){
                    tvComission = 70 * this.state.tvinstall - 0.15*70
                  }

                }else{
                  tvComission = 70 * this.state.tvinstall
                }
              }else if(this.state.tvinstall >= 1000 && this.state.tvinstall <= 2499){
                if (this.state.tvchargeback){
                  if (this.state.tvchargeback > 0 && this.state.tvchargeback <= 19){
                    tvComission = 80 * this.state.tvinstall - 0.15*35
                  }else if (this.state.tvchargeback >= 20 && this.state.tvchargeback <= 99){
                    tvComission = 80 * this.state.tvinstall - 0.15*60
                  }else if (this.state.tvchargeback >= 100 && this.state.tvchargeback <= 299){
                    tvComission = 80 * this.state.tvinstall - 0.15*65
                  }else if (this.state.tvchargeback >= 300 && this.state.tvchargeback <= 999){
                    tvComission = 80 * this.state.tvinstall - 0.15*70
                  }else if (this.state.tvchargeback >= 1000 && this.state.tvchargeback <= 2499){
                    tvComission = 80 * this.state.tvinstall - 0.15*80
                  }
                }else{
                  tvComission = 80 * this.state.tvinstall
                }
              }else if(this.state.tvinstall >= 2500){
                if (this.state.tvchargeback){
                  if (this.state.tvchargeback > 0 && this.state.tvchargeback <= 19){
                    tvComission = 90 * this.state.tvinstall - 0.15*35
                  }else if (this.state.tvchargeback >= 20 && this.state.tvchargeback <= 99){
                    tvComission = 90 * this.state.tvinstall - 0.15*60
                  }else if (this.state.tvchargeback >= 100 && this.state.tvchargeback <= 299){
                    tvComission = 90 * this.state.tvinstall - 0.15*65
                  }else if (this.state.tvchargeback >= 300 && this.state.tvchargeback <= 999){
                    tvComission = 90 * this.state.tvinstall - 0.15*70
                  }else if (this.state.tvchargeback >= 1000 && this.state.tvchargeback <= 2499){
                    tvComission = 90 * this.state.tvinstall - 0.15*80
                  }else if (this.state.tvchargeback >= 2500){
                    tvComission = 90 * this.state.tvinstall - 0.15*90
                  }
                }else{
                  tvComission = 90 * this.state.tvinstall
                }
              }

              if (this.state.phoneinstall > 0 && this.state.phoneinstall <= 19){
                if (this.state.phonechargeback){
                  if (this.state.phonechargeback > 0 && this.state.phonechargeback <= 19){
                    phoneComission = 25 * this.state.phoneinstall - 0.15*25
                  }
                }else{
                  phoneComission = 25 * this.state.phoneinstall
                }
              }else if(this.state.phoneinstall >= 20 && this.state.phoneinstall <= 99){
                if (this.state.phonechargeback){
                  if (this.state.phonechargeback > 0 && this.state.phonechargeback <= 19){
                    phoneComission = 50 * this.state.phoneinstall - 0.15*25
                  }else if (this.state.phonechargeback >= 20 && this.state.phonechargeback <= 99){
                    phoneComission = 50 * this.state.phoneinstall - 0.15*50
                  }
                }else{
                  phoneComission = 50 * this.state.phoneinstall
                }
              }else if(this.state.phoneinstall >= 100 && this.state.phoneinstall <= 299){
                if (this.state.phonechargeback){
                  if (this.state.phonechargeback > 0 && this.state.phonechargeback <= 19){
                    phoneComission = 55 * this.state.phoneinstall - 0.15*25
                  }else if (this.state.phonechargeback >= 20 && this.state.phonechargeback <= 99){
                    phoneComission = 55 * this.state.phoneinstall - 0.15*50
                  }else if (this.state.phonechargeback >= 100 && this.state.phonechargeback <= 299){
                    phoneComission = 55 * this.state.phoneinstall - 0.15*55
                  }
                }else{
                  phoneComission = 55 * this.state.phoneinstall
                }
              }else if(this.state.phoneinstall >= 300 && this.state.phoneinstall <= 999){
                if (this.state.phonechargeback){
                  if (this.state.phonechargeback > 0 && this.state.phonechargeback <= 19){
                    phoneComission = 60 * this.state.phoneinstall - 0.15*25
                  }else if (this.state.phonechargeback >= 20 && this.state.phonechargeback <= 99){
                    phoneComission = 60 * this.state.phoneinstall - 0.15*50
                  }else if (this.state.phonechargeback >= 100 && this.state.phonechargeback <= 299){
                    phoneComission = 60 * this.state.phoneinstall - 0.15*55
                  }else if (this.state.phonechargeback >= 300 && this.state.phonechargeback <= 999){
                    phoneComission = 60 * this.state.phoneinstall - 0.15*60
                  }
                }else{
                  phoneComission = 60 * this.state.phoneinstall
                }
              }else if(this.state.phoneinstall >= 1000 && this.state.phoneinstall <= 2499){
                if (this.state.phonechargeback){
                  if (this.state.phonechargeback > 0 && this.state.phonechargeback <= 19){
                    phoneComission = 70 * this.state.phoneinstall - 0.15*25
                  }else if (this.state.phonechargeback >= 20 && this.state.phonechargeback <= 99){
                    phoneComission = 70 * this.state.phoneinstall - 0.15*50
                  }else if (this.state.phonechargeback >= 100 && this.state.phonechargeback <= 299){
                    phoneComission = 70 * this.state.phoneinstall - 0.15*55
                  }else if (this.state.phonechargeback >= 300 && this.state.phonechargeback <= 999){
                    phoneComission = 70 * this.state.phoneinstall - 0.15*60
                  }else if (this.state.phonechargeback >= 1000 && this.state.phonechargeback <= 2499){
                    phoneComission = 70 * this.state.phoneinstall - 0.15*70
                  }
                }else{
                  phoneComission = 70 * this.state.phoneinstall
                }
              }else if(this.state.phoneinstall >= 2500){
                if (this.state.phonechargeback){
                  if (this.state.phonechargeback > 0 && this.state.phonechargeback <= 19){
                    phoneComission = 80 * this.state.phoneinstall - 0.15*25
                  }else if (this.state.phonechargeback >= 20 && this.state.phonechargeback <= 99){
                    phoneComission = 80 * this.state.phoneinstall - 0.15*50
                  }else if (this.state.phonechargeback >= 100 && this.state.phonechargeback <= 299){
                    phoneComission = 80 * this.state.phoneinstall - 0.15*55
                  }else if (this.state.phonechargeback >= 300 && this.state.phonechargeback <= 999){
                    phoneComission = 80 * this.state.phoneinstall - 0.15*60
                  }else if (this.state.phonechargeback >= 1000 && this.state.phonechargeback <= 2499){
                    phoneComission = 80 * this.state.phoneinstall - 0.15*70
                  }else if (this.state.phonechargeback >= 2500){
                    phoneComission = 80 * this.state.phoneinstall - 0.15*80
                  }
                }else{
                  phoneComission = 80 * this.state.phoneinstall
                }
              }

              if (this.state.intinstall > 0 && this.state.intinstall <= 19){
                if (this.state.intchargeback){
                  if (this.state.intchargeback > 0 && this.state.intchargeback <= 19){
                    internetComission = 25 * this.state.intinstall - 0.15*25
                  }
                }else{
                  internetComission = 25 * this.state.intinstall
                }
              }else if(this.state.intinstall >= 20 && this.state.intinstall <= 99){
                if (this.state.intchargeback){
                  if (this.state.intchargeback > 0 && this.state.intchargeback <= 19){
                    internetComission = 50 * this.state.intinstall - 0.15*25
                  }else if (this.state.intchargeback >= 20 && this.state.intchargeback <= 99){
                    internetComission = 50 * this.state.intinstall - 0.15*50
                  }
                }else{
                  internetComission = 50 * this.state.intinstall
                }
              }else if(this.state.intinstall >= 100 && this.state.intinstall <= 299){
                if (this.state.intchargeback){
                  if (this.state.intchargeback > 0 && this.state.intchargeback <= 19){
                    internetComission = 55 * this.state.intinstall - 0.15*25
                  }else if (this.state.intchargeback >= 20 && this.state.intchargeback <= 99){
                    internetComission = 55 * this.state.intinstall - 0.15*50
                  }else if (this.state.intchargeback >= 100 && this.state.intchargeback <= 299){
                    internetComission = 55 * this.state.intinstall - 0.15*55
                  }
                }else{
                  internetComission = 55 * this.state.intinstall
                }
              }else if(this.state.intinstall >= 300 && this.state.intinstall <= 999){
                if (this.state.intchargeback){
                  if (this.state.intchargeback > 0 && this.state.intchargeback <= 19){
                    internetComission = 60 * this.state.intinstall - 0.15*25
                  }else if (this.state.intchargeback >= 20 && this.state.intchargeback <= 99){
                    internetComission = 60 * this.state.intinstall - 0.15*50
                  }else if (this.state.intchargeback >= 100 && this.state.intchargeback <= 299){
                    internetComission = 60 * this.state.intinstall - 0.15*55
                  }else if (this.state.intchargeback >= 300 && this.state.intchargeback <= 999){
                    internetComission = 60 * this.state.intinstall - 0.15*60
                  }
                }else{
                  internetComission = 60 * this.state.intinstall
                }
              }else if(this.state.intinstall >= 1000 && this.state.intinstall <= 2499){
                if (this.state.intchargeback){
                  if (this.state.intchargeback > 0 && this.state.intchargeback <= 19){
                    internetComission = 70 * this.state.intinstall - 0.15*25
                  }else if (this.state.intchargeback >= 20 && this.state.intchargeback <= 99){
                    internetComission = 70 * this.state.intinstall - 0.15*50
                  }else if (this.state.intchargeback >= 100 && this.state.intchargeback <= 299){
                    internetComission = 70 * this.state.intinstall - 0.15*55
                  }else if (this.state.intchargeback >= 300 && this.state.intchargeback <= 999){
                    internetComission = 70 * this.state.intinstall - 0.15*60
                  }else if (this.state.intchargeback >= 1000 && this.state.intchargeback <= 2499){
                    internetComission = 70 * this.state.intinstall - 0.15*70
                  }
                }else{
                  internetComission = 70 * this.state.intinstall
                }
              }else if(this.state.intinstall >= 2500){
                if (this.state.intchargeback){
                  if (this.state.intchargeback > 0 && this.state.intchargeback <= 19){
                    internetComission = 80 * this.state.intinstall - 0.15*25
                  }else if (this.state.intchargeback >= 20 && this.state.intchargeback <= 99){
                    internetComission = 80 * this.state.intinstall - 0.15*50
                  }else if (this.state.intchargeback >= 100 && this.state.intchargeback <= 299){
                    internetComission = 80 * this.state.intinstall - 0.15*55
                  }else if (this.state.intchargeback >= 300 && this.state.intchargeback <= 999){
                    internetComission = 80 * this.state.intinstall - 0.15*60
                  }else if (this.state.intchargeback >= 1000 && this.state.intchargeback <= 2499){
                    internetComission = 80 * this.state.intinstall - 0.15*70
                  }else if (this.state.intchargeback >= 2500){
                    internetComission = 80 * this.state.intinstall - 0.15*80
                  }
                }else{
                  internetComission = 80 * this.state.intinstall
                }
              }
              this.setState({
                trippleComission: trippleComission,
                tvInternetComission: tvInternetComission,
                tvPhoneComission: tvPhoneComission,
                internetPhoneComission: internetPhoneComission,
                internetComission: internetComission,
                phoneComission: phoneComission,
                tvComission: tvComission,
              })
            })
          }),
          (error) => {
            console.log(error)
          }

        }),
        (error) => {
          console.log(error)
        }

      }),
      (error) => {
        console.log(error)
      }
    }

    handlePanelChange = panel => (event, expanded) => {
      this.setState({
        expandedrep: expanded ? panel : false,
      }, ()=>{
        if (this.state.expandedrep){
          let selectedrep = _.filter(this.state.topreps, {_id:this.state.expandedrep})
          let reptripple = 0
          let repdoubletvint = 0
          let repdoubletvphone = 0
          let repdoubleintphone = 0
          let repsingletv = 0
          let repsinglephone = 0
          let repsingleint = 0

          console.log("EXPANDED TAB REP:-----", this.state.expandedrep, selectedrep[0])

          for (let obj of selectedrep[0].orders){
            if (obj.noofunits == 3){
              reptripple++
            }
            if (obj.noofunits == 2){
              if (obj.tv == true && obj.internet == true)
                repdoubletvint++
              if (obj.tv == true && obj.phone == true)
                repdoubletvphone++
              if (obj.internet == true && obj.phone == true)
                repdoubleintphone++
            }
            if (obj.noofunits == 1){
              if (obj.tv == true)
                repsingletv++
              if (obj.phone == true)
                repsinglephone++
              if (obj.internet)
                repsingleint++
            }
          }
          this.setState({
            reptripple: reptripple,
            repdoubleintphone: repdoubleintphone,
            repdoubletvint: repdoubletvint,
            repdoubletvphone: repdoubletvphone,
            repsingletv: repsingletv,
            repsingleint: repsingleint,
            repsinglephone: repsinglephone,
          })
        }
      });
    };

    handleTeamPanelChange = panel => (event, expanded) => {
      this.setState({
        expandedteam: expanded ? panel : false,
      }, ()=>{
        console.log("EXPANDED TAB TEAM:-----", this.state.expandedteam)
        if (this.state.expandedteam){

          fetch("http://localhost:8008/api/dashboardteamdetail", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({managerid: this.state.expandedteam})
          })
          .then(res => res.json())
          .then((result)=>{
            // console.log("TEAM STATS:***********", result[0].stats[0])
            var obj = {
              teamtripple: "",
              teamdoubletvint: "",
              teamdoubletvphone: "",
              teamdoubleintphone: "",
              teamsingletv: "",
              teamsingleint: "",
              teamsinglephone: "",
            }
            if (result[0].stats.length != 0){
              if (result[0].stats[0].tripple.length != 0){
                obj.teamtripple = result[0].stats[0].tripple[0].count
              }else{
                obj.teamtripple = 0
              }
              if (result[0].stats[0].doubletvint.length != 0){
                obj.teamdoubletvint = result[0].stats[0].doubletvint[0].count
              }else{
                obj.teamdoubletvint = 0
              }
              if (result[0].stats[0].doubleintphone.length != 0){
                obj.teamdoubleintphone = result[0].stats[0].doubleintphone[0].count
              }else{
                obj.teamdoubleintphone = 0
              }
              if (result[0].stats[0].doubletvphone.length != 0){
                obj.teamdoubletvphone = result[0].stats[0].doubletvphone[0].count
              }else{
                obj.teamdoubletvphone = 0
              }
              if (result[0].stats[0].singletv.length != 0){
                obj.teamsingletv = result[0].stats[0].singletv[0].count
              }else{
                obj.teamsingletv = 0
              }
              if (result[0].stats[0].singlephone.length != 0){
                obj.teamsinglephone = result[0].stats[0].singlephone[0].count
              }else{
                obj.teamsinglephone = 0
              }
              if (result[0].stats[0].singleint.length != 0){
                obj.teamsingleint = result[0].stats[0].singleint[0].count
              }else{
                obj.teamsingleint = 0
              }
            }
            // console.log("OBJECT CREATED FROM SERVER RESULT:***********", obj)
            this.setState({
              // teamstats: result[0].stats[0],
              teamtripple: obj.teamtripple,
              teamdoubletvint: obj.teamdoubletvint,
              teamdoubletvphone: obj.teamdoubletvphone,
              teamdoubleintphone: obj.teamdoubleintphone,
              teamsingletv: obj.teamsingletv,
              teamsingleint: obj.teamsingleint,
              teamsinglephone: obj.teamsinglephone,
            }, () => {
              // console.log("TEAM STATS:***********", this.state.teamstats)
            })
          }),
          (error) => {
            console.log(error)
          }
        }
      });
    };


    gotohireform(){
      this.props.history.push({
        pathname: '/newhire'
      })
    }

    gotoorderentry(){
      this.props.history.push({
        pathname: '/orderentry'
      })
    }



    handlelogout(){
        localStorage.removeItem('userId')
        localStorage.removeItem('navbar')
        localStorage.removeItem('loggeduser')
        this.props.changeDetect();
        this.props.history.push('/login')
        console.log("UserID", localStorage.getItem('userId'))
    }



    render() {
      const { classes } = this.props;
      const { expandedrep, expandedteam, topreps, topteams, ordersstatus } = this.state;
      // const contracted = this.props.location.state.detail[0].contracted
      let contractBtn = null
      let orderBtn = null
      // let contracted = null
      // console.log("CONTRACTED VALUE:---", contracted)
      {this.state.loggedInUser.contracted ?
        (contractBtn = <IconButton disabled={true}
                          variant="fab"
                          color="primary"
                          aria-label="Hire Form"
                          className={classes.button}>
                <PaperWork />
              </IconButton>) :
        (contractBtn = <IconButton onClick={this.gotohireform}
                          variant="fab"
                          color="primary"
                          aria-label="Hire Form"
                          className={classes.button}>
                <PaperWork />
              </IconButton>)
      }
      {this.state.loggedInUser.contracted ?
        (orderBtn = <IconButton onClick={this.gotoorderentry}
                          variant="fab"
                          color="primary"
                          aria-label="Add Order"
                          className={classes.button}>
                <AddIcon />
              </IconButton>) :
        (orderBtn = <IconButton disabled={true}
                          variant="fab"
                          color="primary"
                          aria-label="Add Order"
                          className={classes.button}>
                <AddIcon />
              </IconButton>)
      }

      return (
        <div>
          <Grid container spacing={8}>
            <Grid item xs={6} sm={9}>
              <h1>Welcome {this.state.loggedInUser.firstname}</h1>
            </Grid>
            <Grid item xs={6} sm={3}>
              {this.state.loggedInUser.role === 'user' ? orderBtn : ''}
              {this.state.loggedInUser.role === 'user' ? contractBtn : ''}
              <IconButton onClick={this.handlelogout}
                          variant="fab"
                          color="secondary"
                          aria-label="Log Out"
                          className={classes.button}>
                <LogOutIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Divider />

          <h4>Comission Details</h4>
          <Divider style={{marginTop: 15, marginBottom: 15,}} />
          <Grid container spacing={8}>
            <Grid item xs={6} sm={4}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="Tripple Comission"
                >
                  <TrippleIcon size={35} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} />
                </Tooltip>
                <span style={{display:'block', textAlign: 'center'}}><strong>$ {this.state.trippleComission}</strong></span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={4}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="Double Comission"
                ><DoubleIcon size={35} color="#303F9F" style={{display:'block', margin:'15 auto'}}/></Tooltip>
                <span style={{display:'block', textAlign: 'center'}}>
                  <strong>$ {this.state.tvInternetComission + this.state.tvPhoneComission + this.state.internetPhoneComission}</strong>
                </span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={4}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="Single Comission"
                ><SingleIcon size={35} color="#303F9F" style={{display:'block', margin:'15 auto'}}/></Tooltip>
                <span style={{display:'block', textAlign: 'center'}}><strong>$ {this.state.internetComission + this.state.tvComission + this.state.phoneComission}</strong></span>
              </Paper>
            </Grid>
          </Grid>
          <Grid container spacing={8}>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="Cancel Sales"
                ><CancelIcon size={35} color="#D50000" style={{display:'block', margin:'15 auto'}}/></Tooltip>
                <span style={{display:'block', textAlign: 'center'}}>{this.state.cancel}</span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="Installed Sales"
                ><InstallIcon size={35} color="#33691E" style={{display:'block', margin:'15 auto'}}/></Tooltip>
                <span style={{display:'block', textAlign: 'center'}}>{this.state.installed}</span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="Pending Sales"
                ><PendingIcon size={35} color="#ffea00" style={{display:'block', margin:'15 auto'}}/></Tooltip>
                <span style={{display:'block', textAlign: 'center'}}>{this.state.pending}</span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="Chargeback Sales"
                ><IoEject size={35} color="#f44336" style={{display:'block', margin:'15 auto'}}/></Tooltip>
                <span style={{display:'block', textAlign: 'center'}}>{this.state.chargedback}</span>
              </Paper>
            </Grid>

          </Grid>

          <h4>Order Details</h4>
          <Divider style={{marginTop: 15, marginBottom: 15,}} />
          <Grid container spacing={8}>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="left-end"
                  title="Tripple Sales"
                ><MdTv size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} /></Tooltip>
                <MdPhoneAndroid size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} />
                <MdWifi size={30} color="#303F9F"  style={{display:'inline-block', margin:'15 auto'}} />
                <span style={{display:'block',}}><strong>{this.state.trippleplays}</strong></span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="left"
                  title="TV, PHONE"
                ><MdTv size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} /></Tooltip>
                <MdPhoneAndroid size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} />
                <span style={{display:'block',}}><strong>{this.state.tvphoneplays}</strong></span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="left"
                  title="TV, HSI"
                ><MdTv size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} /></Tooltip>
                <MdWifi size={30} color="#303F9F"  style={{display:'inline-block', margin:'15 auto'}} />
                <span style={{display:'block',}}><strong>{this.state.tvintplays}</strong></span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="left"
                  title="PHONE, HSI"
                ><MdPhoneAndroid size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} /></Tooltip>
                <MdWifi size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} />
                <span style={{display:'block',}}><strong>{this.state.intphoneplays}</strong></span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="left"
                  title="HSI"
                ><MdWifi size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} /></Tooltip>
                <span style={{display:'block',}}><strong>{this.state.intplays}</strong></span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="left"
                  title="PHONE"
                ><MdPhoneAndroid size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} /></Tooltip>
                <span style={{display:'block',}}><strong>{this.state.phoneplays}</strong></span>
              </Paper>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="TV"
                ><MdTv size={30} color="#303F9F" style={{display:'inline-block', margin:'15 auto'}} /></Tooltip>
                <span style={{display:'block',}}><strong>{this.state.tvplays}</strong></span>
              </Paper>
            </Grid>


            <Grid item xs={6} sm={3}>
              <Paper className={classes.paper}>
                <Tooltip
                  enterDelay={100}
                  id="tooltip-controlled"
                  leaveDelay={100}
                  placement="right"
                  title="All Sales"
                ><AllPlays size={30} color="#303F9F" style={{display:'block', margin:'15 auto'}}/></Tooltip>
                <span style={{display:'block', textAlign: 'center'}}><strong>{this.state.singleplays + this.state.doubleplays + this.state.trippleplays}</strong></span>
              </Paper>
            </Grid>

          </Grid>


          <Divider style={{marginTop: 15, marginBottom: 15,}}/>
          <Grid container spacing={8}>
            <Grid item xs={12} sm={12}>
              <Paper className={classes.paper}>
                <h4>Top Reps</h4>
                <Divider />
                {topreps.map((n, i)=> {
                  return  <ExpansionPanel key={n._id} expanded={expandedrep === n._id} onChange={this.handlePanelChange(n._id)}>
                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                              <Typography className={classes.heading}>{n.username.toUpperCase()}</Typography>
                              <Typography className={classes.secondaryHeading}>{"Sales:\t"+n.orderscount}</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                              <Table className={classes.table}>
                                <TableHead>
                                  <TableRow>
                                    <TableCell>Plays Info.</TableCell>
                                    <TableCell numeric>#</TableCell>
                                  </TableRow>
                                </TableHead>
                                <TableBody>
                                  <TableRow>
                                    <TableCell component="th" scope="row"><MdTv /><MdPhoneAndroid /><MdWifi /></TableCell>
                                    <TableCell numeric>{this.state.reptripple}</TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell component="th" scope="row"><MdTv /><MdWifi /></TableCell>
                                    <TableCell numeric>{this.state.repdoubletvint}</TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell component="th" scope="row"><MdPhoneAndroid /><MdWifi /></TableCell>
                                    <TableCell numeric>{this.state.repdoubleintphone}</TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell component="th" scope="row"><MdTv /><MdPhoneAndroid /></TableCell>
                                    <TableCell numeric>{this.state.repdoubletvphone}</TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell component="th" scope="row"><MdTv /></TableCell>
                                    <TableCell numeric>{this.state.repsingletv}</TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell component="th" scope="row"><MdPhoneAndroid /></TableCell>
                                    <TableCell numeric>{this.state.repsinglephone}</TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell component="th" scope="row"><MdWifi /></TableCell>
                                    <TableCell numeric>{this.state.repsingleint}</TableCell>
                                  </TableRow>

                                </TableBody>
                              </Table>
                            </ExpansionPanelDetails>
                          </ExpansionPanel>})}</Paper>
            </Grid>
            <Grid item xs={12} sm={12}>
              <Paper className={classes.paper}>
              <h4>Top Teams</h4>
              <Divider />
              {topteams.map((n, i)=> {
                return  <ExpansionPanel key={n._id} expanded={expandedteam === n._id} onChange={this.handleTeamPanelChange(n._id)}>
                          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.heading}>{n.teamname[0].toUpperCase()}</Typography>
                            <Typography className={classes.secondaryHeading}>{"Sales:\t"+n.totalorders}</Typography>
                            <Typography className={classes.secondaryHeading}>{"Team Reps:\t"+n.count}</Typography>
                          </ExpansionPanelSummary>
                          <ExpansionPanelDetails>
                            <Table className={classes.table}>
                              <TableHead>
                                <TableRow>
                                  <TableCell>Plays Info.</TableCell>
                                  <TableCell numeric>#</TableCell>
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                <TableRow>
                                  <TableCell component="th" scope="row"><MdTv /><MdPhoneAndroid /><MdWifi /></TableCell>
                                  <TableCell numeric>{this.state.teamtripple}</TableCell>
                                </TableRow>
                                <TableRow>
                                  <TableCell component="th" scope="row"><MdTv /><MdWifi /></TableCell>
                                  <TableCell numeric>{this.state.teamdoubletvint}</TableCell>
                                </TableRow>
                                <TableRow>
                                  <TableCell component="th" scope="row"><MdPhoneAndroid /><MdWifi /></TableCell>
                                  <TableCell numeric>{this.state.teamdoubleintphone}</TableCell>
                                </TableRow>
                                <TableRow>
                                  <TableCell component="th" scope="row"><MdTv /><MdPhoneAndroid /></TableCell>
                                  <TableCell numeric>{this.state.teamdoubletvphone}</TableCell>
                                </TableRow>
                                <TableRow>
                                  <TableCell component="th" scope="row"><MdTv /></TableCell>
                                  <TableCell numeric>{this.state.teamsingletv}</TableCell>
                                </TableRow>
                                <TableRow>
                                  <TableCell component="th" scope="row"><MdPhoneAndroid /></TableCell>
                                  <TableCell numeric>{this.state.teamsinglephone}</TableCell>
                                </TableRow>
                                <TableRow>
                                  <TableCell component="th" scope="row"><MdWifi /></TableCell>
                                  <TableCell numeric>{this.state.teamsingleint}</TableCell>
                                </TableRow>

                              </TableBody>
                            </Table>
                          </ExpansionPanelDetails>
                        </ExpansionPanel>})}</Paper>
            </Grid>
          </Grid>
        </div>
      );
    }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(Dashboard));
