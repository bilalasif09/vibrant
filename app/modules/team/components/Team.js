// import React, { Component } from 'react';
// import PropTypes from 'prop-types';
// import {
//   TableHeader,
//   TableHeaderColumn,
//   TableRowColumn,
// } from 'material-ui/Table';

// import Table from '@material-ui/core/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableRow from '@material-ui/core/TableRow';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';

import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from 'react-icons/lib/md/keyboard-arrow-down';

import CkeckIcon from 'react-icons/lib/md/check'
import DatePicker from 'material-ui/DatePicker';

import Button from '@material-ui/core/Button';
import ClearAll from 'react-icons/lib/md/clear-all';

import CancelIcon from 'react-icons/lib/fa/close'; 
import InstallIcon from 'react-icons/lib/fa/check'; 
import PendingIcon from 'react-icons/lib/fa/exclamation-circle'; 

import Tooltip from '@material-ui/core/Tooltip';
import Divider from '@material-ui/core/Divider';


import TrippleIcon from 'react-icons/lib/md/filter-3'
import DoubleIcon from 'react-icons/lib/md/filter-2'
import SingleIcon from 'react-icons/lib/md/filter-1'
import MoneyIcon from 'react-icons/lib/md/attach-money'
import IoEject from "react-icons/lib/io/eject"

const styles = {
  root: {
    width: '100%',
    marginTop: 16,
    overflowX: 'auto',
  },
  table: {
    minWidth: '100%',
    overflowX: 'visible'
  },
  select: {
    minWidth: '33%',
  },

  button: {
    margin: 14,
  },
  input: {
    display: 'none',
  },
  greenStyle: {
    backgroundColor: 'red'
  }
}

// import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };

  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  TablePaginationActions,
);


class Team extends Component {

    constructor(props) {
      super(props);
      this.state = {
        team: [],
        expanded: null,
        trippleCount: 0,
        doubleCuont: 0,
        singleCount: 0,
        controlledDate: null,
        trippeleComission: null,
        tvInternetComission: null,
        tvPhoneComission: null,
        internetPhoneComission: null,
        internetComission: null,
        tvComission: null,
        phoneComission: null,
        querycomission: null,
        page: 0,
        rowsPerPage: 5,
      }
      this.loggeduser = JSON.parse(localStorage.getItem('loggeduser'))
      this.handleChange = this.handleChange.bind(this)
      this.handleDateChange = this.handleDateChange.bind(this)
      this.clearFilter = this.clearFilter.bind(this)
    }

    handleChangePage = (event, page) => {
      this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
      this.setState({ rowsPerPage: event.target.value });
    };

    componentWillMount(){
      this.teamOrders()
    }

    teamOrders(){
      fetch("http://localhost:8008/api/teamorders", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({userid: this.loggeduser._id})
      })
      .then(res => res.json())
      .then((result)=>{
        console.log("ALL TEAM ORDERS:----", result, "\n", this.loggeduser._id)
        this.setState({team: result[0].orders}, () => {
          fetch("http://localhost:8008/api/getteamcomission", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({user: this.loggeduser._id, type: 'team'})
          })
          .then(res => res.json())
          .then((result)=>{
            this.setState({querycomission: result[0].comission[0]}, () => {
              console.log("SINGLE COMISSION:------", this.state.querycomission)
              this.countPlays(this.state.team)
            })
            
          }),
          (error) => {
            console.log(error)
          }
        })
      }),
      (error) => {
        console.log(error)
      }
    }

    clearFilter(){
      this.setState({controlledDate: null}, () => {
        this.teamOrders()
      })
    }

    handleDateChange(event, date){
      this.setState({
        controlledDate: date,
      }, () => {
        let date = (this.state.controlledDate)
        let day = date.getDate()
        let month = date.getMonth()
        let year = date.getFullYear()
        console.log("EVENT FROM DATE CHANGE:---", (this.state.controlledDate), day, month+1, year)
        this.filterOrders(date, day, month, year)
      });
    }

    filterOrders(date, day, month, year){
        console.log("EVENT FROM DATE CHANGE:---", year, month+1, day)
        let filterdate = null
        if (month+1 < 10){
          filterdate = year+'-0'+(month+1)+'-'+day
        }else{
          filterdate = year+'-'+(month+1)+'-'+day
        }
        console.log("CONACT DATE FOR FILTER:---", filterdate)
      fetch("http://localhost:8008/api/filterorders", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({date: date, day: day, month: month+1, year: year, filterdate: filterdate})
      })
      .then(res => res.json())
      .then((result)=>{
        console.log("ALL FILTERED ORDERS:----", result)
        this.setState({team: result[0].orders}, () => {
          this.countPlays(this.state.team)
        })
      }),
      (error) => {
        console.log(error)
      }
    }

    handleChange(panel, event, expanded) {
      this.setState({
        expanded: expanded ? panel : false,
      });
    };

  setStatus(status, chargeback){
    if (status === 'cancel' && !chargeback){
      return <Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="CANCELED"
      ><CancelIcon size={22} color="#D50000" /></Tooltip>
    }
    if (status === 'cancel' && chargeback){
      return <div><Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="CANCELED"
      ><CancelIcon size={22} color="#D50000" /></Tooltip>
      <Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="CHARGED BACK"
      ><IoEject size={22} color="#F44336" /></Tooltip>
      </div>
    }

    else if (status === 'installed'){
      return <Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="INSTALLED"
      ><InstallIcon size={22} color="#33691E" /></Tooltip>    
    }
    else{
      return <Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="PENDING"
      ><PendingIcon size={22} color="#EEFF41" /></Tooltip>
    }
  }

  countPlays(allorders){
    
    console.log("ALL ORDERS IN COUNT FUCNTION:---", allorders)
    
    let trippleCount = 0
    let doubleCount = 0
    let singleCount = 0
    let tvInternetCount = 0
    let tvPhoneCount = 0
    let internetPhoneCount = 0
    let internetCount = 0
    let phoneCount = 0
    let tvCount = 0
    
    for (let obj of allorders){
      if (obj.status != "cancel"){
        
        if (obj.noofunits === 3){
          trippleCount++
        }
        if (obj.noofunits === 2){
          doubleCount++
          if (obj.tv === true && obj.internet === true)
            tvInternetCount++
          else if (obj.tv === true && obj.phone === true)
            tvPhoneCount++
          else if (obj.internet === true && obj.phone === true)
            internetPhoneCount++
        }
        if (obj.noofunits === 1){
          singleCount++
          if (obj.tv === true)
            tvCount++        
          if (obj.internet === true)
            internetCount++        
          if (obj.phone === true)
            phoneCount++
        }
      }
    }
    
    console.log("TRIPPLE COUNT:---", trippleCount, 
                "\nDouble COUNT:---", doubleCount, 
                "\nSingle Count:---", singleCount,
                "\nTV INTERNET Count:---", tvInternetCount,
                "\nTV PHONE Count:---", tvPhoneCount,
                "\nINTERBET PHONE Count:---", internetPhoneCount,
                "\nSingle TV Count:---", tvCount,
                "\nSingle INTERNET Count:---", internetCount,
                "\nSingle PHONE Count:---", phoneCount,
                )

        this.setState({trippleCount: trippleCount, 
                  doubleCount: doubleCount, 
                  singleCount: singleCount,}, () => {

      
      let trippeleComission = 0
      let doubleComission = 0
      let singleComission = 0
      let tvInternetComission = 0
      let tvPhoneComission = 0
      let internetPhoneComission = 0
      let internetComission = 0
      let tvComission = 0
      let phoneComission = 0

      if (this.state.trippleCount > 0 && this.state.trippleCount <= 19){
        trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount
      }else if(this.state.trippleCount >= 20 && this.state.trippleCount <= 99){
        trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

      }else if(this.state.trippleCount >= 100 && this.state.trippleCount <= 299){
        trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

      }else if(this.state.trippleCount >= 300 && this.state.trippleCount <= 999){
        trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

      }else if(this.state.trippleCount >= 1000 && this.state.trippleCount <= 2499){
        trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

      }else if(this.state.trippleCount >= 2500){
        trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

      }

      if (tvInternetCount > 0 && tvInternetCount <= 19){
        tvInternetComission = this.state.querycomission.dptvintlt20 * tvInternetCount
      }else if(tvInternetCount >= 20 && tvInternetCount <= 99){
        tvInternetComission = this.state.querycomission.dptvintlt100 * tvInternetCount

      }else if(tvInternetCount >= 100 && tvInternetCount <= 299){
        tvInternetComission = this.state.querycomission.dptvintlt300 * tvInternetCount

      }else if(tvInternetCount >= 300 && tvInternetCount <= 999){
        tvInternetComission = this.state.querycomission.dptvintlt1000 * tvInternetCount

      }else if(tvInternetCount >= 1000 && tvInternetCount <= 2499){
        tvInternetComission = this.state.querycomission.dptvintlt2500 * tvInternetCount

      }else if(tvInternetCount >= 2500){
        tvInternetComission = this.state.querycomission.dptvintgt2500 * tvInternetCount
      }

      if (tvPhoneCount > 0 && tvPhoneCount <= 19){
        tvPhoneComission = this.state.querycomission.dptvphonelt20 * tvPhoneCount
      }else if(tvPhoneCount >= 20 && tvPhoneCount <= 99){
        tvPhoneComission = this.state.querycomission.dptvphonelt100 * tvPhoneCount

      }else if(tvPhoneCount >= 100 && tvPhoneCount <= 299){
        tvPhoneComission = this.state.querycomission.dptvphonelt300 * tvPhoneCount

      }else if(tvPhoneCount >= 300 && tvPhoneCount <= 999){
        tvPhoneComission = this.state.querycomission.dptvphonelt1000 * tvPhoneCount

      }else if(tvPhoneCount >= 1000 && tvPhoneCount <= 2499){
        tvPhoneComission = this.state.querycomission.dptvphonelt2500 * tvPhoneCount

      }else if(tvPhoneCount >= 2500){
        tvPhoneComission = this.state.querycomission.dptvphonegt2500 * tvPhoneCount
      }

      if (internetPhoneCount > 0 && internetPhoneCount <= 19){
        internetPhoneComission = this.state.querycomission.dpintphonelt20 * internetPhoneCount
      }else if(internetPhoneCount >= 20 && internetPhoneCount <= 99){
        internetPhoneComission = this.state.querycomission.dpintphonelt100 * internetPhoneCount

      }else if(internetPhoneCount >= 100 && internetPhoneCount <= 299){
        internetPhoneComission = this.state.querycomission.dpintphonelt300 * internetPhoneCount

      }else if(internetPhoneCount >= 300 && internetPhoneCount <= 999){
        internetPhoneComission = this.state.querycomission.dpintphonelt1000 * internetPhoneCount

      }else if(internetPhoneCount >= 1000 && internetPhoneCount <= 2499){
        internetPhoneComission = this.state.querycomission.dpintphonelt2500 * internetPhoneCount

      }else if(internetPhoneCount >= 2500){
        internetPhoneComission = this.state.querycomission.dpintphonegt2500 * internetPhoneCount
      }

      if (tvCount > 0 && tvCount <= 19){
        tvComission = this.state.querycomission.singletvlt20 * tvCount
      }else if(tvCount >= 20 && tvCount <= 99){
        tvComission = this.state.querycomission.singletvlt100 * tvCount

      }else if(tvCount >= 100 && tvCount <= 299){
        tvComission = this.state.querycomission.singletvlt300 * tvCount

      }else if(tvCount >= 300 && tvCount <= 999){
        tvComission = this.state.querycomission.singletvlt1000 * tvCount

      }else if(tvCount >= 1000 && tvCount <= 2499){
        tvComission = this.state.querycomission.singletvlt2500 * tvCount

      }else if(tvCount >= 2500){
        tvComission = this.state.querycomission.singletvgt2500 * tvCount
      }

      if (phoneCount > 0 && phoneCount <= 19){
        phoneComission = this.state.querycomission.singlephonelt20 * phoneCount
      }else if(phoneCount >= 20 && phoneCount <= 99){
        phoneComission = this.state.querycomission.singlephonelt100 * phoneCount

      }else if(phoneCount >= 100 && phoneCount <= 299){
        phoneComission = this.state.querycomission.singlephonelt300 * tvCount

      }else if(phoneCount >= 300 && phoneCount <= 999){
        phoneComission = this.state.querycomission.singlephonelt1000 * phoneCount

      }else if(phoneCount >= 1000 && phoneCount <= 2499){
        phoneComission = this.state.querycomission.singlephonelt2500 * phoneCount

      }else if(phoneCount >= 2500){
        phoneComission = this.state.querycomission.singlephonegt2500 * phoneCount
      }

      if (internetCount > 0 && internetCount <= 19){
        internetComission = this.state.querycomission.singleintlt20 * internetCount
      }else if(internetCount >= 20 && internetCount <= 99){
        internetComission = this.state.querycomission.singleintlt100 * internetCount

      }else if(internetCount >= 100 && internetCount <= 299){
        internetComission = this.state.querycomission.singleintlt300 * tvCount

      }else if(internetCount >= 300 && internetCount <= 999){
        internetComission = this.state.querycomission.singleintlt1000 * internetCount

      }else if(internetCount >= 1000 && internetCount <= 2499){
        internetComission = this.state.querycomission.singleintlt2500 * internetCount

      }else if(internetCount >= 2500){
        internetComission = this.state.querycomission.singleintgt2500 * internetCount
      }

      this.setState({trippeleComission: trippeleComission, 
                  tvInternetComission: tvInternetComission, 
                  tvPhoneComission: tvPhoneComission,
                  internetPhoneComission: internetPhoneComission,
                  tvComission: tvComission,
                  internetComission: internetComission,
                  phoneComission: phoneComission,}, () => {

        console.log("TRIPPLE COMMISIION:---", this.state.trippeleComission, 
                    "\nTV INTERNET COMMISIION:---", this.state.tvInternetComission,
                    "\nTV PHONE COMMISIION:---", this.state.tvPhoneComission,
                    "\nINTERBET PHONE COMMISIION:---", this.state.internetPhoneComission,
                    "\nSingle TV COMMISIION:---", this.state.tvComission,
                    "\nSingle INTERNET COMMISIION:---", this.state.internetComission,
                    "\nSingle PHONE COMMISIION:---", this.state.phoneComission,)
      })


    })
  }

    selectStyle(n){
      if (n.status == 'installed'){
        return {backgroundColor:'green'}
      }
      if (n.status == 'cancel'){
        return {backgroundColor:'red'}
      }
      if (n.status == 'pending'){
        return {backgroundColor:'yellow'}
      }

    }
    render() {

      const { classes } = this.props;
      const { team, rowsPerPage, page } = this.state;
      const emptyRows = rowsPerPage - Math.min(rowsPerPage, team.length - page * rowsPerPage);

      return (
        <div>
          
          <h3>TEAM ORDERS</h3>
          <Divider light  style={{marginBottom:15, marginTop: 15}} />
          
          <div style={{marginBottom: 10, }}>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Tripple Play Count"
            ><TrippleIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{this.state.trippleCount}</strong>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Est. Comission"
            ><MoneyIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{this.state.trippeleComission}</strong>

          </div>
          <div style={{marginBottom: 10, }}>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Duoble Play Count"
            ><DoubleIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{this.state.doubleCount}</strong>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Est. Comission"
            ><MoneyIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{(this.state.tvInternetComission + this.state.tvPhoneComission + this.state.internetPhoneComission)}</strong>

          </div>
          <div style={{marginBottom: 10, }}>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Single Play Count"
            ><SingleIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{this.state.singleCount}</strong>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Est. Comission"
            ><MoneyIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10,}}>{this.state.tvComission + this.state.phoneComission + this.state.internetComission}</strong>

          </div> 
          
          <Divider light  style={{marginBottom:15, marginTop: 15}} />

          <DatePicker hintText="Filter By Date" value={this.state.controlledDate} mode="landscape" onChange={this.handleDateChange}/>
          <Button variant="contained" size="small" color="primary" onClick={this.clearFilter}>
            CLEAR DATE
          </Button>
          <Table style={styles.table}>
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Rep ID</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Submit By</TableCell>
                <TableCell>Rep Name</TableCell>
                <TableCell>Tripple Play</TableCell>
                <TableCell>Double Play</TableCell>
                <TableCell>Single Play</TableCell>
                <TableCell>Customer Name</TableCell>
                <TableCell>Account</TableCell>
                <TableCell>Customer Number</TableCell>
                <TableCell>Address</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {team.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                return (
                  <TableRow key={n._id} hover={true}>
                    <TableCell component="th" scope="row"><Moment format="YYYY/MM/DD">{n.created_at}</Moment></TableCell>
                    <TableCell component="th" scope="row">{n.userdata[0].username}</TableCell>
                    <TableCell component="th" scope="row">{this.setStatus(n.status, n.chargeback)}</TableCell>
                    <TableCell component="th" scope="row">{n.repid != n.submitid ? n.submit_by[0].username : ''}</TableCell>
                    <TableCell component="th" scope="row">{n.repfirstname}</TableCell>
                    <TableCell component="th" scope="row">{n.trippleplay ? <CkeckIcon /> : ''}</TableCell>
                    <TableCell component="th" scope="row">{n.doubleplay ? <CkeckIcon /> : ''}</TableCell>
                    <TableCell component="th" scope="row">{n.singleplay ? <CkeckIcon /> : ''}</TableCell>
                    <TableCell component="th" scope="row">{n.customername}</TableCell>
                    <TableCell component="th" scope="row">{n.customeracc}</TableCell>
                    <TableCell component="th" scope="row">{n.customernumber}</TableCell>
                    <TableCell component="th" scope="row">{n.address}</TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 48 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  colSpan={3}
                  count={team.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActionsWrapped}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      );
    }
}

const createTab = (obj) => {
return   <ExpansionPanel key={obj._id}>
    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
      <Typography>{obj.username}</Typography>
    </ExpansionPanelSummary>
    <ExpansionPanelDetails>
      <Typography>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
        sit amet blandit leo lobortis eget.
      </Typography>
    </ExpansionPanelDetails>
  </ExpansionPanel>
}

export default Team;
