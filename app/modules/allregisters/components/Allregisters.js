import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {withRouter} from 'react-router-dom';

import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import {Tabs, Tab} from 'material-ui/Tabs';
import FontIcon from 'material-ui/FontIcon';
import Badge from 'material-ui/Badge';
import Snackbar from 'material-ui/Snackbar';

import MapsPersonPin from 'material-ui/svg-icons/maps/person-pin';
import TikTak from 'react-icons/lib/ti/tick'
import Pending from 'react-icons/lib/md/format-list-bulleted'
import Cancel from 'react-icons/lib/ti/times'
import AddPerson from 'react-icons/lib/md/person-add'

import SwipeableViews from 'react-swipeable-views';

const btnSyle = {
  margin: 12,
};

// const headingStyle = {
//   float: left,
// };

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    padding: 10,
  },
};

class Allregisters extends Component {
  
  constructor(props){
    super(props)
    this.state = {
      requests: [],
      approvedreqs: [],
      rejectedreqs: [],
      selected: [],
      multiSelectable: true, 
      selectable: true,  
      deselectOnClickaway: false,
      slideIndex: 0,
      enableSelectAll: true,
      snackBarDuration: 4000,
      snackBarMessage: 'No Request Selected',
      snackBarOpen: false,
    };
    this.getAllRequests = this.getAllRequests.bind(this)
    this.handleRowSelection = this.handleRowSelection.bind(this)
    this.handleRejectActive = this.handleRejectActive.bind(this)
    this.handleApproveActive = this.handleApproveActive.bind(this)
    this.createUser = this.createUser.bind(this)
    this.isSelected = this.isSelected.bind(this)
    this.handleSlideChange = this.handleSlideChange.bind(this)
    this.approve = this.approve.bind(this)
    this.reject = this.reject.bind(this)
    this.snackBarActionClick = this.snackBarActionClick.bind(this)
    this.snackBarOpen = this.snackBarOpen.bind(this)
    this.snackBarClose = this.snackBarClose.bind(this)
  }


  snackBarActionClick(){
    this.setState({
      snackBarOpen: false,
    });
    // alert('Event removed from your calendar.');
  };

  // snackBarDuration(event){
  //   const value = event.target.value;
  //   this.setState({
  //     snackBarDuration: value.length > 0 ? parseInt(value) : 0,
  //   });
  // };

  snackBarOpen(){
    this.setState({
      snackBarOpen: true,
    });
  };

  snackBarClose(){
    this.setState({
      snackBarOpen: false,
    });
  };


  componentDidMount(){
    console.log("SELECTED ROWS:---", this.state.selected)
    this.getAllRequests()
  }

  getAllRequests(){
    fetch("http://localhost:8008/api/allregisters", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(res => res.json())
    .then((result)=>{
      this.setState({requests: result[0].requests})
    }),
    (error) => {
      console.log(error)
    }
  }

  isSelected(index){
    return this.state.selected.indexOf(index) !== -1;
  }

  handleRowSelection(selectedRows){
    console.log("SELECTED ROWS:---", selectedRows)
    this.setState({
      selected: selectedRows
    });
  }

  handleApproveActive(){
    console.log("--------:APPROVED TAB SELECTED:--------")
    fetch("http://localhost:8008/api/approvedregisters", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(res => res.json())
    .then((result)=>{
      this.setState({approvedreqs: result[0].requests})
      console.log("RESULT FROM APPROVE:***********", result, this.state.approvedreqs)
    }),
    (error) => {
      console.log(error)
    }
  }


  handleRejectActive(){
    console.log("--------:REJECTED TAB SELECTED:--------")
    fetch("http://localhost:8008/api/rejectedregisters", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(res => res.json())
    .then((result)=>{
      this.setState({rejectedreqs: result[0].requests})
      console.log("RESULT FROM REJECT:***********", result, this.state.rejectedreqs)
    }),
    (error) => {
      console.log(error)
    }
  }


  handleSlideChange(value){
    this.setState({
      slideIndex: value
    });
  }


  approve(){
    console.log("APPROVE ACCOUNT:---", this.state.selected, this.state.requests)
    if (this.state.requests.length != 0 || this.state.selected.length != 0){
      let updateRequests = [];
      for(let obj of this.state.selected){
        let temp = this.state.requests[obj]
        // temp.status = "approved"
        updateRequests.push(temp._id)
        console.log("REQUEST OBJECT AFTER STATUS UPDATE:---", this.state.requests[obj])
      }
      console.log("ALL UPDATED REQUESTS:---", updateRequests)
      fetch("http://localhost:8008/api/updateregister", {
        method: 'post',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({updateRequests: updateRequests})
      })
      .then(res => res.json())
      .then((result) => {
        console.log(result)
        this.getAllRequests()
      }),(error) => {
        console.log(err)
      }
    }else{
      this.setState({
        snackBarOpen: true
      })
    }
  }

  reject(){
    console.log("APPROVE ACCOUNT:---", this.state.selected, this.state.requests)
    if (this.state.requests.length != 0 || this.state.selected.length != 0){
      let updateRequests = [];
      for(let obj of this.state.selected){
        let temp = this.state.requests[obj]
        // temp.status = "rejected"
        updateRequests.push(temp._id)
        console.log("REQUEST OBJECT AFTER STATUS UPDATE:---", this.state.requests[obj])
      }
      fetch("http://localhost:8008/api/rejectregister", {
        method: 'post',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({updateRequests: updateRequests})
      })
      .then(res => res.json())
      .then((result) => {
        console.log(result)
        this.getAllRequests()
      }),(error) => {
        console.log(err)
      }
    }else{
      this.setState({
        snackBarOpen: true
      })
    }
  }


  createUser($event){
    console.log("user friom create user;---", JSON.parse($event.currentTarget.dataset.id))
    this.props.history.push({
      pathname: '/adduser',
      state: { user: $event.currentTarget.dataset.id }
    })

  }


  render() {

    let buttons = null
    if (this.state.selected.length === 0 || this.state.selected === 'none'){
      buttons = null
    }else{
      buttons = <div>
          <RaisedButton
            label="Approve"
            labelPosition="before"
            style={btnSyle}
            primary={true}
            icon={<TikTak size="20" />}
            onClick={this.approve}
          />
          <RaisedButton
            label="Reject"
            labelPosition="before"
            style={btnSyle}
            secondary={true}
            onClick={this.reject}
            icon={<Cancel size="20" />}
          />
        </div>;
    }
    return (
      <div>
        <h5 className="title">Register Requests</h5>
        <Tabs
          onChange={this.handleSlideChange}
          value={this.state.slideIndex}
        >
          <Tab icon={<Pending />} 
               onActive={this.handleAllActive}
               label="PENDING" value={0} />
          <Tab icon={<TikTak />} 
               onActive={this.handleApproveActive}
               label="APPROVED" value={1} />
          <Tab icon={<Cancel />} 
               onActive={this.handleRejectActive}
               label="REJECTED" value={2} />
        </Tabs>
        <SwipeableViews
          index={this.state.slideIndex}
          onChangeIndex={this.handleSlideChange} >
          <div>
            <Table onRowSelection={this.handleRowSelection}
                   selectable={this.state.selectable}
                   multiSelectable={this.state.multiSelectable}
                   deselectOnClickaway={this.state.multiSelectable}>
              <TableHeader enableSelectAll={this.state.enableSelectAll}>
                <TableRow>
                  <TableHeaderColumn colSpan="7" tooltip="Super Header" style={{textAlign: 'center'}}>
                    {buttons}
                  </TableHeaderColumn>
                </TableRow>
                <TableRow>
                  <TableHeaderColumn>First Name</TableHeaderColumn>
                  <TableHeaderColumn>Last Name</TableHeaderColumn>
                  <TableHeaderColumn>Type</TableHeaderColumn>
                  <TableHeaderColumn>Email</TableHeaderColumn>
                  <TableHeaderColumn>Contact</TableHeaderColumn>
                  <TableHeaderColumn>Manager</TableHeaderColumn>
                  <TableHeaderColumn>Status</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody showRowHover={true}
                         deselectOnClickaway={this.state.deselectOnClickaway}>
                  {this.state.requests.map((req, i) => TablesRow(req, this.isSelected, i) )}
              </TableBody>
            </Table>
          </div>
          <div style={styles.slide}>
            <Table onRowSelection={this.handleRowSelection}
                   selectable={false}
                   multiSelectable={false}
                   deselectOnClickaway={false}>
              <TableHeader enableSelectAll={false} 
                           displaySelectAll={false}
                           adjustForCheckbox={false}>
                <TableRow>
                  <TableHeaderColumn>First Name</TableHeaderColumn>
                  <TableHeaderColumn>Last Name</TableHeaderColumn>
                  <TableHeaderColumn>Type</TableHeaderColumn>
                  <TableHeaderColumn>Email</TableHeaderColumn>
                  <TableHeaderColumn>Contact</TableHeaderColumn>
                  <TableHeaderColumn>Manager</TableHeaderColumn>
                  <TableHeaderColumn>Status</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false} 
                         showRowHover={true}>
                  {this.state.approvedreqs.map((req, i) => TablesApproveRow(req, this.createUser) )}
              </TableBody>
            </Table>
          </div>
          <div style={styles.slide}>
            <Table onRowSelection={this.handleRowSelection}
                   selectable={false}
                   multiSelectable={false}
                   deselectOnClickaway={false}>
              <TableHeader enableSelectAll={false}
                           displaySelectAll={false}
                           adjustForCheckbox={false}>
                <TableRow>
                  <TableHeaderColumn>First Name</TableHeaderColumn>
                  <TableHeaderColumn>Last Name</TableHeaderColumn>
                  <TableHeaderColumn>Type</TableHeaderColumn>
                  <TableHeaderColumn>Email</TableHeaderColumn>
                  <TableHeaderColumn>Contact</TableHeaderColumn>
                  <TableHeaderColumn>Manager</TableHeaderColumn>
                  <TableHeaderColumn>Status</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false} 
                         showRowHover={true}>
                  {this.state.rejectedreqs.map((req, i) => TablesRejectedRow(req) )}
              </TableBody>
            </Table>

          </div>
        </SwipeableViews>
        <Snackbar
          open={this.state.snackBarOpen}
          message={this.state.snackBarMessage}
          action="undo"
          autoHideDuration={this.state.snackBarDuration}
          onActionClick={this.snackBarActionClick}
          onRequestClose={this.snackBarClose}
        />
      </div>
    );
  }
}


const TablesRow = (obj, isSelected, i) => (
  <TableRow key={obj._id} selected={isSelected(i)}>
    <TableRowColumn>{obj.firstname}</TableRowColumn>
    <TableRowColumn>{obj.lastname}</TableRowColumn>
    <TableRowColumn>{obj.type}</TableRowColumn>
    <TableRowColumn>{obj.email}</TableRowColumn>
    <TableRowColumn>{obj.contact}</TableRowColumn>
    <TableRowColumn>{obj.managername}</TableRowColumn>
    <TableRowColumn>{obj.status}</TableRowColumn>
  </TableRow>
)

const test = (obj) => console.log(JSON.parse(obj.currentTarget.dataset.id))

const TablesApproveRow = (obj, createUser) => (
  // const { createUser } = this.props
  <TableRow key={obj._id}>
    <TableRowColumn>{obj.firstname}</TableRowColumn>
    <TableRowColumn>{obj.lastname}</TableRowColumn>
    <TableRowColumn>{obj.type}</TableRowColumn>
    <TableRowColumn>{obj.email}</TableRowColumn>
    <TableRowColumn>{obj.contact}</TableRowColumn>
    <TableRowColumn>{obj.managername}</TableRowColumn>
    <TableRowColumn>
      <IconButton data-id={JSON.stringify(obj)}
                  onClick={createUser}
                  disabled={obj.activated} 
                  style={btnSyle}>
        <AddPerson size="24" />
      </IconButton>
    </TableRowColumn>
  </TableRow>
)

const TablesRejectedRow = (obj) => (
  <TableRow key={obj._id}>
    <TableRowColumn>{obj.firstname}</TableRowColumn>
    <TableRowColumn>{obj.lastname}</TableRowColumn>
    <TableRowColumn>{obj.type}</TableRowColumn>
    <TableRowColumn>{obj.email}</TableRowColumn>
    <TableRowColumn>{obj.contact}</TableRowColumn>
    <TableRowColumn>{obj.managername}</TableRowColumn>
    <TableRowColumn>{obj.status}</TableRowColumn>
  </TableRow>
)







export default withRouter(Allregisters);