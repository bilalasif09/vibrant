import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TableHeader,
  TableHeaderColumn,
  TableRowColumn,
} from 'material-ui/Table';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Moment from 'react-moment';
import PiorityHigh from 'react-icons/lib/md/priority-high';
import PaperWork from 'react-icons/lib/md/work';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import ArrowBack from 'react-icons/lib/md/arrow-back';


import {withRouter} from 'react-router-dom'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

const styles = {
  root: {
    width: '100%',
    marginTop: 16,
    overflowX: 'auto',
  },
  table: {
    minWidth: '100%',
  },
  customWidth: {
    width: '100%',
  },
}

class Allusers extends Component {

  constructor(props) {
      super(props);
      this.state = {
        allusers: [],
        allmanagers: [],
      }
      this.handleManagerChange = this.handleManagerChange.bind(this)
  }

  componentDidMount(){
    fetch("http://localhost:8008/api/allusers")
    .then(res => res.json())
    .then(
      (result) => {
            console.log("STATE FROM ALL USERS:-----", result)
          this.setState({
            allusers: result[0].users
          }, () => {
            console.log("STATE FROM ALL USERS:-----", this.state.allusers)
          })
      },
      (error) => {
        console.log(error)
      }
    )
    fetch("http://localhost:8008/api/allmanagers", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(res => res.json())
    .then((result)=>{
      this.setState({
        allmanagers: result[0].users
      }, () => {
        console.log("STATE ALL MANAGER:---", this.state.allmanagers)
      })
    }),
    (error) => {
      console.log(error)
    }
  }

  handleManagerChange(event, index, value){
    this.setState({manager: value}, () => {
      console.log("MANAGER VALUE:---", this.state.manager)
      if (this.state.manager){

        fetch("http://localhost:8008/api/managerreps", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({manager: this.state.manager})
        })
        .then(res => res.json())
        .then((result)=>{
          this.setState({allusers: result[0].users}, () => {
            console.log("ALL USERS OF MANAGER:----", this.state.allreps)
          })
        }),
        (error) => {
          console.log(error)
        }
      }else{
        fetch("http://localhost:8008/api/allusers")
        .then(res => res.json())
        .then(
          (result) => {
              console.log("STATE FROM ALL USERS:-----", result)
              this.setState({
                allusers: result[0].users
              }, () => {
                console.log("STATE FROM ALL USERS:-----", this.state.allusers)
              })
          },
          (error) => {
            console.log(error)
          }
        )
      }
    });
  }

  render() {
    
    const { classes } = this.props;

    return (
      <div>
        <Grid container spacing={8}>
          <Grid item xs={6} sm={10}>
            <h3>ALL USERS</h3> 
          </Grid>
          <Grid item xs={6} sm={2}>
            <IconButton onClick={this.props.history.goBack} 
                        variant="fab" 
                        color="primary" 
                        aria-label="Go Back">
              <ArrowBack />
            </IconButton>
          </Grid>
        </Grid>
        <SelectField
          floatingLabelText="Filter By Manager"
          value={this.state.manager}
          onChange={this.handleManagerChange}
          style={styles.customWidth}
        >
          <MenuItem value={null} primaryText="" />
            {this.state.allmanagers.map((obj, i) => {
              return (<MenuItem key={obj._id} value={obj._id} primaryText={obj.firstname} />)
          })}
        </SelectField>

        <Table style={styles.root}>
          <TableHead>
            <TableRow>
              <TableCell>Date</TableCell>
              <TableCell>Username</TableCell>
              <TableCell>Password</TableCell>
              <TableCell>First Name</TableCell>
              <TableCell>Last Name</TableCell>
              <TableCell>Type</TableCell>
              <TableCell>Paper Work</TableCell>
              <TableCell>E-mail</TableCell>
              <TableCell>Contact</TableCell>
              <TableCell>Manager</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.allusers.map(n => {
              return (
                <TableRow key={n._id}>
                  <TableCell component="th" scope="row"><Moment format="YYYY/MM/DD">{n.created_at}</Moment></TableCell>
                  <TableCell component="th" scope="row">{n.username}</TableCell>
                  <TableCell component="th" scope="row">{n.password}</TableCell>
                  <TableCell component="th" scope="row">{n.firstname}</TableCell>
                  <TableCell component="th" scope="row">{n.lastname}</TableCell>
                  <TableCell component="th" scope="row">{n.type}</TableCell>
                  <TableCell component="th" scope="row">{n.contracted ? 
                  <Tooltip id="tooltip-icon" title="Paper Work Submitted">
                    <IconButton aria-label="Paper Work">
                      <PaperWork />
                    </IconButton>
                  </Tooltip> : 
                  <Tooltip id="tooltip-icon" title="Need Paper Work">
                    <IconButton aria-label="Paper Work">
                      <PiorityHigh />
                    </IconButton>
                  </Tooltip>}</TableCell>
                  <TableCell component="th" scope="row">{n.email}</TableCell>
                  <TableCell component="th" scope="row">{n.contact}</TableCell>
                  <TableCell component="th" scope="row">{n.managername}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default withRouter(Allusers);
