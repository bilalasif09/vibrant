import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TableHeader,
  TableHeaderColumn,
  TableRowColumn,
} from 'material-ui/Table';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Moment from 'react-moment';
import PiorityHigh from 'react-icons/lib/md/priority-high';
import PaperWork from 'react-icons/lib/md/work';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';



const styles = {
  root: {
    width: '100%',
    marginTop: 16,
    overflowX: 'auto',
  },
  table: {
    minWidth: '100%',
  },
  customWidth: {
    width: '100%',
  },
}

class Comissions extends Component {

  constructor(props) {
      super(props);
      this.state = {
        allcomissions: [],
        allmanagers: [],
        tripplelt20: '',
        tripplelt100: '',
        tripplelt300: '',
        tripplelt1000: '',
        tripplelt2500: '',
        tripplegt2500: '',
        dptvphonelt20: '',
        dptvphonelt100: '',
        dptvphonelt300: '',
        dptvphonelt1000: '',
        dptvphonelt2500: '',
        dptvphonegt2500: '',
        dpintphonelt20: '',
        dpintphonelt100: '',
        dpintphonelt300: '',
        dpintphonelt1000: '',
        dpintphonelt2500: '',
        dpintphonegt2500: '',
        dptvintlt20: '',
        dptvintlt100: '',
        dptvintlt300: '',
        dptvintlt1000: '',
        dptvintlt2500: '',
        dptvintgt2500: '',
        singletvlt20: '',
        singletvlt100: '',
        singletvlt300: '',
        singletvlt1000: '',
        singletvlt2500: '',
        singletvgt2500: '',
        singlephonelt20: '',
        singlephonelt100: '',
        singlephonelt300: '',
        singlephonelt1000: '',
        singlephonelt2500: '',
        singlephonegt2500: '',
        singleintlt20: '',
        singleintlt100: '',
        singleintlt300: '',
        singleintlt1000: '',
        singleintlt2500: '',
        singleintgt2500: '',
        allmanagers: [],
        manager: null,
        type: null,
        managerplaceholder: 'SELECT MANAGER',
        updateflag: false,
      }
      this.handleManagerChange = this.handleManagerChange.bind(this)
      this.handleTypeChange = this.handleTypeChange.bind(this)
  }

  componentDidMount(){
    // fetch("http://localhost:8008/api/allcomissions")
    // .then(res => res.json())
    // .then(
    //   (result) => {
    //         console.log("STATE FROM ALL COMISIONS:-----", result)
    //       this.setState({
    //         allcomissions: result[0].comissions
    //       }, () => {
    //         console.log("STATE FROM ALL COMISIONS:-----", this.state.allcomissions)
    //       })
    //   },
    //   (error) => {
    //     console.log(error)
    //   }
    // )
    fetch("http://localhost:8008/api/allmanagers", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(res => res.json())
    .then((result)=>{
      this.setState({
        allmanagers: result[0].users
      }, () => {
        console.log("STATE ALL MANAGER:---", this.state.allmanagers)
      })
    }),
    (error) => {
      console.log(error)
    }
  }

  handleTypeChange(event, index, value){
    this.setState({type: value}, () => {
      if (this.state.type == 'team'){
        console.log("TYPE CHANGE TEAM:---", this.state.type)
        this.setState({managerplaceholder: "SELECT TEAM", manager: null,}, () => {
          this.clearForm()
        })
      }else{
        console.log("TYPE CHANGE MANAGER:---", this.state.type)
        this.setState({managerplaceholder: "SELECT MANAGER", manager: null,}, () => {
          this.clearForm()
        })
      }
    })
  }

  handleManagerChange(event, index, value){
    this.setState({manager: value}, () => {
      console.log("MANAGER STATE VALUE:---", this.state.manager)
      if (this.state.manager){
        if (this.state.type == 'team'){
          fetch("http://localhost:8008/api/getteamcomission", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({user: this.state.manager, type: 'team'})
          })
          .then(res => res.json())
          .then((result)=>{
            console.log("SINGLE COMISSION:------", result)
            if (result[0].comission.length){
              this.setState({updateflag: true}, () => {
                this.setFields(result[0].comission[0])
              })
            }else{
              this.clearForm()
            }
          }),
          (error) => {
            console.log(error)
          }
        }else if (this.state.type == 'manager'){
          fetch("http://localhost:8008/api/getteamcomission", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({user: this.state.manager, type: 'manager'})
          })
          .then(res => res.json())
          .then((result)=>{
            console.log("SINGLE COMISSION:------", result)
            if (result[0].comission.length){
              this.setState({updateflag: true}, () => {
                this.setFields(result[0].comission[0])
              })
            }else{
              this.clearForm()
            }
          }),
          (error) => {
            console.log(error)
          }
        }
      }else{
        this.clearForm()
      }
    })
  }

  clearForm(){
    this.setState({
      tripplelt20: '',
      tripplelt100: '',
      tripplelt300: '',
      tripplelt1000: '',
      tripplelt2500: '',
      tripplegt2500: '',
      dptvphonelt20: '',
      dptvphonelt100: '',
      dptvphonelt300: '',
      dptvphonelt1000: '',
      dptvphonelt2500: '',
      dptvphonegt2500: '',
      dpintphonelt20: '',
      dpintphonelt100: '',
      dpintphonelt300: '',
      dpintphonelt1000: '',
      dpintphonelt2500: '',
      dpintphonegt2500: '',
      dptvintlt20: '',
      dptvintlt100: '',
      dptvintlt300: '',
      dptvintlt1000: '',
      dptvintlt2500: '',
      dptvintgt2500: '',
      singletvlt20: '',
      singletvlt100: '',
      singletvlt300: '',
      singletvlt1000: '',
      singletvlt2500: '',
      singletvgt2500: '',
      singlephonelt20: '',
      singlephonelt100: '',
      singlephonelt300: '',
      singlephonelt1000: '',
      singlephonelt2500: '',
      singlephonegt2500: '',
      singleintlt20: '',
      singleintlt100: '',
      singleintlt300: '',
      singleintlt1000: '',
      singleintlt2500: '',
      singleintgt2500: '',
      updateflag: false,
    })
  }


  setFields(obj){
    console.log("OBJECT TO SET INPUT STATE:---", obj)
    this.setState({
                  tripplelt20: obj.tripplelt20,
                  tripplelt100: obj.tripplelt100,
                  tripplelt300: obj.tripplelt300,
                  tripplelt1000: obj.tripplelt1000,
                  tripplelt2500: obj.tripplelt2500,
                  tripplegt2500: obj.tripplegt2500,
                  
                  dptvphonelt20: obj.dptvintlt20,
                  dptvphonelt100: obj.dptvphonelt100,
                  dptvphonelt300: obj.dptvphonelt300,
                  dptvphonelt1000: obj.dptvphonelt1000,
                  dptvphonelt2500: obj.dptvphonelt2500,
                  dptvphonegt2500: obj.dptvphonegt2500,
                  
                  dpintphonelt20: obj.dpintphonelt20,
                  dpintphonelt100: obj.dpintphonelt100,
                  dpintphonelt300: obj.dpintphonelt300,
                  dpintphonelt1000: obj.dpintphonelt1000,
                  dpintphonelt2500: obj.dpintphonelt2500,
                  dpintphonegt2500: obj.dpintphonegt2500,
                  
                  dptvintlt20: obj.dptvintlt20,
                  dptvintlt100: obj.dptvintlt100,
                  dptvintlt300: obj.dptvintlt300,
                  dptvintlt1000: obj.dptvintlt1000,
                  dptvintlt2500: obj.dptvintlt2500,
                  dptvintgt2500: obj.dptvintgt2500,

                  singletvlt20: obj.singletvlt20,
                  singletvlt100: obj.singletvlt100,
                  singletvlt300: obj.singletvlt300,
                  singletvlt1000: obj.singletvlt1000,
                  singletvlt2500: obj.singletvlt2500,
                  singletvgt2500: obj.singletvgt2500,
                  
                  singlephonelt20: obj.singlephonelt20,
                  singlephonelt100: obj.singlephonelt100,
                  singlephonelt300: obj.singlephonelt300,
                  singlephonelt1000: obj.singlephonelt1000,
                  singlephonelt2500: obj.singlephonelt2500,
                  singlephonegt2500: obj.singlephonegt2500,
                  
                  singleintlt20: obj.singleintlt20,
                  singleintlt100: obj.singleintlt100,
                  singleintlt300: obj.singleintlt300,
                  singleintlt1000: obj.singleintlt1000,
                  singleintlt2500: obj.singleintlt2500,
                  singleintgt2500: obj.singleintgt2500,

                  userref: obj.manager,
                  type: obj.type,
    })
  }

  render() {
    
    const { classes } = this.props;
    const {tripplelt20,
      tripplelt100,
      tripplelt300,
      tripplelt1000,
      tripplelt2500,
      tripplegt2500,
      dptvphonelt20,
      dptvphonelt100,
      dptvphonelt300,
      dptvphonelt1000,
      dptvphonelt2500,
      dptvphonegt2500,
      dpintphonelt20,
      dpintphonelt100,
      dpintphonelt300,
      dpintphonelt1000,
      dpintphonelt2500,
      dpintphonegt2500,
      dptvintlt20,
      dptvintlt100,
      dptvintlt300,
      dptvintlt1000,
      dptvintlt2500,
      dptvintgt2500,
      singletvlt20,
      singletvlt100,
      singletvlt300,
      singletvlt1000,
      singletvlt2500,
      singletvgt2500,
      singlephonelt20,
      singlephonelt100,
      singlephonelt300,
      singlephonelt1000,
      singlephonelt2500,
      singlephonegt2500,
      singleintlt20,
      singleintlt100,
      singleintlt300,
      singleintlt1000,
      singleintlt2500,
      singleintgt2500,} = this.state

    return (
      <div>
        <h3>COMISSIONS</h3> 
        <SelectField
          floatingLabelText="COMISSION FOR"
          value={this.state.type}
          onChange={this.handleTypeChange}
          required
        >
          <MenuItem value={null} primaryText="" />
          <MenuItem value={'team'} primaryText="Team" />
          <MenuItem value={'manager'} primaryText="Manager" />
        </SelectField>        
        <SelectField
          floatingLabelText={this.state.managerplaceholder}
          value={this.state.manager}
          onChange={this.handleManagerChange}
          required
        >
          <MenuItem value={null} primaryText="" />
            {this.state.allmanagers.map((obj, i) => {
              return (<MenuItem key={obj._id} value={obj._id} primaryText={this.state.type == 'team' ? obj.firstname+" Team" : obj.firstname} />)
          })}
        </SelectField>
        <Table style={styles.root}>
          <TableHead>
            <TableRow>
              <TableCell>{"product pacKage".toUpperCase()}</TableCell>
              <TableCell>2500+ Units</TableCell>
              <TableCell>1000-2499 Units</TableCell>
              <TableCell>300-999 Units</TableCell>
              <TableCell>100-299 Units</TableCell>
              <TableCell>20-99 Units</TableCell>
              <TableCell>19 & under Units</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">TRIPPLE PLAY</TableCell>
              <TableCell component="th" scope="row">{tripplegt2500}</TableCell>
              <TableCell component="th" scope="row">{tripplelt2500}</TableCell>
              <TableCell component="th" scope="row">{tripplelt1000}</TableCell>
              <TableCell component="th" scope="row">{tripplelt300}</TableCell>
              <TableCell component="th" scope="row">{tripplelt100}</TableCell>
              <TableCell component="th" scope="row">{tripplelt20}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">DOUBLE PLAY (INTERNET, PHONE)</TableCell>
              <TableCell component="th" scope="row">{dpintphonegt2500}</TableCell>
              <TableCell component="th" scope="row">{dpintphonelt2500}</TableCell>
              <TableCell component="th" scope="row">{dpintphonelt1000}</TableCell>
              <TableCell component="th" scope="row">{dpintphonelt300}</TableCell>
              <TableCell component="th" scope="row">{dpintphonelt100}</TableCell>
              <TableCell component="th" scope="row">{dpintphonelt20}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">DOUBLE PLAY (INTERNET, TV)</TableCell>
              <TableCell component="th" scope="row">{dptvintgt2500}</TableCell>
              <TableCell component="th" scope="row">{dptvintlt2500}</TableCell>
              <TableCell component="th" scope="row">{dptvintlt1000}</TableCell>
              <TableCell component="th" scope="row">{dptvintlt300}</TableCell>
              <TableCell component="th" scope="row">{dptvintlt100}</TableCell>
              <TableCell component="th" scope="row">{dptvintlt20}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">DOUBLE PLAY (PHONE, TV)</TableCell>
              <TableCell component="th" scope="row">{dptvphonegt2500}</TableCell>
              <TableCell component="th" scope="row">{dptvphonelt2500}</TableCell>
              <TableCell component="th" scope="row">{dptvphonelt1000}</TableCell>
              <TableCell component="th" scope="row">{dptvphonelt300}</TableCell>
              <TableCell component="th" scope="row">{dptvphonelt100}</TableCell>
              <TableCell component="th" scope="row">{dptvphonelt20}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">SINGLE PLAY PHONE</TableCell>
              <TableCell component="th" scope="row">{singlephonegt2500}</TableCell>
              <TableCell component="th" scope="row">{singlephonelt2500}</TableCell>
              <TableCell component="th" scope="row">{singlephonelt1000}</TableCell>
              <TableCell component="th" scope="row">{singlephonelt300}</TableCell>
              <TableCell component="th" scope="row">{singlephonelt100}</TableCell>
              <TableCell component="th" scope="row">{singlephonelt20}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">SINGLE PLAY TV</TableCell>
              <TableCell component="th" scope="row">{singletvgt2500}</TableCell>
              <TableCell component="th" scope="row">{singletvlt2500}</TableCell>
              <TableCell component="th" scope="row">{singletvlt1000}</TableCell>
              <TableCell component="th" scope="row">{singletvlt300}</TableCell>
              <TableCell component="th" scope="row">{singletvlt100}</TableCell>
              <TableCell component="th" scope="row">{singletvlt20}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">SINGLE PLAY INTERNET</TableCell>
              <TableCell component="th" scope="row">{singleintgt2500}</TableCell>
              <TableCell component="th" scope="row">{singleintlt2500}</TableCell>
              <TableCell component="th" scope="row">{singleintlt1000}</TableCell>
              <TableCell component="th" scope="row">{singleintlt300}</TableCell>
              <TableCell component="th" scope="row">{singleintlt100}</TableCell>
              <TableCell component="th" scope="row">{singleintlt20}</TableCell>
            </TableRow>

          </TableBody>
        </Table>
      </div>
    );
  }
}

export default Comissions;
