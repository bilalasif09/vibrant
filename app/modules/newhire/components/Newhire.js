import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import AddBtn from 'material-ui/svg-icons/action/done';
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import Checkbox from 'material-ui/Checkbox';


const radioStyles = {
  block: {
    maxWidth: '100%',
  },
  radioButton: {
    marginBottom: 16,
  },
};

const checkboxStyles = {
  block: {
    maxWidth: '100%',
  },
  radioButton: {
    marginBottom: 16,
  },
};



const styles = {
  customWidth: {
    width: '100%',
    marginTop: 16,
  },
};

const nameInputStyle = {
  width: '33.3%',
}

const ciNameStyle = {
  width: '25%',
}


const phoneInputStyle = {
  width: '50%',
}

class Newhire extends Component {

  constructor(props) {
      super(props);
      this.state = {
        value: "",
        citizen: false,
        noncitizen: false,
        presidence: false,
        alienauth: false,
        firstname: "",
        middlename: "",
        lastname: "",
        email: "",
        address: "",
        city: "",
        state: "",
        zip: "",
        mailaddress: "",
        mailcity: "",
        mailstate: "",
        mailzip: "",
        phone: "",
        altphone: "",
        refname1: "",
        refphone1: "",
        refname2: "",
        refphone2: "",
        cilastname: "",
        cifirstname: "",
        cimiddlename: "",
        cimaidanname: "",
        contractorpaidas: "",
        contractorprefix: "",
        ciaddress: "",
        cicity: "",
        cistate: "",
        cizip: "",
        busaddress: "",
        buscity: "",
        busstate: "",
        buszip: "",
        homephone: "",
        cellphone: "",
        altphone2: "",
        ciemail: "",
        cissn: "",
        driverlicense: "",
        refperson: "",
        ein: "",
        emergcontact: "",
        emergphone: "",
        campaigncity: "",
        campaignstate: "",
      }
      this.handleNameChanges = this.handleNameChanges.bind(this)
      this.handleAddressChanges = this.handleAddressChanges.bind(this)
      this.handleStateChanges = this.handleStateChanges.bind(this)
      this.handleZipChanges = this.handleZipChanges.bind(this)
      this.handlePhoneChanges = this.handlePhoneChanges.bind(this)
      this.handleCityChanges = this.handleCityChanges.bind(this)
      this.handleEmailChanges = this.handleEmailChanges.bind(this)
      this.handleSsnChanges = this.handleSsnChanges.bind(this)
      this.driverlicenseChanges = this.driverlicenseChanges.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
      this.loggedUser = JSON.parse(localStorage.getItem('loggeduser'))
      
      
  }

  updateCitizenCheck() {
    this.setState((oldState) => {
      return {
        citizen: !oldState.citizen,
      };
    });
  }

  alienauthCheck() {
    this.setState((oldState) => {
      return {
        alienauth: !oldState.alienauth,
      };
    });
  }

  nonCitizenCheck() {
    this.setState((oldState) => {
      return {
        noncitizen: !oldState.noncitizen,
      };
    });
  }

  presidenceCheck() {
    this.setState((oldState) => {
      return {
        presidence: !oldState.presidence,
      };
    });
  }

  handleSubmit(e){
    e.preventDefault()
    let userId = localStorage.getItem('userId')
    console.log("First Name:---", this.state.firstname, "\n",
                "Last Name:---", this.state.lastname, "\n",
                "Middle Name:---", this.state.middlename, "\n",
                "ADDRESS:---", this.state.address, "\n",
                "CITY:---", this.state.city, "\n",
                "STATE:---", this.state.state, "\n",
                "ZIP:---", this.state.zip, "\n",
                "MAIL ADDRESS:---", this.state.mailaddress, "\n",
                "MAIL CITY:---", this.state.mailcity, "\n",
                "MAIL STATE:---", this.state.mailstate, "\n",
                "MAIL ZIP:---", this.state.mailzip, "\n",
                "MAIL EMAIL:---", this.state.email, "\n",
                "MAIL PHONE:---", this.state.phone, "\n",
                "MAIL ALT. PHONE:---", this.state.altphone, "\n",
                "FIRST REF NAME:---", this.state.refname1, "\n",
                "FIRST REF NUMBER:---", this.state.refphone1, "\n",
                "SECOND REF NAME:---", this.state.refname2, "\n",
                "SECOND REF NUMBER:---", this.state.refphone2, "\n",
                "CONTCT INFO FIRST NAME:---", this.state.cifirstname, "\n",
                "CONTCT INFO LAST NAME:---", this.state.cilastname, "\n",
                "CONTCT INFO MIDDLE NAME:---", this.state.cimiddlename, "\n",
                "CONTCT INFO MAIDEN NAME:---", this.state.cimaidanname, "\n",
                "CONTCT INFO HOME ADDRESS:---", this.state.ciaddress, "\n",
                "CONTCT INFO HOME CITY:---", this.state.cicity, "\n",
                "CONTCT INFO HOME STATE:---", this.state.cistate, "\n",
                "CONTCT INFO HOME ZIP:---", this.state.cizip, "\n",
                "CONTCT INFO BUSINESS ADDRESS:---", this.state.busaddress, "\n",
                "CONTCT INFO BUSINESS CITY:---", this.state.buscity, "\n",
                "CONTCT INFO BUSINESS STATE:---", this.state.busstate, "\n",
                "CONTCT INFO BUSINESS ZIP:---", this.state.buszip, "\n",
                "CONTCT INFO HOME PHONE:---", this.state.homephone, "\n",
                "CONTCT INFO CELL PHONE:---", this.state.cellphone, "\n",
                "CONTCT INFO ALT. PHONE:---", this.state.altphone2, "\n",
                "CONTCT INFO EMAIL:---", this.state.ciemail, "\n",
                "CONTCT INFO SSN:---", this.state.cissn, "\n",
                "CONTCT INFO DRIVER LICENSE:---", this.state.driverlicense, "\n",
                "CONACT INFO REF PERSON:---", this.state.refperson, "\n",
                "CONTACT INFO REF PERSON:---", this.state.ein, "\n",
                "EMERGENCY CONTACT NAME:---", this.state.emergcontact, "\n",
                "EMERGENCY CONTACT PHONE:---", this.state.emergphone, "\n",
                "CAMPAIGN CITY:---", this.state.campaigncity, "\n",
                "CAMPAIGN SATTE:---", this.state.campaignstate, "\n",
                )
    fetch("http://localhost:8008/api/addnewhire", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({firstname: this.state.firstname, 
                              lastname: this.state.lastname, 
                              middlename: this.state.middlename,
                              address: this.state.address,
                              city: this.state.city, 
                              state: this.state.state,
                              zip: this.state.zip, 
                              mailaddress: this.state.mailaddress, 
                              mailcity: this.state.mailcity, 
                              mailstate: this.state.mailstate, 
                              mailzip: this.state.mailzip, 
                              email: this.state.email, 
                              phone: this.state.phone, 
                              altphone: this.state.altphone, 
                              refname1: this.state.refname1, 
                              refphone1: this.state.refphone1, 
                              refname2: this.state.refname2, 
                              refphone2: this.state.refphone2, 
                              cifirstname: this.state.cifirstname, 
                              cilastname: this.state.cilastname, 
                              cimiddlename: this.state.cimiddlename, 
                              cimaidanname: this.state.cimaidanname, 
                              ciaddress: this.state.ciaddress, 
                              cicity: this.state.cicity, 
                              cistate: this.state.cistate, 
                              cizip: this.state.cizip,
                              busaddress: this.state.busaddress, 
                              buscity: this.state.buscity, 
                              busstate: this.state.busstate, 
                              buszip: this.state.buszip,
                              homephone: this.state.homephone, 
                              cellphone: this.state.cellphone, 
                              altphone2: this.state.altphone2,
                              ciemail: this.state.ciemail, 
                              cissn: this.state.cissn,
                              driverlicense: this.state.driverlicense,
                              refperson: this.state.refperson,
                              ein: this.state.ein,
                              emergcontact: this.state.emergcontact,
                              emergphone: this.state.emergphone,
                              campaigncity: this.state.campaigncity,
                              campaignstate: this.state.campaignstate,
                              contractorpaidas: this.state.contractorpaidas,
                              contractorprefix: this.state.contractorprefix,
                              userId: userId})
    })
    .then(res => res.json())
    .then((result)=>{
      console.log("RESULT FROM ADD USER:------", result)
      this.setState({
        firstname: "",
        middlename: "",
        address: "",
        city: "", 
        state: "",
        zip: "", 
        mailaddress: "", 
        mailcity: "", 
        mailstate: "", 
        mailzip: "", 
        email: "", 
        phone: "", 
        altphone: "", 
        refname1: "", 
        refphone1: "", 
        refname2: "", 
        refphone2: "", 
        cifirstname: "", 
        cilastname: "", 
        cimiddlename: "", 
        cimaidanname: "", 
        ciaddress: "", 
        cicity: "", 
        cistate: "", 
        cizip: "",
        busaddress: "", 
        buscity: "", 
        busstate: "", 
        buszip: "",
        homephone: "", 
        cellphone: "", 
        altphone2: "",
        ciemail: "", 
        cissn: "",
        driverlicense: "",
        refperson: "",
        ein: "",
        emergcontact: "",
        emergphone: "",
        campaigncity: "",
        campaignstate: "",
        contractorpaidas: "",
        contractorprefix: ""
      }, () => {
        fetch("http://localhost:8008/api/setcontract", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({userid: this.loggedUser._id})
        })
        .then(res => res.json())
        .then((result)=>{
          console.log("RESULT FROM UPDATING CONTRACT USER:------", result)
        }),
        (error) => {
          console.log(error)
        }
      })
    }),
    (error) => {
      console.log(error)
    }
  }




  handleNameChanges(e){
    let name = e.target.name
    this.setState({
      [name]: e.target.value
    })
  }

  handleAddressChanges(e){
    let name = e.target.name
    this.setState({
      [name]: e.target.value
    })
  }

  handleStateChanges(e){
    let name = e.target.name
    this.setState({
      [name]: e.target.value
    })
  }

  handleCityChanges(e){
    let name = e.target.name
    this.setState({
      [name]: e.target.value
    })
  }


  handleZipChanges(e){
    let name = e.target.name
    this.setState({
      [name]: e.target.value
    })
  }

  handlePhoneChanges(e){
    let name = e.target.name
    this.setState({
      [name]: e.target.value
    })
  }

  handleEmailChanges(e){
    let name = e.target.name
    this.setState({
      [name]: e.target.value
    })
  }


  handleSsnChanges(e){
    this.setState({
      cissn: e.target.value
    })
  }

  driverlicenseChanges(e){
    this.setState({
      driverlicense: e.target.value
    })
  }





  render() {

    const { value, 
            firstname, 
            middlename, 
            lastname, 
            email, 
            address, 
            city, 
            state, 
            zip, 
            mailaddress, 
            mailcity, 
            mailstate, 
            mailzip,
            phone,
            refphone1,
            refphone2,
            refname1,
            refname2,
            cilastname,
            cifirstname,
            cimiddlename,
            cimaidanname,
            contractorpaidas,
            contractorprefix,
            ciaddress,
            cicity,
            cistate,
            cizip,
            busaddress,
            buscity,
            busstate,
            buszip,
            homephone,
            cellphone,
            altphone,
            ciemail,
            cissn,
            driverlicense,
            refperson,
            ein,
            emergcontact,
            emergphone,
            campaigncity,
            campaignstate,
            altphone2 } = this.state

    return (
        <div>
          <h2>NEW HIRE ON BOARD</h2>
          <Divider />
          <form onSubmit={this.handleSubmit}>
            <SelectField
              floatingLabelText="Hiring Manager"
              value={value}
              onChange={this.handleChange}
              style={styles.customWidth}
            >
              <MenuItem value={null} primaryText="" />
              <MenuItem value={'re'} primaryText="Representative" />
              <MenuItem value={'ma'} primaryText="Manager" />
              <MenuItem value={'cp'} primaryText="Corporate Employee" />
            </SelectField>
            <TextField
              id="lastname"
              floatingLabelText="Last Name"
              name="lastname"
              onChange={this.handleNameChanges}
              value={lastname}
              style={nameInputStyle}
              required
            />
            <TextField
              id="firstname"
              name="firstname"
              onChange={this.handleNameChanges}
              floatingLabelText="First Name"
              value={firstname}
              style={nameInputStyle}
              required
            />
            <TextField
              id="middlename"
              name="middlename"
              onChange={this.handleNameChanges}
              floatingLabelText="Middle Name"
              value={middlename}
              style={nameInputStyle}
              required
            />

            <h4>Physical Address</h4>
            <Divider />
            <TextField
              id="address"
              name="address"
              onChange={this.handleAddressChanges}
              floatingLabelText="Address"
              value={address}
              style={styles.customWidth}
              required
            />
            <TextField
              id="city"
              name="city"
              onChange={this.handleCityChanges}
              floatingLabelText="City"
              value={city}
              style={nameInputStyle}
              required
            />
            <TextField
              id="state"
              name="state"
              onChange={this.handleStateChanges}
              floatingLabelText="State"
              value={state}
              style={nameInputStyle}
              required
            />
            <TextField
              id="zip"
              name="zip"
              onChange={this.handleZipChanges}
              floatingLabelText="Zip Code"
              value={zip}
              style={nameInputStyle}
              required
            />


            <h4>Mailing Address</h4>
            <Divider />
            <TextField
              id="mailaddress"
              name="mailaddress"
              onChange={this.handleAddressChanges}
              floatingLabelText="Street Address / P.O. Box"
              value={mailaddress}
              style={styles.customWidth}
              required
            />
            <TextField
              id="mailcity"
              name="mailcity"
              onChange={this.handleCityChanges}
              floatingLabelText="City"
              value={mailcity}
              style={nameInputStyle}
              required
            />
            <TextField
              id="mailstate"
              name="mailstate"
              onChange={this.handleStateChanges}
              floatingLabelText="State"
              value={mailstate}
              style={nameInputStyle}
              required
            />
            <TextField
              id="mailzip"
              onChange={this.handleZipChanges}
              name="mailzip"
              floatingLabelText="Zip Code"
              value={mailzip}
              style={nameInputStyle}
              required
            />
            <TextField
              id="email"
              name="email"
              onChange={this.handleEmailChanges}
              floatingLabelText="Email"
              value={email}
              style={nameInputStyle}
              required
            />

            <TextField
              id="phone"
              onChange={this.handlePhoneChanges}
              name="phone"
              floatingLabelText="Phone"
              value={phone}
              style={nameInputStyle}
              required
            />
            <TextField
              id="altphone"
              name="altphone"
              onChange={this.handlePhoneChanges}
              floatingLabelText="Alternate Phone"
              value={altphone}
              style={nameInputStyle}
              required
            />

            <h4>Personal References</h4>
            <Divider />
            <TextField
              id="refname1"
              name="refname1"
              onChange={this.handleNameChanges}
              floatingLabelText="Refrence Name"
              value={refname1}
              style={phoneInputStyle}
              required
            />
            <TextField
              id="refphone1"
              name="refphone1"
              floatingLabelText="Reference Phone"
              onChange={this.handlePhoneChanges}
              value={refphone1}
              style={phoneInputStyle}
              required
            />

            <TextField
              id="refname2"
              name="refname2"
              onChange={this.handleNameChanges}
              floatingLabelText="Refrence Name"
              value={refname2}
              style={phoneInputStyle}
              required
            />
            <TextField
              id="refphone2"
              name="refphone2"
              floatingLabelText="Reference Phone"
              onChange={this.handlePhoneChanges}
              value={refphone2}
              style={phoneInputStyle}
              required
            />

            <h4>Contact Information</h4>
            <Divider />
            <TextField
              id="cilastname"
              name="cilastname"
              floatingLabelText="Last Name"
              onChange={this.handleNameChanges}
              value={cilastname}
              style={ciNameStyle}
              required
            />
            <TextField
              id="cifirstname"
              name="cifirstname"
              floatingLabelText="First Name"
              onChange={this.handleNameChanges}
              value={cifirstname}
              style={ciNameStyle}
              required
            />
            <TextField
              id="cimiddlename"
              name="cimiddlename"
              floatingLabelText="Middle Name"
              onChange={this.handleNameChanges}
              value={cimiddlename}
              style={ciNameStyle}
              required
            />
            <TextField
              id="cimaidanname"
              name="cimaidanname"
              floatingLabelText="Maiden Name"
              onChange={this.handleNameChanges}
              value={cimaidanname}
              style={ciNameStyle}
              required
            />

            <TextField
              id="contractorpaidas"
              name="contractorpaidas"
              onChange={this.handleNameChanges}
              floatingLabelText="Contractor / Paid as"
              value={contractorpaidas}
              style={phoneInputStyle}
              required
            />
            <TextField
              id="contractorprefix"
              name="contractorprefix"
              onChange={this.handleNameChanges}
              floatingLabelText="Contractor / Paid as Prefix"
              value={contractorprefix}
              style={phoneInputStyle}
              required
            />

            <TextField
              id="ciaddress"
              name="ciaddress"
              onChange={this.handleAddressChanges}
              floatingLabelText="Current Home Address"
              value={ciaddress}
              style={styles.customWidth}
              required
            />

            <TextField
              id="cicity"
              name="cicity"
              onChange={this.handleCityChanges}
              floatingLabelText="Home City"
              value={cicity}
              style={nameInputStyle}
              required
            />

            <TextField
              id="cistate"
              name="cistate"
              onChange={this.handleStateChanges}
              floatingLabelText="Home State"
              value={cistate}
              style={nameInputStyle}
              required
            />

            <TextField
              id="cizip"
              name="cizip"
              onChange={this.handleZipChanges}
              floatingLabelText="Home Zip"
              value={cizip}
              style={nameInputStyle}
              required
            />

            <TextField
              id="busaddress"
              name="busaddress"
              onChange={this.handleAddressChanges}
              floatingLabelText="Business Address"
              value={busaddress}
              style={styles.customWidth}
              required
            />
            <TextField
              id="buscity"
              name="buscity"
              onChange={this.handleCityChanges}
              floatingLabelText="Business City"
              value={buscity}
              style={nameInputStyle}
              required
            />
            <TextField
              id="busstate"
              name="busstate"
              onChange={this.handleStateChanges}
              floatingLabelText="Business State"
              value={busstate}
              style={nameInputStyle}
              required
            />
            <TextField
              id="buszip"
              name="buszip"
              onChange={this.handleZipChanges}
              floatingLabelText="Business Zip"
              value={buszip}
              style={nameInputStyle}
              required
            />
            <TextField
              id="homephone"
              name="homephone"
              onChange={this.handlePhoneChanges}
              floatingLabelText="Home Phone"
              value={homephone}
              style={nameInputStyle}
              required
            />
            <TextField
              id="cellphone"
              name="cellphone"
              onChange={this.handlePhoneChanges}
              floatingLabelText="Cell Phone"
              value={cellphone}
              style={nameInputStyle}
              required
            />
            <TextField
              id="altphone2"
              name="altphone2"
              onChange={this.handlePhoneChanges}
              floatingLabelText="Alt. Phone"
              value={altphone2}
              style={nameInputStyle}
              required
            />
            <TextField
              id="ciemail"
              name="ciemail"
              onChange={this.handleEmailChanges}
              floatingLabelText="Email"
              value={ciemail}
              style={phoneInputStyle}
              required
            />
            <TextField
              id="cissn"
              name="cissn"
              onChange={this.handleSsnChanges}
              floatingLabelText="Social Security Number"
              value={cissn}
              style={phoneInputStyle}
              required
            />
            <TextField
              id="driverlicense"
              name="driverlicense"
              onChange={this.driverlicenseChanges}
              floatingLabelText="Driver License No."
              value={driverlicense}
              style={styles.customWidth}
              required
            />
            <TextField
              id="refperson"
              name="refperson"
              onChange={this.handleNameChanges}
              floatingLabelText="Name of person who reffered you"
              value={refperson}
              style={phoneInputStyle}
              required
            />
            <TextField
              id="ein"
              floatingLabelText="EIN (if applicable)"
              value={ein}
              style={phoneInputStyle}
            />
            <TextField
              id="emergcontact"
              name="emergcontact"
              onChange={this.handlePhoneChanges}
              floatingLabelText="Emergency Contact Name"
              value={emergcontact}
              style={phoneInputStyle}
              required
            />
            <TextField
              id="emergphone"
              name="emergphone"
              onChange={this.handlePhoneChanges}
              floatingLabelText="Emergency Contact Phone Number"
              value={emergphone}
              style={phoneInputStyle}
              required
            />

            <TextField
              id="campaigncity"
              name="campaigncity"
              onChange={this.handleCityChanges}
              floatingLabelText="Campaign City"
              value={campaigncity}
              style={phoneInputStyle}
              required
            />

            <TextField
              id="campaignstate"
              name="campaignstate"
              onChange={this.handleStateChanges}
              floatingLabelText="Campaign State"
              value={campaignstate}
              style={phoneInputStyle}
              required
            />

            <h4>Which of the following apply to you? (check only one)</h4>
            <div style={radioStyles.block}>
              <RadioButtonGroup name="shipSpeed" defaultSelected="indisp">
                <RadioButton
                  value="indisp"
                  label="Individual/Sole Proprietor"
                  style={radioStyles.radioButton}
                />
                <RadioButton
                  value="corporation"
                  label="C Corporation"
                  style={radioStyles.radioButton}
                />
                <RadioButton
                  value="partnership"
                  label="Partnership"
                  checkedIcon={<ActionFavorite style={{color: '#F44336'}} />}
                  uncheckedIcon={<ActionFavoriteBorder />}
                  style={radioStyles.radioButton}
                />
                <RadioButton
                  value="trust"
                  label="Trust/Estate"
                  checkedIcon={<ActionFavorite style={{color: '#F44336'}} />}
                  uncheckedIcon={<ActionFavoriteBorder />}
                  style={radioStyles.radioButton}
                />
                <RadioButton
                  value="llc"
                  label="Limited Liability Company"
                  checkedIcon={<ActionFavorite style={{color: '#F44336'}} />}
                  uncheckedIcon={<ActionFavoriteBorder />}
                  style={radioStyles.radioButton}
                />
              </RadioButtonGroup>
            </div>
            <Divider />
            <h4>Which of the following apply to you? (check only one)</h4>
            <div style={checkboxStyles.block}>
              <Checkbox
                label="I am a citizen of the United States"
                checked={this.state.citizen}
                onCheck={this.updateCitizenCheck.bind(this)}
                style={checkboxStyles.radioButton}
              />
              <Checkbox
                label="A non-citizen national of the United States"
                checked={this.state.noncitizen}
                onCheck={this.nonCitizenCheck.bind(this)}
                style={checkboxStyles.radioButton}
              />
              <Checkbox
                label="A lawful Permannent Residence (provide alien #)"
                checked={this.state.presidence}
                onCheck={this.presidenceCheck.bind(this)}
                style={checkboxStyles.radioButton}
              />
              <Checkbox
                label="An alien authorized to work (provide alien # and admission #)"
                checked={this.state.alienauth}
                onCheck={this.alienauthCheck.bind(this)}
                style={checkboxStyles.radioButton}
              />

            </div>
            <Divider />




            <RaisedButton
              label="SUBMIT"
              labelPosition="after"
              style={styles.customWidth}
              type="submit"
              primary={true}
              icon={<AddBtn />}
            />

          </form>
        </div>
    );
  }
}
const propTypes = {
    className: PropTypes.string,
};

export default Newhire;
