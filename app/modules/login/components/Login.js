// import React from 'react';
import React, { Component, PropTypes } from 'react';
import { Redirect  } from 'react-router';
import { withRouter, BrowserRouter as Router, Route, Link  } from 'react-router-dom';
import axios from 'axios';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import "./login.scss";
// export default withRouter(Login)
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {email: '', password:'', naviagte: false};

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    console.log("Login Component userId", localStorage.getItem('userId'))
  }

  componentDidMount(){
    this.userId = localStorage.getItem('userId')
    console.log("LOGIN COMPONENT MOUNTED:===", this.userId)

    if (this.userId) {
      this.props.history.push({
        pathname: '/dashboard'
      })
    }
  }

	handleEmailChange(event) {
    this.setState({email: event.target.value});
	}

	handlePasswordChange(event) {
    this.setState({password: event.target.value});
	}




	handleSubmit(event) {
    event.preventDefault();

  	fetch("http://localhost:8008/api/auth/login", {
		    method: 'POST',
		    headers: {
		      'Content-Type': 'application/json',
		      'Accept': 'application/json'
		    },
		    body: JSON.stringify({username: this.state.email, password: this.state.password})
		})
		.then(res => res.json())
		.then((result)=>{

    	if (result.length != 0){
				console.log(result)

        localStorage.setItem("userId", result[0]._id)
        localStorage.setItem("loggeduser", JSON.stringify(result[0]))

        this.props.changeDetect()


        console.log("userID", localStorage.getItem('userId'), "user logged", localStorage.getItem('loggeduser') )

			}
    }),
    (error) => {
      console.log(error)
    }
  }

  render() {
    const {email, password} = this.props
    return (
      <div className="loginform">
      <form onSubmit={this.handleSubmit}>
        <label>
          <TextField
            className="loginfield"
            floatingLabelText="Username"
            type="text"
            defaultValue={email}
            ref="email"
            onChange={this.handleEmailChange}
          />
        </label>
        <label>
          <TextField
            className="loginfield"
            floatingLabelText="Password"
            type="password"
            defaultValue={password}
            ref="password"
            onChange={this.handlePasswordChange}
          />
        </label>

        <RaisedButton
          className="submitbutton"
          label="LOGIN"
          labelPosition="after"
          type="submit"
          primary={true}
        />
      </form>
      </div>
    );
	}
}

Login.defaultProps = {
  email: null,
  password: null
}

export default withRouter(Login)
