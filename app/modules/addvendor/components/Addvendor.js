import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import AddBtn from 'material-ui/svg-icons/action/done';

export default class Addvendor extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      address: '',
      fp: '',
      fpc: '',
    };

    this.handleNameChange = this.handleNameChange.bind(this)
    this.handleAddressChange = this.handleAddressChange.bind(this)
    this.handleFpChange = this.handleFpChange.bind(this)
    this.handleFpcChange = this.handleFpcChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleNameChange(event) {
    this.setState({
      name: event.target.value,
    });
  };
  handleAddressChange(event) {
    this.setState({
      address: event.target.value,
    });
  };
  handleFpChange(event) {
    this.setState({
      fp: event.target.value,
    });
  };
  handleFpcChange(event) {
    this.setState({
      fpc: event.target.value,
    });
  };

  handleSubmit(e) {
    e.preventDefault()
    console.log(this.state.name, this.state.address, this.state.fp, this.state.fpc)
    fetch("http://localhost:8008/api/addvendor", {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({name: this.state.name, 
                            address: this.state.address, 
                            focalperson: this.state.fp, 
                            focalpersonnumber: this.state.fpc})
    })
    .then(res => res.json())
    .then((result) => {
      console.log("RESULT FROM ADDING VENDOR:---", result)
      this.setState({
        name: '',
        address: '',
        fp: '',
        fpc: '',
      })
    }),(error) => {
      console.log(err)
    }
  };


  render() {
    return (
      <div>
        <h2>ADD VENDOR</h2>
        <form onSubmit={this.handleSubmit}>
          <TextField
          	id="name"
          	floatingLabelText="Name"
          	value={this.state.name}
            ref="name"
            onChange={this.handleNameChange}
          />
          <TextField
            id="address"
            floatingLabelText="Address"
            ref="address"
            value={this.state.address}
            onChange={this.handleAddressChange}
          />
          <TextField
            id="fp"
            floatingLabelText="Focal Person"
            ref="fp"
            value={this.state.fp}
            onChange={this.handleFpChange}
          />
          <TextField
          	id="fpc"
          	floatingLabelText="Focal Person Contact"
          	value={this.state.fpc}
          	onChange={this.handleFpcChange}
          />

          <RaisedButton
          	label="ADD"
    		    labelPosition="before"
       		  primary={true}
            onClick={this.handleSubmit}
        		icon={<AddBtn />}
      	  />
        </form>
      </div>
    );
  }
}
