import React, { Component, PropTypes } from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Moment from 'react-moment';
import {withRouter} from 'react-router-dom'
import CkeckIcon from 'react-icons/lib/md/check'
// import Ckeck from 'react-icons/lib/md/filter-1'

import CancelIcon from 'react-icons/lib/fa/close'; 
import InstallIcon from 'react-icons/lib/fa/check'; 
import PendingIcon from 'react-icons/lib/fa/exclamation-circle'; 

import Tooltip from '@material-ui/core/Tooltip';
import DatePicker from 'material-ui/DatePicker';

import Button from '@material-ui/core/Button';
import ClearAll from 'react-icons/lib/md/clear-all';
import Divider from '@material-ui/core/Divider';


import TrippleIcon from 'react-icons/lib/md/filter-3'
import DoubleIcon from 'react-icons/lib/md/filter-2'
import SingleIcon from 'react-icons/lib/md/filter-1'
import MoneyIcon from 'react-icons/lib/md/attach-money'
import IoEject from "react-icons/lib/io/eject"

import * as _ from 'lodash'
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from 'react-icons/lib/md/arrow-back';

const styles = {
  customWidth: {
    width: '100%',
    display: 'Inline-block',
  },
  select: {
    minWidth: '50%',
  },
};


class Allorders extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      allorders: [],
      allmanagers: [],
      allreps: [],
      manager: null,
      rep: null,
      trippleCount: 0,
      doubleCount: 0,
      singleCount: 0,
      managername: null,
      controlledDate: null,
      trippeleComission: null,
      tvInternetComission: null,
      tvPhoneComission: null,
      internetPhoneComission: null,
      internetComission: null,
      tvComission: null,
      phoneComission: null,
      querycomission: null,
      pages: [],
      status: null,
    }
    this.handleManagerChange = this.handleManagerChange.bind(this)
    this.handleRepChange = this.handleRepChange.bind(this)
    this.handleDateChange = this.handleDateChange.bind(this)
    this.clearFilter = this.clearFilter.bind(this)
    this.handleStatusChange = this.handleStatusChange.bind(this)
    this.loggeduser = JSON.parse(localStorage.getItem('loggeduser'))
  }

  handleManagerChange(event, index, value){
    this.setState({manager: value, 
                   managername: event.target.textContent, 
                   rep: null, 
                   status: null,}, () => {
      console.log("MANAGER VALUE:---", this.state.manager, "\nMANAGER NAME:---", this.state.managername)
      if (this.state.manager){

        fetch("http://localhost:8008/api/managerorders", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({userid: this.state.manager})
        })
        .then(res => res.json())
        .then((result)=>{
          console.log("ALL ORDERS ON MANAGER CHANGE:----", result)
          let orders = result
          this.setState({allorders: orders[0].orders}, () => {
            this.countPlays(this.state.allorders)
          })
        }),
        (error) => {
          console.log(error)
        }
      }else{
        fetch("http://localhost:8008/api/allorderentry")
        .then(res => res.json())
        .then((result) => {
            this.setState({
              allorders: result[0].orders
            }, () => {
              console.log("STATE FROM ALL ORDERS:-----", this.state.allorders)
              this.countPlays(this.state.allorders)
            })
          },
          (error) => {
            console.log(error)
          }
        )
      }
    });
  }
  handleRepChange(event, index, value){
    this.setState({rep: value, 
                   manager: null, 
                   managername: null, 
                   status: null}, () => {
      console.log("REP VALUE:---", this.state.rep)
      if (this.state.rep){

        fetch("http://localhost:8008/api/userorders", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({userid: this.state.rep})
        })
        .then(res => res.json())
        .then((result)=>{
          console.log("ALL ORDERS ON REP CHANGE:----", result)
          let orders = result
          this.setState({allorders: orders[0].orders}, () => {
            this.countPlays(this.state.allorders)
          })
        }),
        (error) => {
          console.log(error)
        }
      }else{
        fetch("http://localhost:8008/api/allorderentry")
        .then(res => res.json())
        .then(
          (result) => {
              this.setState({
                allorders: result[0].orders
              }, () => {
                console.log("STATE FROM ALL ORDERS:-----", this.state.allorders)
                this.countPlays(this.state.allorders)
              })
          },
          (error) => {
            console.log(error)
          }
        )
      }
    });
  }

  handleStatusChange(event, index, value){
    this.setState({status: value, 
                   rep: null, 
                   manager: null, 
                   managername: null}, () => {
      console.log("STATUS VALUE:---", this.state.status)
      if (this.state.status){
        fetch("http://localhost:8008/api/filterbystatus", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({status: this.state.status})
        })
        .then(res => res.json())
        .then((result)=>{
          console.log("ALL ORDERS ON STATUS CHANGE:----", result)
          let orders = result
          this.setState({allorders: orders[0].orders}, () => {
            this.countPlays(this.state.allorders)
          })
        }),
        (error) => {
          console.log(error)
        }
      }else{
        fetch("http://localhost:8008/api/allorderentry")
        .then(res => res.json())
        .then(
          (result) => {
              this.setState({
                allorders: result[0].orders
              }, () => {
                console.log("STATE FROM ALL ORDERS:-----", this.state.allorders)
                this.countPlays(this.state.allorders)
              })
          },
          (error) => {
            console.log(error)
          }
        )
      }
    });
  }


  countPlays(allorders){
    
    console.log("ALL ORDERS IN COUNT FUCNTION:---", allorders)
    
    let trippleCount = 0
    let doubleCount = 0
    let singleCount = 0
    let tvInternetCount = 0
    let tvPhoneCount = 0
    let internetPhoneCount = 0
    let internetCount = 0
    let phoneCount = 0
    let tvCount = 0    


    let cbTripple = 0
    let cbDouble = 0
    let cbTvInternet = 0
    let cbTvPhone = 0
    let cbInternetPhone = 0
    let cbInternet = 0
    let cbPhone = 0
    let cbTv = 0
    
    for (let obj of allorders){
      if (obj.status != "cancel"){

        if (obj.noofunits === 3){
          trippleCount++
        }
        if (obj.noofunits === 2){
          doubleCount++
          if (obj.tv === true && obj.internet === true)
            tvInternetCount++
          else if (obj.tv === true && obj.phone === true)
            tvPhoneCount++
          else if (obj.internet === true && obj.phone === true)
            internetPhoneCount++
        }
        if (obj.noofunits === 1){
          singleCount++
          if (obj.tv === true)
            tvCount++        
          if (obj.internet === true)
            internetCount++        
          if (obj.phone === true)
            phoneCount++
        }
      }else{
        if (obj.chargeback == true && 
            this.loggeduser.role == 'admin' && 
            (this.state.manager == null && this.state.rep == null)){
          if (obj.noofunits === 3){
            trippleCount++
            cbTripple++
          }
          if (obj.noofunits === 2){
            doubleCount++
            cbDouble++
            if (obj.tv === true && obj.internet === true){
              tvInternetCount++
              cbTvInternet++
            }
            else if (obj.tv === true && obj.phone === true){
              tvPhoneCount++
              cbTvPhone++
            }
            else if (obj.internet === true && obj.phone === true){
              internetPhoneCount++
              cbInternetPhone++
            }
          }
          if (obj.noofunits === 1){
            singleCount++
            if (obj.tv === true)
              tvCount++        
              cbTv++        
            if (obj.internet === true)
              internetCount++        
              cbInternet++        
            if (obj.phone === true)
              cbPhone++
          }
        }
      }
    }
    
    console.log("TRIPPLE COUNT:---", trippleCount, 
                "\CHARGE BACK TRIPPLE COUNT:---", cbTripple, 
                "\nDouble COUNT:---", doubleCount, 
                "\CHARGE BACK DOUBLE COUNT:---", cbDouble, 
                "\nSingle Count:---", singleCount,
                "\nTV INTERNET Count:---", tvInternetCount,
                "\nTV INTERNET CHAGEBACK Count:---", cbTvInternet,
                "\nTV PHONE Count:---", tvPhoneCount,
                "\nTV PHONE CHARGE BACK Count:---", cbTvPhone,
                "\nINTERBET PHONE Count:---", internetPhoneCount,
                "\nINTERBET PHONE CHARGEBACK Count:---", cbInternetPhone,
                "\nSingle TV Count:---", tvCount,
                "\nSingle TV CHARGEBACK Count:---", cbTv,
                "\nSingle INTERNET Count:---", internetCount,
                "\nSingle INTERNET CHARGEBACK Count:---", cbInternet,
                "\nSingle PHONE Count:---", phoneCount,
                "\nSingle PHONE CHARGEBACK Count:---", cbPhone,
                )

      if (this.loggeduser.role == 'admin' && this.state.rep == null && this.state.manager == null){
        console.log("READER FROM ADMIN")
        this.setState({trippleCount: trippleCount, 
                      doubleCount: doubleCount, 
                      singleCount: singleCount,}, () => {

          
          let trippeleComission = 0
          let doubleComission = 0
          let singleComission = 0
          let tvInternetComission = 0
          let tvPhoneComission = 0
          let internetPhoneComission = 0
          let internetComission = 0
          let tvComission = 0
          let phoneComission = 0

          if (this.state.trippleCount > 0 && this.state.trippleCount <= 19){
            if (cbTripple){
              if (cbTripple > 0 && cbTripple <=19){
                trippeleComission = (100 * this.state.trippleCount) - 0.15*100
              }
            }else{
              trippeleComission = (100 * this.state.trippleCount)
            }
          }else if(this.state.trippleCount >= 20 && this.state.trippleCount <= 99){
            if (cbTripple){
              if (cbTripple > 0 && cbTripple <=19){
                trippeleComission = (190 * this.state.trippleCount) - 0.15*100
              }else if (cbTripple >=20 && cbTripple <=99){
                trippeleComission = (190 * this.state.trippleCount) - 0.15*190
              }
            }else{
              trippeleComission = (190 * this.state.trippleCount)
            }
          }else if(this.state.trippleCount >= 100 && this.state.trippleCount <= 299){
            if (cbTripple){
              if (cbTripple > 0 && cbTripple <=19){
                trippeleComission = (220 * this.state.trippleCount) - 0.15*100
              }else if (cbTripple >=20 && cbTripple <=99){
                trippeleComission = (220 * this.state.trippleCount) - 0.15*190
              }else if (cbTripple >=100 && cbTripple <=299){
                trippeleComission = (220 * this.state.trippleCount) - 0.15*220
              }
            }else{
              trippeleComission = (220 * this.state.trippleCount)
            }
          }else if(this.state.trippleCount >= 300 && this.state.trippleCount <= 999){
            if (cbTripple){
              if (cbTripple > 0 && cbTripple <=19){
                trippeleComission = (250 * this.state.trippleCount) - 0.15*100
              }else if (cbTripple >=20 && cbTripple <=99){
                trippeleComission = (250 * this.state.trippleCount) - 0.15*190
              }else if (cbTripple >=100 && cbTripple <=299){
                trippeleComission = (250 * this.state.trippleCount) - 0.15*220
              }else if (cbTripple >=300 && cbTripple <=999){
                trippeleComission = (250 * this.state.trippleCount) - 0.15*250
              }
            }else{
              trippeleComission = (250 * this.state.trippleCount)
            }
          }else if(this.state.trippleCount >= 1000 && this.state.trippleCount <= 2499){
            if (cbTripple){
              if (cbTripple > 0 && cbTripple <=19){
                trippeleComission = (275 * this.state.trippleCount) - 0.15*100
              }else if (cbTripple >=20 && cbTripple <=99){
                trippeleComission = (275 * this.state.trippleCount) - 0.15*190
              }else if (cbTripple >=100 && cbTripple <=299){
                trippeleComission = (275 * this.state.trippleCount) - 0.15*220
              }else if (cbTripple >=300 && cbTripple <=999){
                trippeleComission = (275 * this.state.trippleCount) - 0.15*250
              }else if (cbTripple >= 1000 && cbTripple <= 2499){
                trippeleComission = (275 * this.state.trippleCount) - 0.15*275
              }
            }else{
              trippeleComission = (275 * this.state.trippleCount)
            }
          }else if(this.state.trippleCount >= 2500){
            if (cbTripple){
              if (cbTripple > 0 && cbTripple <=19){
                trippeleComission = (300 * this.state.trippleCount) - 0.15*100
              }else if (cbTripple >=20 && cbTripple <=99){
                trippeleComission = (300 * this.state.trippleCount) - 0.15*190
              }else if (cbTripple >=100 && cbTripple <=299){
                trippeleComission = (300 * this.state.trippleCount) - 0.15*220
              }else if (cbTripple >=300 && cbTripple <=999){
                trippeleComission = (300 * this.state.trippleCount) - 0.15*250
              }else if (cbTripple >= 1000 && cbTripple <= 2499){
                trippeleComission = (300 * this.state.trippleCount) - 0.15*275
              }else if (cbTripple >= 2500){
                trippeleComission = (300 * this.state.trippleCount) - 0.15*300
              }
            }else{
              trippeleComission = (300 * this.state.trippleCount)
            }
          }

          if (tvInternetCount > 0 && tvInternetCount <= 19){
            if (cbTvInternet){
              if (cbTvInternet > 0 && cbTvInternet <= 19){
                tvInternetComission = 60 * tvInternetCount - 0.15*60
              }
            }else{
              tvInternetComission = 60 * tvInternetCount
            }
          }else if(tvInternetCount >= 20 && tvInternetCount <= 99){
            if (cbTvInternet){
              if (cbTvInternet > 0 && cbTvInternet <= 19){
                tvInternetComission = 110 * tvInternetCount - 0.15*60
              }else if (cbTvInternet >= 20 && cbTvInternet <= 99){
                tvInternetComission = 110 * tvInternetCount - 0.15*110
              }
            }else{
              tvInternetComission = 110 * tvInternetCount
            }
          }else if(tvInternetCount >= 100 && tvInternetCount <= 299){
            // CHARGE BACK
            if (cbTvInternet){
              if (cbTvInternet > 0 && cbTvInternet <= 19){
                tvInternetComission = 130 * tvInternetCount - 0.15*60
              }else if (cbTvInternet >= 20 && cbTvInternet <= 99){
                tvInternetComission = 130 * tvInternetCount - 0.15*110
              }else if (cbTvInternet >= 100 && cbTvInternet <= 299){
                tvInternetComission = 130 * tvInternetCount - 0.15*130
              }
            }else{
              // NO CHARGE BACK
              tvInternetComission = 130 * tvInternetCount
            }
          }else if(tvInternetCount >= 300 && tvInternetCount <= 999){
            if (cbTvInternet){
              if (cbTvInternet > 0 && cbTvInternet <= 19){
                tvInternetComission = 150 * tvInternetCount - 0.15*60
              }else if (cbTvInternet >= 20 && cbTvInternet <= 99){
                tvInternetComission = 150 * tvInternetCount - 0.15*110
              }else if (cbTvInternet >= 100 && cbTvInternet <= 299){
                tvInternetComission = 150 * tvInternetCount - 0.15*130
              }else if (cbTvInternet >= 300 && cbTvInternet <= 999){
                tvInternetComission = 150 * tvInternetCount - 0.15*150
              }
            }else{
              // NO CHARGE BACK
              tvInternetComission = 150 * tvInternetCount
            }
          }else if(tvInternetCount >= 1000 && tvInternetCount <= 2499){
            if (cbTvInternet){
              if (cbTvInternet > 0 && cbTvInternet <= 19){
                tvInternetComission = 160 * tvInternetCount - 0.15*60
              }else if (cbTvInternet >= 20 && cbTvInternet <= 99){
                tvInternetComission = 160 * tvInternetCount - 0.15*110
              }else if (cbTvInternet >= 100 && cbTvInternet <= 299){
                tvInternetComission = 160 * tvInternetCount - 0.15*130
              }else if (cbTvInternet >= 300 && cbTvInternet <= 999){
                tvInternetComission = 160 * tvInternetCount - 0.15*150
              }else if (cbTvInternet >= 1000 && cbTvInternet <= 2499){
                tvInternetComission = 160 * tvInternetCount - 0.15*160
              }
            }else{
              // NO CHARGE BACK
              tvInternetComission = 160 * tvInternetCount
            }
          }else if(tvInternetCount >= 2500){
            if (cbTvInternet){
              if (cbTvInternet > 0 && cbTvInternet <= 19){
                tvInternetComission = 170 * tvInternetCount - 0.15*60
              }else if (cbTvInternet >= 20 && cbTvInternet <= 99){
                tvInternetComission = 170 * tvInternetCount - 0.15*110
              }else if (cbTvInternet >= 100 && cbTvInternet <= 299){
                tvInternetComission = 170 * tvInternetCount - 0.15*130
              }else if (cbTvInternet >= 300 && cbTvInternet <= 999){
                tvInternetComission = 170 * tvInternetCount - 0.15*150
              }else if (cbTvInternet >= 1000 && cbTvInternet <= 2499){
                tvInternetComission = 170 * tvInternetCount - 0.15*160
              }else if (cbTvInternet >= 2500){
                tvInternetComission = 170 * tvInternetCount - 0.15*170
              }
            }else{
              // NO CHARGE BACK
              tvInternetComission = 170 * tvInternetCount
            }
          }

          if (tvPhoneCount > 0 && tvPhoneCount <= 19){
            if (cbTvPhone){
              if (cbTvPhone > 0 && cbTvPhone <= 19){
                tvPhoneComission = 60 * tvPhoneCount - 0.15*60
              }
            }else{
              tvPhoneComission = 60 * tvPhoneCount
            }
          }else if(tvPhoneCount >= 20 && tvPhoneCount <= 99){
            if (cbTvPhone){
              if (cbTvPhone > 0 && cbTvPhone <= 19){
                tvPhoneComission = 110 * tvPhoneCount - 0.15*60
              }else if (cbTvPhone >= 20 && cbTvPhone <= 99){
                tvPhoneComission = 110 * tvPhoneCount - 0.15*110
              }

            }else{
              tvPhoneComission = 110 * tvPhoneCount
            }

          }else if(tvPhoneCount >= 100 && tvPhoneCount <= 299){
            if (cbTvPhone){
              if (cbTvPhone > 0 && cbTvPhone <= 19){
                tvPhoneComission = 130 * tvPhoneCount - 0.15*60
              }else if (cbTvPhone >= 20 && cbTvPhone <= 99){
                tvPhoneComission = 130 * tvPhoneCount - 0.15*110
              }else if (cbTvPhone >= 100 && cbTvPhone <= 299){
                tvPhoneComission = 130 * tvPhoneCount - 0.15*130
              }

            }else{
              tvPhoneComission = 130 * tvPhoneCount
            }

          }else if(tvPhoneCount >= 300 && tvPhoneCount <= 999){
            if (cbTvPhone){
              if (cbTvPhone > 0 && cbTvPhone <= 19){
                tvPhoneComission = 150 * tvPhoneCount - 0.15*60
              }else if (cbTvPhone >= 20 && cbTvPhone <= 99){
                tvPhoneComission = 150 * tvPhoneCount - 0.15*110
              }else if (cbTvPhone >= 100 && cbTvPhone <= 299){
                tvPhoneComission = 150 * tvPhoneCount - 0.15*130
              }else if (cbTvPhone >= 300 && cbTvPhone <= 999){
                tvPhoneComission = 150 * tvPhoneCount - 0.15*150
              }

            }else{
              tvPhoneComission = 150 * tvPhoneCount
            }

          }else if(tvPhoneCount >= 1000 && tvPhoneCount <= 2499){
           if (cbTvPhone){
              if (cbTvPhone > 0 && cbTvPhone <= 19){
                tvPhoneComission = 160 * tvPhoneCount - 0.15*60
              }else if (cbTvPhone >= 20 && cbTvPhone <= 99){
                tvPhoneComission = 160 * tvPhoneCount - 0.15*110
              }else if (cbTvPhone >= 100 && cbTvPhone <= 299){
                tvPhoneComission = 160 * tvPhoneCount - 0.15*130
              }else if (cbTvPhone >= 300 && cbTvPhone <= 999){
                tvPhoneComission = 160 * tvPhoneCount - 0.15*150
              }else if (cbTvPhone >= 1000 && cbTvPhone <= 2499){
                tvPhoneComission = 160 * tvPhoneCount - 0.15*160
              }

            }else{
              tvPhoneComission = 160 * tvPhoneCount
            }

          }else if(tvPhoneCount >= 2500){
           if (cbTvPhone){
              if (cbTvPhone > 0 && cbTvPhone <= 19){
                tvPhoneComission = 170 * tvPhoneCount - 0.15*60
              }else if (cbTvPhone >= 20 && cbTvPhone <= 99){
                tvPhoneComission = 170 * tvPhoneCount - 0.15*110
              }else if (cbTvPhone >= 100 && cbTvPhone <= 299){
                tvPhoneComission = 170 * tvPhoneCount - 0.15*130
              }else if (cbTvPhone >= 300 && cbTvPhone <= 999){
                tvPhoneComission = 170 * tvPhoneCount - 0.15*150
              }else if (cbTvPhone >= 1000 && cbTvPhone <= 2499){
                tvPhoneComission = 170 * tvPhoneCount - 0.15*160
              }else if (cbTvPhone >= 2500){
                tvPhoneComission = 170 * tvPhoneCount - 0.15*170
              }

            }else{
              tvPhoneComission = 170 * tvPhoneCount
            }
          }

          if (internetPhoneCount > 0 && internetPhoneCount <= 19){
            if (cbInternetPhone){
              if (cbInternetPhone > 0 && cbInternetPhone <= 19){
                internetPhoneComission = 45 * internetPhoneCount - 0.15*45
              }
            }else{
              internetPhoneComission = 45 * internetPhoneCount
            }
          }else if(internetPhoneCount >= 20 && internetPhoneCount <= 99){
            if (cbInternetPhone){
              if (cbInternetPhone > 0 && cbInternetPhone <= 19){
                internetPhoneComission = 85 * internetPhoneCount - 0.15*45
              }else if (cbInternetPhone >= 20 && cbInternetPhone <= 99){
                internetPhoneComission = 85 * internetPhoneCount - 0.15*85
              }
            }else{
              internetPhoneComission = 85 * internetPhoneCount
            }
          }else if(internetPhoneCount >= 100 && internetPhoneCount <= 299){
            if (cbInternetPhone){
              if (cbInternetPhone > 0 && cbInternetPhone <= 19){
                internetPhoneComission = 100 * internetPhoneCount - 0.15*45
              }else if (cbInternetPhone >= 20 && cbInternetPhone <= 99){
                internetPhoneComission = 100 * internetPhoneCount - 0.15*85
              }else if (cbInternetPhone >= 100 && cbInternetPhone <= 299){
                internetPhoneComission = 100 * internetPhoneCount - 0.15*100
              }

            }else{
              internetPhoneComission = 100 * internetPhoneCount
            }
          }else if(internetPhoneCount >= 300 && internetPhoneCount <= 999){
            if (cbInternetPhone){
              if (cbInternetPhone > 0 && cbInternetPhone <= 19){
                internetPhoneComission = 115 * internetPhoneCount - 0.15*45
              }else if (cbInternetPhone >= 20 && cbInternetPhone <= 99){
                internetPhoneComission = 115 * internetPhoneCount - 0.15*85
              }else if (cbInternetPhone >= 100 && cbInternetPhone <= 299){
                internetPhoneComission = 115 * internetPhoneCount - 0.15*100
              }else if (cbInternetPhone >= 300 && cbInternetPhone <= 999){
                internetPhoneComission = 115 * internetPhoneCount - 0.15*115
              }
            }else{
              internetPhoneComission = 115 * internetPhoneCount
            }
          }else if(internetPhoneCount >= 1000 && internetPhoneCount <= 2499){
            if (cbInternetPhone){
              if (cbInternetPhone > 0 && cbInternetPhone <= 19){
                internetPhoneComission = 125 * internetPhoneCount - 0.15*45
              }else if (cbInternetPhone >= 20 && cbInternetPhone <= 99){
                internetPhoneComission = 125 * internetPhoneCount - 0.15*85
              }else if (cbInternetPhone >= 100 && cbInternetPhone <= 299){
                internetPhoneComission = 125 * internetPhoneCount - 0.15*100
              }else if (cbInternetPhone >= 300 && cbInternetPhone <= 999){
                internetPhoneComission = 125 * internetPhoneCount - 0.15*115
              }else if (cbInternetPhone >= 1000 && cbInternetPhone <= 2499){
                internetPhoneComission = 125 * internetPhoneCount - 0.15*125
              }
            }else{
              internetPhoneComission = 125 * internetPhoneCount
            }
          }else if(internetPhoneCount >= 2500){
            if (cbInternetPhone){
              if (cbInternetPhone > 0 && cbInternetPhone <= 19){
                internetPhoneComission = 135 * internetPhoneCount - 0.15*45
              }else if (cbInternetPhone >= 20 && cbInternetPhone <= 99){
                internetPhoneComission = 135 * internetPhoneCount - 0.15*85
              }else if (cbInternetPhone >= 100 && cbInternetPhone <= 299){
                internetPhoneComission = 135 * internetPhoneCount - 0.15*100
              }else if (cbInternetPhone >= 300 && cbInternetPhone <= 999){
                internetPhoneComission = 135 * internetPhoneCount - 0.15*115
              }else if (cbInternetPhone >= 1000 && cbInternetPhone <= 2499){
                internetPhoneComission = 135 * internetPhoneCount - 0.15*125
              }else if (cbInternetPhone >= 2500){
                internetPhoneComission = 135 * internetPhoneCount - 0.15*135
              }
            }else{
              // internetPhoneComission = 125 * internetPhoneCount
              internetPhoneComission = 135 * internetPhoneCount
            }
          }

          if (tvCount > 0 && tvCount <= 19){
            if (cbTv){
              if (cbTv > 0 && cbTv <= 19){
                tvComission = 35 * tvCount - 0.15*35
              }
            }else{
              tvComission = 35 * tvCount
            }
          }else if(tvCount >= 20 && tvCount <= 99){
            if (cbTv){
              if (cbTv > 0 && cbTv <= 19){
                tvComission = 60 * tvCount - 0.15*35
              }else if (cbTv >= 20 && cbTv <= 99){
                tvComission = 60 * tvCount - 0.15*60
              }
            }else{
              tvComission = 60 * tvCount
            }
          }else if(tvCount >= 100 && tvCount <= 299){
            if (cbTv){
              if (cbTv > 0 && cbTv <= 19){
                tvComission = 65 * tvCount - 0.15*35
              }else if (cbTv >= 20 && cbTv <= 99){
                tvComission = 65 * tvCount - 0.15*60
              }else if (cbTv >= 100 && cbTv <= 299){
                tvComission = 65 * tvCount - 0.15*65
              }
            }else{
              tvComission = 65 * tvCount
            }
          }else if(tvCount >= 300 && tvCount <= 999){
            if (cbTv){
              if (cbTv > 0 && cbTv <= 19){
                tvComission = 70 * tvCount - 0.15*35
              }else if (cbTv >= 20 && cbTv <= 99){
                tvComission = 70 * tvCount - 0.15*60
              }else if (cbTv >= 100 && cbTv <= 299){
                tvComission = 70 * tvCount - 0.15*65
              }else if (cbTv >= 300 && cbTv <= 999){
                tvComission = 70 * tvCount - 0.15*70
              }

            }else{
              tvComission = 70 * tvCount
            }
          }else if(tvCount >= 1000 && tvCount <= 2499){
            if (cbTv){
              if (cbTv > 0 && cbTv <= 19){
                tvComission = 80 * tvCount - 0.15*35
              }else if (cbTv >= 20 && cbTv <= 99){
                tvComission = 80 * tvCount - 0.15*60
              }else if (cbTv >= 100 && cbTv <= 299){
                tvComission = 80 * tvCount - 0.15*65
              }else if (cbTv >= 300 && cbTv <= 999){
                tvComission = 80 * tvCount - 0.15*70
              }else if (cbTv >= 1000 && cbTv <= 2499){
                tvComission = 80 * tvCount - 0.15*80
              }
            }else{
              tvComission = 80 * tvCount
            }
          }else if(tvCount >= 2500){
            if (cbTv){
              if (cbTv > 0 && cbTv <= 19){
                tvComission = 90 * tvCount - 0.15*35
              }else if (cbTv >= 20 && cbTv <= 99){
                tvComission = 90 * tvCount - 0.15*60
              }else if (cbTv >= 100 && cbTv <= 299){
                tvComission = 90 * tvCount - 0.15*65
              }else if (cbTv >= 300 && cbTv <= 999){
                tvComission = 90 * tvCount - 0.15*70
              }else if (cbTv >= 1000 && cbTv <= 2499){
                tvComission = 90 * tvCount - 0.15*80
              }else if (cbTv >= 2500){
                tvComission = 90 * tvCount - 0.15*90
              }
            }else{
              tvComission = 90 * tvCount
            }
          }

          if (phoneCount > 0 && phoneCount <= 19){
            if (cbPhone){
              if (cbPhone > 0 && cbPhone <= 19){
                phoneComission = 25 * phoneCount - 0.15*25
              }
            }else{
              phoneComission = 25 * phoneCount
            }
          }else if(phoneCount >= 20 && phoneCount <= 99){
            if (cbPhone){
              if (cbPhone > 0 && cbPhone <= 19){
                phoneComission = 50 * phoneCount - 0.15*25
              }else if (cbPhone >= 20 && cbPhone <= 99){
                phoneComission = 50 * phoneCount - 0.15*50
              }
            }else{
              phoneComission = 50 * phoneCount
            }
          }else if(phoneCount >= 100 && phoneCount <= 299){
            if (cbPhone){
              if (cbPhone > 0 && cbPhone <= 19){
                phoneComission = 55 * phoneCount - 0.15*25
              }else if (cbPhone >= 20 && cbPhone <= 99){
                phoneComission = 55 * phoneCount - 0.15*50
              }else if (cbPhone >= 100 && cbPhone <= 299){
                phoneComission = 55 * phoneCount - 0.15*55
              }
            }else{
              phoneComission = 55 * phoneCount
            }
          }else if(phoneCount >= 300 && phoneCount <= 999){
            if (cbPhone){
              if (cbPhone > 0 && cbPhone <= 19){
                phoneComission = 60 * phoneCount - 0.15*25
              }else if (cbPhone >= 20 && cbPhone <= 99){
                phoneComission = 60 * phoneCount - 0.15*50
              }else if (cbPhone >= 100 && cbPhone <= 299){
                phoneComission = 60 * phoneCount - 0.15*55
              }else if (cbPhone >= 300 && cbPhone <= 999){
                phoneComission = 60 * phoneCount - 0.15*60
              }
            }else{
              phoneComission = 60 * phoneCount
            }
          }else if(phoneCount >= 1000 && phoneCount <= 2499){
            if (cbPhone){
              if (cbPhone > 0 && cbPhone <= 19){
                phoneComission = 70 * phoneCount - 0.15*25
              }else if (cbPhone >= 20 && cbPhone <= 99){
                phoneComission = 70 * phoneCount - 0.15*50
              }else if (cbPhone >= 100 && cbPhone <= 299){
                phoneComission = 70 * phoneCount - 0.15*55
              }else if (cbPhone >= 300 && cbPhone <= 999){
                phoneComission = 70 * phoneCount - 0.15*60
              }else if (cbPhone >= 1000 && cbPhone <= 2499){
                phoneComission = 70 * phoneCount - 0.15*70
              }
            }else{
              phoneComission = 70 * phoneCount
            }
          }else if(phoneCount >= 2500){
            if (cbPhone){
              if (cbPhone > 0 && cbPhone <= 19){
                phoneComission = 80 * phoneCount - 0.15*25
              }else if (cbPhone >= 20 && cbPhone <= 99){
                phoneComission = 80 * phoneCount - 0.15*50
              }else if (cbPhone >= 100 && cbPhone <= 299){
                phoneComission = 80 * phoneCount - 0.15*55
              }else if (cbPhone >= 300 && cbPhone <= 999){
                phoneComission = 80 * phoneCount - 0.15*60
              }else if (cbPhone >= 1000 && cbPhone <= 2499){
                phoneComission = 80 * phoneCount - 0.15*70
              }else if (cbPhone >= 2500){
                phoneComission = 80 * phoneCount - 0.15*80
              }
            }else{
              phoneComission = 80 * phoneCount
            }
          }

          if (internetCount > 0 && internetCount <= 19){
            if (cbInternet){
              if (cbInternet > 0 && cbInternet <= 19){
                internetComission = 25 * internetCount - 0.15*25
              }
            }else{
              internetComission = 25 * internetCount
            }
          }else if(internetCount >= 20 && internetCount <= 99){
            if (cbInternet){
              if (cbInternet > 0 && cbInternet <= 19){
                internetComission = 50 * internetCount - 0.15*25
              }else if (cbInternet >= 20 && cbInternet <= 99){
                internetComission = 50 * internetCount - 0.15*50
              }
            }else{
              internetComission = 50 * internetCount
            }
          }else if(internetCount >= 100 && internetCount <= 299){
            if (cbInternet){
              if (cbInternet > 0 && cbInternet <= 19){
                internetComission = 55 * internetCount - 0.15*25
              }else if (cbInternet >= 20 && cbInternet <= 99){
                internetComission = 55 * internetCount - 0.15*50
              }else if (cbInternet >= 100 && cbInternet <= 299){
                internetComission = 55 * internetCount - 0.15*55
              }
            }else{
              internetComission = 55 * internetCount
            }
          }else if(internetCount >= 300 && internetCount <= 999){
            if (cbInternet){
              if (cbInternet > 0 && cbInternet <= 19){
                internetComission = 60 * internetCount - 0.15*25
              }else if (cbInternet >= 20 && cbInternet <= 99){
                internetComission = 60 * internetCount - 0.15*50
              }else if (cbInternet >= 100 && cbInternet <= 299){
                internetComission = 60 * internetCount - 0.15*55
              }else if (cbInternet >= 300 && cbInternet <= 999){
                internetComission = 60 * internetCount - 0.15*60
              }
            }else{
              internetComission = 60 * internetCount
            }
          }else if(internetCount >= 1000 && internetCount <= 2499){
            if (cbInternet){
              if (cbInternet > 0 && cbInternet <= 19){
                internetComission = 70 * internetCount - 0.15*25
              }else if (cbInternet >= 20 && cbInternet <= 99){
                internetComission = 70 * internetCount - 0.15*50
              }else if (cbInternet >= 100 && cbInternet <= 299){
                internetComission = 70 * internetCount - 0.15*55
              }else if (cbInternet >= 300 && cbInternet <= 999){
                internetComission = 70 * internetCount - 0.15*60
              }else if (cbInternet >= 1000 && cbInternet <= 2499){
                internetComission = 70 * internetCount - 0.15*70
              }
            }else{
              internetComission = 70 * internetCount
            }
          }else if(internetCount >= 2500){
            if (cbInternet){
              if (cbInternet > 0 && cbInternet <= 19){
                internetComission = 80 * internetCount - 0.15*25
              }else if (cbInternet >= 20 && cbInternet <= 99){
                internetComission = 80 * internetCount - 0.15*50
              }else if (cbInternet >= 100 && cbInternet <= 299){
                internetComission = 80 * internetCount - 0.15*55
              }else if (cbInternet >= 300 && cbInternet <= 999){
                internetComission = 80 * internetCount - 0.15*60
              }else if (cbInternet >= 1000 && cbInternet <= 2499){
                internetComission = 80 * internetCount - 0.15*70
              }else if (cbInternet >= 2500){
                internetComission = 80 * internetCount - 0.15*80
              }
            }else{
              internetComission = 80 * internetCount
            }
          }

          this.setState({trippeleComission: trippeleComission, 
                      tvInternetComission: tvInternetComission, 
                      tvPhoneComission: tvPhoneComission,
                      internetPhoneComission: internetPhoneComission,
                      tvComission: tvComission,
                      internetComission: internetComission,
                      phoneComission: phoneComission,}, () => {

            console.log("TRIPPLE COMMISIION:---", this.state.trippeleComission, 
                        "\nTV INTERNET COMMISIION:---", this.state.tvInternetComission,
                        "\nTV PHONE COMMISIION:---", this.state.tvPhoneComission,
                        "\nINTERBET PHONE COMMISIION:---", this.state.internetPhoneComission,
                        "\nSingle TV COMMISIION:---", this.state.tvComission,
                        "\nSingle INTERNET COMMISIION:---", this.state.internetComission,
                        "\nSingle PHONE COMMISIION:---", this.state.phoneComission,)
          })
        })
      }


      if (this.loggeduser.role == 'admin' && 
            (this.state.manager != null || this.state.rep != null)){
        // console.log("READER FROM USER")
        let filterRep = null
        let user = null
        let type = null
        if (this.state.manager == null){
          filterRep = _.filter(this.state.allreps, {"_id":this.state.rep})
          console.log("REP TO FIND:--", filterRep[0])
          user = filterRep[0].manager
          type = 'team'
        }else if (this.state.rep == null){
          filterRep = _.filter(this.state.allmanagers, {"_id":this.state.manager})
          console.log("REP TO FIND:--", filterRep[0])
          user = filterRep[0]._id
          type = 'manager'
        }
        fetch("http://localhost:8008/api/getteamcomission", {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({user: user, type: type})
        })
        .then(res => res.json())
        .then((result)=>{
          this.setState({querycomission: result[0].comission[0],
                        trippleCount: trippleCount, 
                        doubleCount: doubleCount, 
                        singleCount: singleCount,}, () => {
            console.log("SINGLE COMISSION:------", this.state.querycomission)
            let trippeleComission = 0
            let doubleComission = 0
            let singleComission = 0
            let tvInternetComission = 0
            let tvPhoneComission = 0
            let internetPhoneComission = 0
            let internetComission = 0
            let tvComission = 0
            let phoneComission = 0

            if (this.state.trippleCount > 0 && this.state.trippleCount <= 19){
              trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount
            }else if(this.state.trippleCount >= 20 && this.state.trippleCount <= 99){
              trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

            }else if(this.state.trippleCount >= 100 && this.state.trippleCount <= 299){
              trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

            }else if(this.state.trippleCount >= 300 && this.state.trippleCount <= 999){
              trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

            }else if(this.state.trippleCount >= 1000 && this.state.trippleCount <= 2499){
              trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

            }else if(this.state.trippleCount >= 2500){
              trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount

            }

            if (tvInternetCount > 0 && tvInternetCount <= 19){
              tvInternetComission = this.state.querycomission.dptvintlt20 * tvInternetCount
            }else if(tvInternetCount >= 20 && tvInternetCount <= 99){
              tvInternetComission = this.state.querycomission.dptvintlt100 * tvInternetCount

            }else if(tvInternetCount >= 100 && tvInternetCount <= 299){
              tvInternetComission = this.state.querycomission.dptvintlt300 * tvInternetCount

            }else if(tvInternetCount >= 300 && tvInternetCount <= 999){
              tvInternetComission = this.state.querycomission.dptvintlt1000 * tvInternetCount

            }else if(tvInternetCount >= 1000 && tvInternetCount <= 2499){
              tvInternetComission = this.state.querycomission.dptvintlt2500 * tvInternetCount

            }else if(tvInternetCount >= 2500){
              tvInternetComission = this.state.querycomission.dptvintgt2500 * tvInternetCount
            }

            if (tvPhoneCount > 0 && tvPhoneCount <= 19){
              tvPhoneComission = this.state.querycomission.dptvphonelt20 * tvPhoneCount
            }else if(tvPhoneCount >= 20 && tvPhoneCount <= 99){
              tvPhoneComission = this.state.querycomission.dptvphonelt100 * tvPhoneCount

            }else if(tvPhoneCount >= 100 && tvPhoneCount <= 299){
              tvPhoneComission = this.state.querycomission.dptvphonelt300 * tvPhoneCount

            }else if(tvPhoneCount >= 300 && tvPhoneCount <= 999){
              tvPhoneComission = this.state.querycomission.dptvphonelt1000 * tvPhoneCount

            }else if(tvPhoneCount >= 1000 && tvPhoneCount <= 2499){
              tvPhoneComission = this.state.querycomission.dptvphonelt2500 * tvPhoneCount

            }else if(tvPhoneCount >= 2500){
              tvPhoneComission = this.state.querycomission.dptvphonegt2500 * tvPhoneCount
            }

            if (internetPhoneCount > 0 && internetPhoneCount <= 19){
              internetPhoneComission = this.state.querycomission.dpintphonelt20 * internetPhoneCount
            }else if(internetPhoneCount >= 20 && internetPhoneCount <= 99){
              internetPhoneComission = this.state.querycomission.dpintphonelt100 * internetPhoneCount

            }else if(internetPhoneCount >= 100 && internetPhoneCount <= 299){
              internetPhoneComission = this.state.querycomission.dpintphonelt300 * internetPhoneCount

            }else if(internetPhoneCount >= 300 && internetPhoneCount <= 999){
              internetPhoneComission = this.state.querycomission.dpintphonelt1000 * internetPhoneCount

            }else if(internetPhoneCount >= 1000 && internetPhoneCount <= 2499){
              internetPhoneComission = this.state.querycomission.dpintphonelt2500 * internetPhoneCount

            }else if(internetPhoneCount >= 2500){
              internetPhoneComission = this.state.querycomission.dpintphonegt2500 * internetPhoneCount
            }

            if (tvCount > 0 && tvCount <= 19){
              tvComission = this.state.querycomission.singletvlt20 * tvCount
            }else if(tvCount >= 20 && tvCount <= 99){
              tvComission = this.state.querycomission.singletvlt100 * tvCount

            }else if(tvCount >= 100 && tvCount <= 299){
              tvComission = this.state.querycomission.singletvlt300 * tvCount

            }else if(tvCount >= 300 && tvCount <= 999){
              tvComission = this.state.querycomission.singletvlt1000 * tvCount

            }else if(tvCount >= 1000 && tvCount <= 2499){
              tvComission = this.state.querycomission.singletvlt2500 * tvCount

            }else if(tvCount >= 2500){
              tvComission = this.state.querycomission.singletvgt2500 * tvCount
            }

            if (phoneCount > 0 && phoneCount <= 19){
              phoneComission = this.state.querycomission.singlephonelt20 * phoneCount
            }else if(phoneCount >= 20 && phoneCount <= 99){
              phoneComission = this.state.querycomission.singlephonelt100 * phoneCount

            }else if(phoneCount >= 100 && phoneCount <= 299){
              phoneComission = this.state.querycomission.singlephonelt300 * tvCount

            }else if(phoneCount >= 300 && phoneCount <= 999){
              phoneComission = this.state.querycomission.singlephonelt1000 * phoneCount

            }else if(phoneCount >= 1000 && phoneCount <= 2499){
              phoneComission = this.state.querycomission.singlephonelt2500 * phoneCount

            }else if(phoneCount >= 2500){
              phoneComission = this.state.querycomission.singlephonegt2500 * phoneCount
            }

            if (internetCount > 0 && internetCount <= 19){
              internetComission = this.state.querycomission.singleintlt20 * internetCount
            }else if(internetCount >= 20 && internetCount <= 99){
              internetComission = this.state.querycomission.singleintlt100 * internetCount

            }else if(internetCount >= 100 && internetCount <= 299){
              internetComission = this.state.querycomission.singleintlt300 * tvCount

            }else if(internetCount >= 300 && internetCount <= 999){
              internetComission = this.state.querycomission.singleintlt1000 * internetCount

            }else if(internetCount >= 1000 && internetCount <= 2499){
              internetComission = this.state.querycomission.singleintlt2500 * internetCount

            }else if(internetCount >= 2500){
              internetComission = this.state.querycomission.singleintgt2500 * internetCount
            }

            this.setState({trippeleComission: trippeleComission, 
                        tvInternetComission: tvInternetComission, 
                        tvPhoneComission: tvPhoneComission,
                        internetPhoneComission: internetPhoneComission,
                        tvComission: tvComission,
                        internetComission: internetComission,
                        phoneComission: phoneComission,}, () => {

              console.log("TRIPPLE COMMISIION:---", this.state.trippeleComission, 
                          "\nTV INTERNET COMMISIION:---", this.state.tvInternetComission,
                          "\nTV PHONE COMMISIION:---", this.state.tvPhoneComission,
                          "\nINTERBET PHONE COMMISIION:---", this.state.internetPhoneComission,
                          "\nSingle TV COMMISIION:---", this.state.tvComission,
                          "\nSingle INTERNET COMMISIION:---", this.state.internetComission,
                          "\nSingle PHONE COMMISIION:---", this.state.phoneComission,)
            })
          })
        }),
        (error) => {
          console.log(error)
        }
      }      
      if (this.loggeduser.role == 'user'){
        // console.log("READER FROM USER")
        this.setState({trippleCount: trippleCount, 
                      doubleCount: doubleCount, 
                      singleCount: singleCount,}, () => {
        // this.fetchComissions()

          
          let trippeleComission = 0
          let doubleComission = 0
          let singleComission = 0
          let tvInternetComission = 0
          let tvPhoneComission = 0
          let internetPhoneComission = 0
          let internetComission = 0
          let tvComission = 0
          let phoneComission = 0

          if (this.state.trippleCount > 0 && this.state.trippleCount <= 19){
            trippeleComission = this.state.querycomission.tripplelt20 * this.state.trippleCount
          }else if(this.state.trippleCount >= 20 && this.state.trippleCount <= 99){
            trippeleComission = this.state.querycomission.tripplelt100 * this.state.trippleCount

          }else if(this.state.trippleCount >= 100 && this.state.trippleCount <= 299){
            trippeleComission = this.state.querycomission.tripplelt300 * this.state.trippleCount

          }else if(this.state.trippleCount >= 300 && this.state.trippleCount <= 999){
            trippeleComission = this.state.querycomission.tripplelt1000 * this.state.trippleCount

          }else if(this.state.trippleCount >= 1000 && this.state.trippleCount <= 2499){
            trippeleComission = this.state.querycomission.tripplelt2500 * this.state.trippleCount

          }else if(this.state.trippleCount >= 2500){
            trippeleComission = this.state.querycomission.tripplegt2500 * this.state.trippleCount

          }

          if (tvInternetCount > 0 && tvInternetCount <= 19){
            tvInternetComission = this.state.querycomission.dptvintlt20 * tvInternetCount
          }else if(tvInternetCount >= 20 && tvInternetCount <= 99){
            tvInternetComission = this.state.querycomission.dptvintlt100 * tvInternetCount

          }else if(tvInternetCount >= 100 && tvInternetCount <= 299){
            tvInternetComission = this.state.querycomission.dptvintlt300 * tvInternetCount

          }else if(tvInternetCount >= 300 && tvInternetCount <= 999){
            tvInternetComission = this.state.querycomission.dptvintlt1000 * tvInternetCount

          }else if(tvInternetCount >= 1000 && tvInternetCount <= 2499){
            tvInternetComission = this.state.querycomission.dptvintlt2500 * tvInternetCount

          }else if(tvInternetCount >= 2500){
            tvInternetComission = this.state.querycomission.dptvintgt2500 * tvInternetCount
          }

          if (tvPhoneCount > 0 && tvPhoneCount <= 19){
            tvPhoneComission = this.state.querycomission.dptvphonelt20 * tvPhoneCount
          }else if(tvPhoneCount >= 20 && tvPhoneCount <= 99){
            tvPhoneComission = this.state.querycomission.dptvphonelt100 * tvPhoneCount

          }else if(tvPhoneCount >= 100 && tvPhoneCount <= 299){
            tvPhoneComission = this.state.querycomission.dptvphonelt300 * tvPhoneCount

          }else if(tvPhoneCount >= 300 && tvPhoneCount <= 999){
            tvPhoneComission = this.state.querycomission.dptvphonelt1000 * tvPhoneCount

          }else if(tvPhoneCount >= 1000 && tvPhoneCount <= 2499){
            tvPhoneComission = this.state.querycomission.dptvphonelt2500 * tvPhoneCount

          }else if(tvPhoneCount >= 2500){
            tvPhoneComission = this.state.querycomission.dptvphonegt2500 * tvPhoneCount
          }

          if (internetPhoneCount > 0 && internetPhoneCount <= 19){
            internetPhoneComission = this.state.querycomission.dpintphonelt20 * internetPhoneCount
          }else if(internetPhoneCount >= 20 && internetPhoneCount <= 99){
            internetPhoneComission = this.state.querycomission.dpintphonelt100 * internetPhoneCount

          }else if(internetPhoneCount >= 100 && internetPhoneCount <= 299){
            internetPhoneComission = this.state.querycomission.dpintphonelt300 * internetPhoneCount

          }else if(internetPhoneCount >= 300 && internetPhoneCount <= 999){
            internetPhoneComission = this.state.querycomission.dpintphonelt1000 * internetPhoneCount

          }else if(internetPhoneCount >= 1000 && internetPhoneCount <= 2499){
            internetPhoneComission = this.state.querycomission.dpintphonelt2500 * internetPhoneCount

          }else if(internetPhoneCount >= 2500){
            internetPhoneComission = this.state.querycomission.dpintphonegt2500 * internetPhoneCount
          }

          if (tvCount > 0 && tvCount <= 19){
            tvComission = this.state.querycomission.singletvlt20 * tvCount
          }else if(tvCount >= 20 && tvCount <= 99){
            tvComission = this.state.querycomission.singletvlt100 * tvCount

          }else if(tvCount >= 100 && tvCount <= 299){
            tvComission = this.state.querycomission.singletvlt300 * tvCount

          }else if(tvCount >= 300 && tvCount <= 999){
            tvComission = this.state.querycomission.singletvlt1000 * tvCount

          }else if(tvCount >= 1000 && tvCount <= 2499){
            tvComission = this.state.querycomission.singletvlt2500 * tvCount

          }else if(tvCount >= 2500){
            tvComission = this.state.querycomission.singletvgt2500 * tvCount
          }

          if (phoneCount > 0 && phoneCount <= 19){
            phoneComission = this.state.querycomission.singlephonelt20 * phoneCount
          }else if(phoneCount >= 20 && phoneCount <= 99){
            phoneComission = this.state.querycomission.singlephonelt100 * phoneCount

          }else if(phoneCount >= 100 && phoneCount <= 299){
            phoneComission = this.state.querycomission.singlephonelt300 * tvCount

          }else if(phoneCount >= 300 && phoneCount <= 999){
            phoneComission = this.state.querycomission.singlephonelt1000 * phoneCount

          }else if(phoneCount >= 1000 && phoneCount <= 2499){
            phoneComission = this.state.querycomission.singlephonelt2500 * phoneCount

          }else if(phoneCount >= 2500){
            phoneComission = this.state.querycomission.singlephonegt2500 * phoneCount
          }

          if (internetCount > 0 && internetCount <= 19){
            internetComission = this.state.querycomission.singleintlt20 * internetCount
          }else if(internetCount >= 20 && internetCount <= 99){
            internetComission = this.state.querycomission.singleintlt100 * internetCount

          }else if(internetCount >= 100 && internetCount <= 299){
            internetComission = this.state.querycomission.singleintlt300 * tvCount

          }else if(internetCount >= 300 && internetCount <= 999){
            internetComission = this.state.querycomission.singleintlt1000 * internetCount

          }else if(internetCount >= 1000 && internetCount <= 2499){
            internetComission = this.state.querycomission.singleintlt2500 * internetCount

          }else if(internetCount >= 2500){
            internetComission = this.state.querycomission.singleintgt2500 * internetCount
          }

          this.setState({trippeleComission: trippeleComission, 
                      tvInternetComission: tvInternetComission, 
                      tvPhoneComission: tvPhoneComission,
                      internetPhoneComission: internetPhoneComission,
                      tvComission: tvComission,
                      internetComission: internetComission,
                      phoneComission: phoneComission,}, () => {

            console.log("TRIPPLE COMMISIION:---", this.state.trippeleComission, 
                        "\nTV INTERNET COMMISIION:---", this.state.tvInternetComission,
                        "\nTV PHONE COMMISIION:---", this.state.tvPhoneComission,
                        "\nINTERBET PHONE COMMISIION:---", this.state.internetPhoneComission,
                        "\nSingle TV COMMISIION:---", this.state.tvComission,
                        "\nSingle INTERNET COMMISIION:---", this.state.internetComission,
                        "\nSingle PHONE COMMISIION:---", this.state.phoneComission,)
          })


        })

      }


  }

  componentDidMount(){
    if (this.loggeduser.role === 'admin'){
      
      fetch("http://localhost:8008/api/allorderentry")
      .then(res => res.json())
      .then((result) => {
          let pageSize = Math.ceil(result[0].orders.length/5)
          let pagestoshow = []
          let i = 1
          while(i <= pageSize){
            pagestoshow.push(i)
            i++
          }
          this.setState({
            allorders: result[0].orders,
            pages: pagestoshow,
          }, () => {
            console.log("PAGES TO SHOW:-----", this.state.pages, pagestoshow, pageSize, result[0].orders.length)
            this.countPlays(this.state.allorders)
            fetch("http://localhost:8008/api/allmanagers", {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              }
            })
            .then(res => res.json())
            .then((result)=>{
              this.setState({
                allmanagers: result[0].users,
              }, () => {

                console.log("STATE ALL MANAGER:---", this.state.allmanagers)
                fetch("http://localhost:8008/api/allreps", {
                  method: 'GET',
                  headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                  }
                })
                .then(res => res.json())
                .then((result)=>{
                  this.setState({
                    allreps: result[0].users
                  }, () => {
                    console.log("STATE ALL REPS:---", this.state.allreps)
                  })
                }),
                (error) => {
                  console.log(error)
                }
              })
            }),
            (error) => {
              console.log(error)
            }
          })
        },
        (error) => {
          console.log(error)
        }
      )
    }
    if (this.loggeduser.role === 'user'){
      fetch("http://localhost:8008/api/userorders", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({userid: localStorage.getItem('userId')})
      })
      .then(res => res.json())
      .then((result)=>{
        console.log("ALL USER ORDERS:----", result[0].orders)
        this.setState({allorders: result[0].orders}, () => {
          let type = null
          let user = null
          if (this.loggeduser.type == 'Manager'){
            type = 'manager'
            user = this.loggeduser._id
          }
          if (this.loggeduser.type == 'Representative'){
            type = 'team'
            user = this.loggeduser.manager
          }
          fetch("http://localhost:8008/api/getteamcomission", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({user: user, type: type})
          })
          .then(res => res.json())
          .then((result)=>{
            this.setState({querycomission: result[0].comission[0]}, () => {
              console.log("SINGLE COMISSION:------", this.state.querycomission)
              this.countPlays(this.state.allorders)
            })
          }),
          (error) => {
            console.log(error)
          }
        })
      }),
      (error) => {
        console.log(error)
      }
    }

  }

  fetchComissions(){
    let filterRep = _.filter(this.state.allreps, {"_id":this.state.rep})
    console.log("REP TO FIND:--", filterRep)
    fetch("http://localhost:8008/api/getteamcomission", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({user: filterRep[0].manager, type: 'team'})
    })
    .then(res => res.json())
    .then((result)=>{
      this.setState({querycomission: result[0].comission[0]}, () => {
        console.log("SINGLE COMISSION:------", this.state.querycomission)
      })
    }),
    (error) => {
      console.log(error)
    }
  }

  handleDateChange(event, date){
    this.setState({
      controlledDate: date,
    }, () => {
      let date = (this.state.controlledDate)
      let day = date.getDate()
      let month = date.getMonth()
      let year = date.getFullYear()
      console.log("EVENT FROM DATE CHANGE:---", (this.state.controlledDate), day, month+1, year)
      this.filterOrders(date, day, month, year)
    });
  }

  filterOrders(date, day, month, year){
    console.log("EVENT FROM DATE CHANGE:---", year, month+1, day)
    let filterdate = year+'-0'+(month+1)+'-'+day
    console.log("CONACT DATE FOR FILTER:---", filterdate)
    fetch("http://localhost:8008/api/filterallorders", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({date: date.toString(), day: day, month: month+1, year: year, filterdate: filterdate})
    })
    .then(res => res.json())
    .then((result)=>{
      console.log("ALL FILTERED ORDERS:----", result)
      this.setState({allorders: result[0].orders}, () => {
        this.countPlays(this.state.allorders)
      })
    }),
    (error) => {
      console.log(error)
    }
  }

  clearFilter(){
    this.setState({controlledDate: null}, () => {
      // this.teamOrders()
      fetch("http://localhost:8008/api/allorderentry")
      .then(res => res.json())
      .then(
        (result) => {
            this.setState({
              allorders: result[0].orders
            }, () => {
              console.log("STATE FROM ALL ORDERS:-----", this.state.allorders)
              this.countPlays(this.state.allorders)
            })
        },
        (error) => {
          console.log(error)
        }
      )
    })
  }

  setStatus(status, chargeback){
    if (status === 'cancel' && !chargeback){
      return <Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="CANCELED"
      ><CancelIcon size={22} color="#D50000" /></Tooltip>
    }
    if (status === 'cancel' && chargeback){
      return <div><Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="CANCELED"
      ><CancelIcon size={22} color="#D50000" /></Tooltip>
      <Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="CHARGED BACK"
      ><IoEject size={22} color="#f44336" /></Tooltip>
      </div>
    }

    else if (status === 'installed'){
      return <Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="INSTALLED"
      ><InstallIcon size={22} color="#33691E" /></Tooltip>    
    }
    else{
      return <Tooltip
        enterDelay={300}
        id="tooltip-controlled"
        leaveDelay={300}
        placement="right"
        title="PENDING"
      ><PendingIcon size={22} color="#ffea00" /></Tooltip>
    }
  }

  selectStyle = (n) => {
    if (n.status == 'installed'){
      return {backgroundColor:'green'}
    }
    if (n.status == 'cancel'){
      return {backgroundColor:'red'}
    }
    if (n.status == 'pending'){
      return {backgroundColor:'yellow'}
    }

  }

  render() {
    return (
      <div>
        <Grid container spacing={8}>
          <Grid item xs={6} sm={10}>
            <h4>ALL ORDERS</h4>
          </Grid>
          <Grid item xs={6} sm={2}>
            <IconButton onClick={this.props.history.goBack} 
                        variant="fab" 
                        color="primary" 
                        aria-label="Go Back">
              <ArrowBack />
            </IconButton>
          </Grid>
        </Grid>
        <Divider light style={{marginBottom:15}}/>
        
          <div style={{marginBottom: 10, }}>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Tripple Play Count"
            ><TrippleIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{this.state.trippleCount}</strong>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Est. Comission"
            ><MoneyIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{this.state.trippeleComission}</strong>

          </div>
          <div style={{marginBottom: 10, }}>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Duoble Play Count"
            ><DoubleIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{this.state.doubleCount}</strong>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Est. Comission"
            ><MoneyIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{(this.state.tvInternetComission + this.state.tvPhoneComission + this.state.internetPhoneComission)}</strong>

          </div>
          <div style={{marginBottom: 10, }}>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Single Play Count"
            ><SingleIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10, marginRight: 15,}}>{this.state.singleCount}</strong>
            <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="Est. Comission"
            ><MoneyIcon size={22} color="#303F9F" /></Tooltip>
            :<strong style={{marginLeft: 10,}}>{this.state.tvComission + this.state.phoneComission + this.state.internetComission}</strong>

          </div>        
        
        <Divider light  style={{marginBottom:15, marginTop: 15}} />
        
        

        {this.loggeduser.role === 'admin' ? <div>
        <div><DatePicker hintText="Filter By Date" value={this.state.controlledDate} mode="landscape" onChange={this.handleDateChange}/>
        <Button variant="contained" size="small" color="primary" onClick={this.clearFilter}>
            CLEAR DATE
        </Button></div>
        <SelectField
          floatingLabelText="Filter By Manager"
          value={this.state.manager}
          onChange={this.handleManagerChange}
          style={styles.select}
        >
          <MenuItem value={null} primaryText="" />
            {this.state.allmanagers.map((obj, i) => {
              return (<MenuItem key={obj._id} value={obj._id} primaryText={obj.firstname} />)
          })}
        </SelectField>
        <SelectField
          floatingLabelText="Filter By Rep"
          value={this.state.rep}
          onChange={this.handleRepChange}
          style={styles.select}
        >
          <MenuItem value={null} primaryText="" />
            {this.state.allreps.map((obj, i) => {
              return (<MenuItem key={obj._id} value={obj._id} primaryText={obj.firstname} />)
          })}
        </SelectField>        
        <SelectField
          floatingLabelText="Filter By Status"
          value={this.state.status}
          onChange={this.handleStatusChange}
          style={styles.select}
        >
          <MenuItem value={null} primaryText="" />
          <MenuItem value={'cancel'} primaryText="Cancel" />
          <MenuItem value={'pending'} primaryText="Pending" />
          <MenuItem value={'installed'} primaryText="Installed" />
        </SelectField></div>
         : ""}
        <Table onRowSelection={this.handleRowSelection}
                   selectable={false}
                   multiSelectable={false}
                   deselectOnClickaway={false}>
          <TableHeader enableSelectAll={false}
                       displaySelectAll={false}
                       adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>Date</TableHeaderColumn>
              <TableHeaderColumn>Status</TableHeaderColumn>
              <TableHeaderColumn>CB</TableHeaderColumn>
              <TableHeaderColumn>Rep ID</TableHeaderColumn>
              <TableHeaderColumn>Submit By</TableHeaderColumn>
              <TableHeaderColumn>Rep Name</TableHeaderColumn>
              <TableHeaderColumn>Tripple Play</TableHeaderColumn>
              <TableHeaderColumn>Double Play</TableHeaderColumn>
              <TableHeaderColumn>Single Play</TableHeaderColumn>
              <TableHeaderColumn>Vendor Name</TableHeaderColumn>
              <TableHeaderColumn>Customer Acc.</TableHeaderColumn>
              <TableHeaderColumn>Customer Name</TableHeaderColumn>
              <TableHeaderColumn>Physical Address</TableHeaderColumn>

            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} 
                     showRowHover={true}>
            {this.state.allorders.map((obj, i) => 
            <TableRow key={obj._id}>
              <TableRowColumn><Moment format="YYYY/MM/DD">{obj.created_at}</Moment></TableRowColumn>
              <TableRowColumn>{this.setStatus(obj.status, obj.chargeback)}</TableRowColumn>
              <TableRowColumn>{obj.chargeback ? <Moment diff={obj.installdate} unit="days">{obj.canceldate}</Moment> : ''}</TableRowColumn>
              <TableRowColumn>{obj.userdata[0].username}</TableRowColumn>
              <TableRowColumn>{(obj.repid != obj.submitid) ? obj.submit_by[0].username : ''}</TableRowColumn>
              <TableRowColumn>{obj.repfirstname}</TableRowColumn>
              <TableRowColumn component="th" scope="row">{obj.trippleplay ? <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="TV, HSI, PHONE"
            ><CkeckIcon size="19"/></Tooltip> : ''}</TableRowColumn>
              <TableRowColumn component="th" scope="row">{obj.doubleplay && obj.tv && obj.phone ? <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="TV, PHONE"
            ><CkeckIcon size="19"/></Tooltip> : ''}{obj.doubleplay && obj.tv && obj.internet ? <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="TV, HSI"
            ><CkeckIcon size="19"/></Tooltip> : ''}{obj.doubleplay && obj.phone && obj.internet ? <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="HSI, PHONE"
            ><CkeckIcon size="19"/></Tooltip> : ''}</TableRowColumn>
              <TableRowColumn component="th" scope="row">{obj.singleplay && obj.phone ? <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="PHONE"
            ><CkeckIcon size="19"/></Tooltip> : ''}{obj.singleplay && obj.tv ? <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="TV"
            ><CkeckIcon size="19"/></Tooltip> : ''}{obj.singleplay && obj.internet ? <Tooltip
              enterDelay={300}
              id="tooltip-controlled"
              leaveDelay={300}
              placement="top"
              title="HSI"
            ><CkeckIcon size="19"/></Tooltip> : ''}</TableRowColumn>
              <TableRowColumn>{obj.vendorname}</TableRowColumn>
              <TableRowColumn>{obj.customeracc}</TableRowColumn>
              <TableRowColumn>{obj.customername}</TableRowColumn>
              <TableRowColumn>{obj.address}</TableRowColumn>

            </TableRow> )}
          </TableBody>
        </Table>
        {this.state.pages.map((obj, i)=>{
          <li key={i}>{obj}</li>
        })}
      </div>
    );
  }
}

export default withRouter(Allorders);
