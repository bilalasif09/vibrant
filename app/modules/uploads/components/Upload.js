import React, { Component } from 'react'
import axios from 'axios'

class Upload extends Component {

	constructor (props) {
		super(props)

		this.handleSubmit = this.handleSubmit.bind(this)
	}

	handleSubmit(e) {
		e.preventDefault()

		let formData = new FormData()

		console.log("Files", this.fileInput.files[0])
		let file = this.fileInput.files[0]
		console.log("Files input", file)
		
		formData.append('UploadFile', file, file.name)

		console.log("Form data", formData)

		axios.post("http://localhost:8008/api/attachment", formData)
	    // .then(res => res.json())
	    .then((result)=>{
        console.log("UPLOAD result", result)
	    	console.log("Files result", result.data)
	    }),
	    (error) => {
	      console.log(error)
	    }

	}

	render () {

		return (

			<form onSubmit={this.handleSubmit}>
			  <label>
		          Upload file:
		          <input
		            type="file"
		            ref={input => {
		              this.fileInput = input;
		            }}
		          />
		        </label>
			  <button type="submit"> Submit </button>
			</form>

		)

	}

}

export default Upload