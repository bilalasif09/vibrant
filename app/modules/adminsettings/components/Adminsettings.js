import React, { Component, PropTypes } from 'react';
import {withRouter} from 'react-router-dom'
import {AppTemplate} from '../../../commons/ui/components/AppTemplate';
import Link from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import Toggle from 'material-ui/Toggle';
import Divider from 'material-ui/Divider';
import moment from 'moment';

class adminsettings extends Component {
    

  constructor(props) {
    super(props);
    this.state = {
      tripplelt20: '',
      tripplelt100: '',
      tripplelt300: '',
      tripplelt1000: '',
      tripplelt2500: '',
      tripplegt2500: '',
      dptvphonelt20: '',
      dptvphonelt100: '',
      dptvphonelt300: '',
      dptvphonelt1000: '',
      dptvphonelt2500: '',
      dptvphonegt2500: '',
      dpintphonelt20: '',
      dpintphonelt100: '',
      dpintphonelt300: '',
      dpintphonelt1000: '',
      dpintphonelt2500: '',
      dpintphonegt2500: '',
      dptvintlt20: '',
      dptvintlt100: '',
      dptvintlt300: '',
      dptvintlt1000: '',
      dptvintlt2500: '',
      dptvintgt2500: '',
      singletvlt20: '',
      singletvlt100: '',
      singletvlt300: '',
      singletvlt1000: '',
      singletvlt2500: '',
      singletvgt2500: '',
      singlephonelt20: '',
      singlephonelt100: '',
      singlephonelt300: '',
      singlephonelt1000: '',
      singlephonelt2500: '',
      singlephonegt2500: '',
      singleintlt20: '',
      singleintlt100: '',
      singleintlt300: '',
      singleintlt1000: '',
      singleintlt2500: '',
      singleintgt2500: '',
      allmanagers: [],
      manager: null,
      type: null,
      comissionid: null,
      managerplaceholder: 'SELECT MANAGER',
      updateflag: false,
    }
    this.handleComissionchange = this.handleComissionchange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.clearForm = this.clearForm.bind(this)
    this.handleTypeChange = this.handleTypeChange.bind(this)
    this.handleManagerChange = this.handleManagerChange.bind(this)
    this.updateComission = this.updateComission.bind(this)
    this.loggeduser = JSON.parse(localStorage.getItem('loggeduser'))
  }



  componentDidMount(){
    this.allManagers()
  }

  allManagers(){
    fetch("http://localhost:8008/api/allmanagers", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      }
    })
    .then(res => res.json())
    .then((result)=>{
      this.setState({
        allmanagers: result[0].users
      }, () => {
        console.log("STATE ALL MANAGER:---", this.state.allmanagers)
      })
    }),
    (error) => {
      console.log(error)
    }
  }

    

  handleComissionchange(event){
    let name = event.target.name
    this.setState({[name]: event.target.value})
  }

  handleTypeChange(event, index, value){
    this.setState({type: value}, () => {
      if (this.state.type == 'team'){
        console.log("TYPE CHANGE TEAM:---", this.state.type)
        this.setState({managerplaceholder: "SELECT TEAM", manager: null,}, () => {
          this.clearForm()
        })
      }else{
        console.log("TYPE CHANGE MANAGER:---", this.state.type)
        this.setState({managerplaceholder: "SELECT MANAGER", manager: null,}, () => {
          this.clearForm()
        })
      }
    })
  }

  handleManagerChange(event, index, value){
    this.setState({manager: value}, () => {
      console.log("MANAGER STATE VALUE:---", this.state.manager)
      if (this.state.manager){
        if (this.state.type == 'team'){
          fetch("http://localhost:8008/api/getteamcomission", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({user: this.state.manager, type: 'team'})
          })
          .then(res => res.json())
          .then((result)=>{
            console.log("SINGLE COMISSION:------", result)
            if (result[0].comission.length){
              this.setState({updateflag: true}, () => {
                this.setFields(result[0].comission[0])
              })
            }else{
              this.clearForm()
            }
          }),
          (error) => {
            console.log(error)
          }
        }else if (this.state.type == 'manager'){
          fetch("http://localhost:8008/api/getteamcomission", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({user: this.state.manager, type: 'manager'})
          })
          .then(res => res.json())
          .then((result)=>{
            console.log("SINGLE COMISSION:------", result)
            if (result[0].comission.length){
              this.setState({updateflag: true}, () => {
                this.setFields(result[0].comission[0])
              })
            }else{
              this.clearForm()
            }
          }),
          (error) => {
            console.log(error)
          }
        }
      }else{
        this.clearForm()
      }
    })
  }

  setFields(obj){
    console.log("OBJECT TO SET INPUT STATE:---", obj)
    this.setState({comissionid: obj._id,
                  tripplelt20: obj.tripplelt20,
                  tripplelt100: obj.tripplelt100,
                  tripplelt300: obj.tripplelt300,
                  tripplelt1000: obj.tripplelt1000,
                  tripplelt2500: obj.tripplelt2500,
                  tripplegt2500: obj.tripplegt2500,
                  
                  dptvphonelt20: obj.dptvintlt20,
                  dptvphonelt100: obj.dptvphonelt100,
                  dptvphonelt300: obj.dptvphonelt300,
                  dptvphonelt1000: obj.dptvphonelt1000,
                  dptvphonelt2500: obj.dptvphonelt2500,
                  dptvphonegt2500: obj.dptvphonegt2500,
                  
                  dpintphonelt20: obj.dpintphonelt20,
                  dpintphonelt100: obj.dpintphonelt100,
                  dpintphonelt300: obj.dpintphonelt300,
                  dpintphonelt1000: obj.dpintphonelt1000,
                  dpintphonelt2500: obj.dpintphonelt2500,
                  dpintphonegt2500: obj.dpintphonegt2500,
                  
                  dptvintlt20: obj.dptvintlt20,
                  dptvintlt100: obj.dptvintlt100,
                  dptvintlt300: obj.dptvintlt300,
                  dptvintlt1000: obj.dptvintlt1000,
                  dptvintlt2500: obj.dptvintlt2500,
                  dptvintgt2500: obj.dptvintgt2500,

                  singletvlt20: obj.singletvlt20,
                  singletvlt100: obj.singletvlt100,
                  singletvlt300: obj.singletvlt300,
                  singletvlt1000: obj.singletvlt1000,
                  singletvlt2500: obj.singletvlt2500,
                  singletvgt2500: obj.singletvgt2500,
                  
                  singlephonelt20: obj.singlephonelt20,
                  singlephonelt100: obj.singlephonelt100,
                  singlephonelt300: obj.singlephonelt300,
                  singlephonelt1000: obj.singlephonelt1000,
                  singlephonelt2500: obj.singlephonelt2500,
                  singlephonegt2500: obj.singlephonegt2500,
                  
                  singleintlt20: obj.singleintlt20,
                  singleintlt100: obj.singleintlt100,
                  singleintlt300: obj.singleintlt300,
                  singleintlt1000: obj.singleintlt1000,
                  singleintlt2500: obj.singleintlt2500,
                  singleintgt2500: obj.singleintgt2500,

                  userref: obj.manager,
                  type: obj.type,
    })
  }


  updateComission(){
    console.log("LOGGED IN USER:---", this.loggeduser,

                "\nMANAGER VALUE:---", this.state.manager, "\n",
                "TYPE FOR COMISSION:---", this.state.type, "\n",
                "TRIPPLE PLAY < 19:---", this.state.tripplelt20, "\n",
                "TRIPPLE PLAY < 100:---", this.state.tripplelt100, "\n",
                "TRIPPLE PLAY < 300:---", this.state.tripplelt300, "\n",
                "TRIPPLE PLAY < 1000:---", this.state.tripplelt1000, "\n",
                "TRIPPLE PLAY < 2500:---", this.state.tripplelt2500, "\n",
                "TRIPPLE PLAY > 2500:---", this.state.tripplegt2500, "\n",
                
                "DOUBLE PLAY TV PHONE < 19:---", this.state.dptvphonelt20, "\n",
                "DOUBLE PLAY TV PHONE < 100:---", this.state.dptvphonelt100, "\n",
                "DOUBLE PLAY TV PHONE < 300:---", this.state.dptvphonelt300, "\n",
                "DOUBLE PLAY TV PHONE < 1000:---", this.state.dptvphonelt1000, "\n",
                "DOUBLE PLAY TV PHONE < 2500:---", this.state.dptvphonelt2500, "\n",
                "DOUBLE PLAY TV PHONE > 2500:---", this.state.dptvphonegt2500, "\n",    

                "DOUBLE PLAY TV INTERNET < 19:---", this.state.dptvintlt20, "\n",
                "DOUBLE PLAY TV INTERNET < 100:---", this.state.dptvintlt100, "\n",
                "DOUBLE PLAY TV INTERNET < 300:---", this.state.dptvintlt300, "\n",
                "DOUBLE PLAY TV INTERNET < 1000:---", this.state.dptvintlt1000, "\n",
                "DOUBLE PLAY TV INTERNET < 2500:---", this.state.dptvintlt2500, "\n",
                "DOUBLE PLAY TV INTERNET > 2500:---", this.state.dptvintgt2500, "\n",

                "DOUBLE PLAY PHONE INTERNET < 19:---", this.state.dpintphonelt20, "\n",
                "DOUBLE PLAY PHONE INTERNET < 100:---", this.state.dpintphonelt100, "\n",
                "DOUBLE PLAY PHONE INTERNET < 300:---", this.state.dpintphonelt300, "\n",
                "DOUBLE PLAY PHONE INTERNET < 1000:---", this.state.dpintphonelt1000, "\n",
                "DOUBLE PLAY PHONE INTERNET < 2500:---", this.state.dpintphonelt2500, "\n",
                "DOUBLE PLAY PHONE INTERNET > 2500:---", this.state.dpintphonegt2500, "\n",
                
                "SINLE PLAY PHONE < 19:---", this.state.singlephonelt20, "\n",
                "SINLE PLAY PHONE < 100:---", this.state.singlephonelt100, "\n",
                "SINLE PLAY PHONE < 300:---", this.state.singlephonelt300, "\n",
                "SINLE PLAY PHONE < 1000:---", this.state.singlephonelt1000, "\n",
                "SINLE PLAY PHONE < 2500:---", this.state.singlephonelt2500, "\n",
                "SINLE PLAY PHONE > 2500:---", this.state.singlephonegt2500, "\n",

                "SINLE PLAY INTERNET < 19:---", this.state.singleintlt20, "\n",
                "SINLE PLAY INTERNET < 100:---", this.state.singleintlt100, "\n",
                "SINLE PLAY INTERNET < 300:---", this.state.singleintlt300, "\n",
                "SINLE PLAY INTERNET < 1000:---", this.state.singleintlt1000, "\n",
                "SINLE PLAY INTERNET < 2500:---", this.state.singleintlt2500, "\n",
                "SINLE PLAY INTERNET > 2500:---", this.state.singleintgt2500, "\n",

                "SINLE PLAY TV < 19:---", this.state.singletvlt20, "\n",
                "SINLE PLAY TV < 100:---", this.state.singletvlt100, "\n",
                "SINLE PLAY TV < 300:---", this.state.singletvlt300, "\n",
                "SINLE PLAY TV < 1000:---", this.state.singletvlt1000, "\n",
                "SINLE PLAY TV < 2500:---", this.state.singletvlt2500, "\n",
                "SINLE PLAY TV > 2500:---", this.state.singletvgt2500, "\n",
    )
      fetch("http://localhost:8008/api/updatecomission", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({comissionid: this.state.comissionid,
                            tripplelt20: this.state.tripplelt20,
                            tripplelt100: this.state.tripplelt100,
                            tripplelt300: this.state.tripplelt300,
                            tripplelt1000: this.state.tripplelt1000,
                            tripplelt2500: this.state.tripplelt2500,
                            tripplegt2500: this.state.tripplegt2500,
                            
                            dptvphonelt20: this.state.dptvintlt20,
                            dptvphonelt100: this.state.dptvphonelt100,
                            dptvphonelt300: this.state.dptvphonelt300,
                            dptvphonelt1000: this.state.dptvphonelt1000,
                            dptvphonelt2500: this.state.dptvphonelt2500,
                            dptvphonegt2500: this.state.dptvphonegt2500,
                            
                            dpintphonelt20: this.state.dpintphonelt20,
                            dpintphonelt100: this.state.dpintphonelt100,
                            dpintphonelt300: this.state.dpintphonelt300,
                            dpintphonelt1000: this.state.dpintphonelt1000,
                            dpintphonelt2500: this.state.dpintphonelt2500,
                            dpintphonegt2500: this.state.dpintphonegt2500,
                            
                            dptvintlt20: this.state.dptvintlt20,
                            dptvintlt100: this.state.dptvintlt100,
                            dptvintlt300: this.state.dptvintlt300,
                            dptvintlt1000: this.state.dptvintlt1000,
                            dptvintlt2500: this.state.dptvintlt2500,
                            dptvintgt2500: this.state.dptvintgt2500,

                            singletvlt20: this.state.singletvlt20,
                            singletvlt100: this.state.singletvlt100,
                            singletvlt300: this.state.singletvlt300,
                            singletvlt1000: this.state.singletvlt1000,
                            singletvlt2500: this.state.singletvlt2500,
                            singletvgt2500: this.state.singletvgt2500,
                            
                            singlephonelt20: this.state.singlephonelt20,
                            singlephonelt100: this.state.singlephonelt100,
                            singlephonelt300: this.state.singlephonelt300,
                            singlephonelt1000: this.state.singlephonelt1000,
                            singlephonelt2500: this.state.singlephonelt2500,
                            singlephonegt2500: this.state.singlephonegt2500,
                            
                            singleintlt20: this.state.singleintlt20,
                            singleintlt100: this.state.singleintlt100,
                            singleintlt300: this.state.singleintlt300,
                            singleintlt1000: this.state.singleintlt1000,
                            singleintlt2500: this.state.singleintlt2500,
                            singleintgt2500: this.state.singleintgt2500,

                            userref: this.state.manager,
                            type: this.state.type,
                          })
    })
    .then(res => res.json())
    .then((result)=>{
      console.log("RESULT FROM UPDTE COMISSION:------", result)
      this.clearForm()
    }),
    (error) => {
      console.log(error)
    }
  }

  handleSubmit(event){
    event.preventDefault()
    let type = null
    console.log("LOGGED IN USER:---", this.loggeduser,

                "\nMANAGER VALUE:---", this.state.manager, "\n",
                "TYPE FOR COMISSION:---", this.state.type, "\n",
                "TRIPPLE PLAY < 19:---", this.state.tripplelt20, "\n",
                "TRIPPLE PLAY < 100:---", this.state.tripplelt100, "\n",
                "TRIPPLE PLAY < 300:---", this.state.tripplelt300, "\n",
                "TRIPPLE PLAY < 1000:---", this.state.tripplelt1000, "\n",
                "TRIPPLE PLAY < 2500:---", this.state.tripplelt2500, "\n",
                "TRIPPLE PLAY > 2500:---", this.state.tripplegt2500, "\n",
                
                "DOUBLE PLAY TV PHONE < 19:---", this.state.dptvphonelt20, "\n",
                "DOUBLE PLAY TV PHONE < 100:---", this.state.dptvphonelt100, "\n",
                "DOUBLE PLAY TV PHONE < 300:---", this.state.dptvphonelt300, "\n",
                "DOUBLE PLAY TV PHONE < 1000:---", this.state.dptvphonelt1000, "\n",
                "DOUBLE PLAY TV PHONE < 2500:---", this.state.dptvphonelt2500, "\n",
                "DOUBLE PLAY TV PHONE > 2500:---", this.state.dptvphonegt2500, "\n",    

                "DOUBLE PLAY TV INTERNET < 19:---", this.state.dptvintlt20, "\n",
                "DOUBLE PLAY TV INTERNET < 100:---", this.state.dptvintlt100, "\n",
                "DOUBLE PLAY TV INTERNET < 300:---", this.state.dptvintlt300, "\n",
                "DOUBLE PLAY TV INTERNET < 1000:---", this.state.dptvintlt1000, "\n",
                "DOUBLE PLAY TV INTERNET < 2500:---", this.state.dptvintlt2500, "\n",
                "DOUBLE PLAY TV INTERNET > 2500:---", this.state.dptvintgt2500, "\n",

                "DOUBLE PLAY PHONE INTERNET < 19:---", this.state.dpintphonelt20, "\n",
                "DOUBLE PLAY PHONE INTERNET < 100:---", this.state.dpintphonelt100, "\n",
                "DOUBLE PLAY PHONE INTERNET < 300:---", this.state.dpintphonelt300, "\n",
                "DOUBLE PLAY PHONE INTERNET < 1000:---", this.state.dpintphonelt1000, "\n",
                "DOUBLE PLAY PHONE INTERNET < 2500:---", this.state.dpintphonelt2500, "\n",
                "DOUBLE PLAY PHONE INTERNET > 2500:---", this.state.dpintphonegt2500, "\n",
                
                "SINLE PLAY PHONE < 19:---", this.state.singlephonelt20, "\n",
                "SINLE PLAY PHONE < 100:---", this.state.singlephonelt100, "\n",
                "SINLE PLAY PHONE < 300:---", this.state.singlephonelt300, "\n",
                "SINLE PLAY PHONE < 1000:---", this.state.singlephonelt1000, "\n",
                "SINLE PLAY PHONE < 2500:---", this.state.singlephonelt2500, "\n",
                "SINLE PLAY PHONE > 2500:---", this.state.singlephonegt2500, "\n",

                "SINLE PLAY INTERNET < 19:---", this.state.singleintlt20, "\n",
                "SINLE PLAY INTERNET < 100:---", this.state.singleintlt100, "\n",
                "SINLE PLAY INTERNET < 300:---", this.state.singleintlt300, "\n",
                "SINLE PLAY INTERNET < 1000:---", this.state.singleintlt1000, "\n",
                "SINLE PLAY INTERNET < 2500:---", this.state.singleintlt2500, "\n",
                "SINLE PLAY INTERNET > 2500:---", this.state.singleintgt2500, "\n",

                "SINLE PLAY TV < 19:---", this.state.singletvlt20, "\n",
                "SINLE PLAY TV < 100:---", this.state.singletvlt100, "\n",
                "SINLE PLAY TV < 300:---", this.state.singletvlt300, "\n",
                "SINLE PLAY TV < 1000:---", this.state.singletvlt1000, "\n",
                "SINLE PLAY TV < 2500:---", this.state.singletvlt2500, "\n",
                "SINLE PLAY TV > 2500:---", this.state.singletvgt2500, "\n",
    )

    fetch("http://localhost:8008/api/addcomission", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({tripplelt20: this.state.tripplelt20,
                            tripplelt100: this.state.tripplelt100,
                            tripplelt300: this.state.tripplelt300,
                            tripplelt1000: this.state.tripplelt1000,
                            tripplelt2500: this.state.tripplelt2500,
                            tripplegt2500: this.state.tripplegt2500,
                            
                            dptvphonelt20: this.state.dptvintlt20,
                            dptvphonelt100: this.state.dptvphonelt100,
                            dptvphonelt300: this.state.dptvphonelt300,
                            dptvphonelt1000: this.state.dptvphonelt1000,
                            dptvphonelt2500: this.state.dptvphonelt2500,
                            dptvphonegt2500: this.state.dptvphonegt2500,
                            
                            dpintphonelt20: this.state.dpintphonelt20,
                            dpintphonelt100: this.state.dpintphonelt100,
                            dpintphonelt300: this.state.dpintphonelt300,
                            dpintphonelt1000: this.state.dpintphonelt1000,
                            dpintphonelt2500: this.state.dpintphonelt2500,
                            dpintphonegt2500: this.state.dpintphonegt2500,
                            
                            dptvintlt20: this.state.dptvintlt20,
                            dptvintlt100: this.state.dptvintlt100,
                            dptvintlt300: this.state.dptvintlt300,
                            dptvintlt1000: this.state.dptvintlt1000,
                            dptvintlt2500: this.state.dptvintlt2500,
                            dptvintgt2500: this.state.dptvintgt2500,

                            singletvlt20: this.state.singletvlt20,
                            singletvlt100: this.state.singletvlt100,
                            singletvlt300: this.state.singletvlt300,
                            singletvlt1000: this.state.singletvlt1000,
                            singletvlt2500: this.state.singletvlt2500,
                            singletvgt2500: this.state.singletvgt2500,
                            
                            singlephonelt20: this.state.singlephonelt20,
                            singlephonelt100: this.state.singlephonelt100,
                            singlephonelt300: this.state.singlephonelt300,
                            singlephonelt1000: this.state.singlephonelt1000,
                            singlephonelt2500: this.state.singlephonelt2500,
                            singlephonegt2500: this.state.singlephonegt2500,
                            
                            singleintlt20: this.state.singleintlt20,
                            singleintlt100: this.state.singleintlt100,
                            singleintlt300: this.state.singleintlt300,
                            singleintlt1000: this.state.singleintlt1000,
                            singleintlt2500: this.state.singleintlt2500,
                            singleintgt2500: this.state.singleintgt2500,

                            userref: this.state.manager,
                            type: this.state.type,
                          })
    })
    .then(res => res.json())
    .then((result)=>{
      console.log("RESULT FROM ADD COMISSION:------", result)
      this.clearForm()
    }),
    (error) => {
      console.log(error)
    }


  }

  clearForm(){
    this.setState({
      tripplelt20: '',
      // manager: null,
      tripplelt100: '',
      tripplelt300: '',
      tripplelt1000: '',
      tripplelt2500: '',
      tripplegt2500: '',
      dptvphonelt20: '',
      dptvphonelt100: '',
      dptvphonelt300: '',
      dptvphonelt1000: '',
      dptvphonelt2500: '',
      dptvphonegt2500: '',
      dpintphonelt20: '',
      dpintphonelt100: '',
      dpintphonelt300: '',
      dpintphonelt1000: '',
      dpintphonelt2500: '',
      dpintphonegt2500: '',
      dptvintlt20: '',
      dptvintlt100: '',
      dptvintlt300: '',
      dptvintlt1000: '',
      dptvintlt2500: '',
      dptvintgt2500: '',
      singletvlt20: '',
      singletvlt100: '',
      singletvlt300: '',
      singletvlt1000: '',
      singletvlt2500: '',
      singletvgt2500: '',
      singlephonelt20: '',
      singlephonelt100: '',
      singlephonelt300: '',
      singlephonelt1000: '',
      singlephonelt2500: '',
      singlephonegt2500: '',
      singleintlt20: '',
      singleintlt100: '',
      singleintlt300: '',
      singleintlt1000: '',
      singleintlt2500: '',
      singleintgt2500: '',
      updateflag: false,
    })
  }

  render() {

    const {tripplelt20,
          tripplelt100,
          tripplelt300,
          tripplelt1000,
          tripplelt2500,
          tripplegt2500,
          dptvphonelt20,
          dptvphonelt100,
          dptvphonelt300,
          dptvphonelt1000,
          dptvphonelt2500,
          dptvphonegt2500,
          dpintphonelt20,
          dpintphonelt100,
          dpintphonelt300,
          dpintphonelt1000,
          dpintphonelt2500,
          dpintphonegt2500,
          dptvintlt20,
          dptvintlt100,
          dptvintlt300,
          dptvintlt1000,
          dptvintlt2500,
          dptvintgt2500,
          singletvlt20,
          singletvlt100,
          singletvlt300,
          singletvlt1000,
          singletvlt2500,
          singletvgt2500,
          singlephonelt20,
          singlephonelt100,
          singlephonelt300,
          singlephonelt1000,
          singlephonelt2500,
          singlephonegt2500,
          singleintlt20,
          singleintlt100,
          singleintlt300,
          singleintlt1000,
          singleintlt2500,
          singleintgt2500,} = this.state

    return (
      <div> 
        <h3>COMISSION SETTINGS</h3>
        <Divider />
        <form onSubmit={this.handleSubmit}>
          <SelectField
            floatingLabelText="COMISSION FOR"
            value={this.state.type}
            onChange={this.handleTypeChange}
            required
          >
            <MenuItem value={null} primaryText="" />
            <MenuItem value={'team'} primaryText="Team" />
            <MenuItem value={'manager'} primaryText="Manager" />
          </SelectField>        
          <SelectField
            floatingLabelText={this.state.managerplaceholder}
            value={this.state.manager}
            onChange={this.handleManagerChange}
            required
          >
            <MenuItem value={null} primaryText="" />
              {this.state.allmanagers.map((obj, i) => {
                return (<MenuItem key={obj._id} value={obj._id} primaryText={this.state.type == 'team' ? obj.firstname+" Team" : obj.firstname} />)
            })}
          </SelectField>

          <h4>Tripple Play Comissions</h4>
          <TextField
          id="tripplelt20"
          name="tripplelt20"
          floatingLabelText="Volume>0, Volume<=19"
          onChange={this.handleComissionchange}
          value={tripplelt20}
          type='number'
          required/>
          <TextField
          id="tripplelt100"
          name="tripplelt100"
          floatingLabelText="Volume>=20, Volume<=99"
          onChange={this.handleComissionchange}
          value={tripplelt100}
          type='number'
          required/>
          <TextField
          id="tripplelt300"
          name="tripplelt300"
          floatingLabelText="Volume>=100, Volume<=299"
          onChange={this.handleComissionchange}
          value={tripplelt300}
          type='number'
          required/>
          <TextField
          id="tripplelt1000"
          name="tripplelt1000"
          floatingLabelText="Volume>=300, Volume<=999"
          onChange={this.handleComissionchange}
          value={tripplelt1000}
          type='number'
          required/>
          <TextField
          id="tripplelt2500"
          name="tripplelt2500"
          floatingLabelText="Volume>=1000, Volume<=2499"
          onChange={this.handleComissionchange}
          value={tripplelt2500}
          type='number'
          required/>
          <TextField
          id="tripplegt2500"
          name="tripplegt2500"
          floatingLabelText="Volume>=2500"
          onChange={this.handleComissionchange}
          value={tripplegt2500}
          type='number'
          required/>

          <h4>Double Play (TV, PHONE) Comissions</h4>
          <TextField
          id="dptvphonelt20"
          name="dptvphonelt20"
          floatingLabelText="Volume>0, Volume<=19"
          onChange={this.handleComissionchange}
          value={dptvphonelt20}
          type='number'
          required/>
          <TextField
          id="dptvphonelt100"
          name="dptvphonelt100"
          floatingLabelText="Volume>=20, Volume<=99"
          onChange={this.handleComissionchange}
          value={dptvphonelt100}
          type='number'
          required/>
          <TextField
          id="dptvphonelt300"
          name="dptvphonelt300"
          floatingLabelText="Volume>=100, Volume<=299"
          onChange={this.handleComissionchange}
          value={dptvphonelt300}
          type='number'
          required/>
          <TextField
          id="dptvphonelt1000"
          name="dptvphonelt1000"
          floatingLabelText="Volume>=300, Volume<=999"
          onChange={this.handleComissionchange}
          value={dptvphonelt1000}
          type='number'
          required/>
          <TextField
          id="dptvphonelt2500"
          name="dptvphonelt2500"
          floatingLabelText="Volume>=1000, Volume<=2499"
          onChange={this.handleComissionchange}
          value={dptvphonelt2500}
          type='number'
          required/>
          <TextField
          id="dptvphonegt2500"
          name="dptvphonegt2500"
          floatingLabelText="Volume>=2500"
          onChange={this.handleComissionchange}
          value={dptvphonegt2500}
          type='number'
          required/>
        
          <h4>Double Play (TV, INTERNET) Comissions</h4>
          <TextField
          id="dptvintlt20"
          name="dptvintlt20"
          floatingLabelText="Volume>0, Volume<=19"
          onChange={this.handleComissionchange}
          value={dptvintlt20}
          type='number'
          required/>
          <TextField
          id="dptvintlt100"
          name="dptvintlt100"
          floatingLabelText="Volume>=20, Volume<=99"
          onChange={this.handleComissionchange}
          value={dptvintlt100}
          type='number'
          required/>
          <TextField
          id="dptvintlt300"
          name="dptvintlt300"
          floatingLabelText="Volume>=100, Volume<=299"
          onChange={this.handleComissionchange}
          value={dptvintlt300}
          type='number'
          required/>
          <TextField
          id="dptvintlt1000"
          name="dptvintlt1000"
          floatingLabelText="Volume>=300, Volume<=999"
          onChange={this.handleComissionchange}
          value={dptvintlt1000}
          type='number'
          required/>
          <TextField
          id="dptvintlt2500"
          name="dptvintlt2500"
          floatingLabelText="Volume>=1000, Volume<=2499"
          onChange={this.handleComissionchange}
          value={dptvintlt2500}
          type='number'
          required/>
          <TextField
          id="dptvintgt2500"
          name="dptvintgt2500"
          floatingLabelText="Volume>=2500"
          onChange={this.handleComissionchange}
          value={dptvintgt2500}
          type='number'
          required/>

          <h4>Double Play (PHONE, INTERNET) Comissions</h4>
          <TextField
          id="dpintphonelt20"
          name="dpintphonelt20"
          floatingLabelText="Volume>0, Volume<=19"
          onChange={this.handleComissionchange}
          value={dpintphonelt20}
          type='number'
          required/>
          <TextField
          id="dpintphonelt100"
          name="dpintphonelt100"
          floatingLabelText="Volume>=20, Volume<=99"
          onChange={this.handleComissionchange}
          value={dpintphonelt100}
          type='number'
          required/>
          <TextField
          id="dpintphonelt300"
          name="dpintphonelt300"
          floatingLabelText="Volume>=100, Volume<=299"
          onChange={this.handleComissionchange}
          value={dpintphonelt300}
          type='number'
          required/>
          <TextField
          id="dpintphonelt1000"
          name="dpintphonelt1000"
          floatingLabelText="Volume>=300, Volume<=999"
          onChange={this.handleComissionchange}
          value={dpintphonelt1000}
          type='number'
          required/>
          <TextField
          id="dpintphonelt2500"
          name="dpintphonelt2500"
          floatingLabelText="Volume>=1000, Volume<=2499"
          onChange={this.handleComissionchange}
          value={dpintphonelt2500}
          type='number'
          required/>
          <TextField
          id="dpintphonegt2500"
          name="dpintphonegt2500"
          floatingLabelText="Volume>=2500"
          onChange={this.handleComissionchange}
          value={dpintphonegt2500}
          type='number'
          required/>
          <h4>Single Play (TV) Comissions</h4>
          <TextField
          id="singletvlt20"
          name="singletvlt20"
          floatingLabelText="Volume>0, Volume<=19"
          onChange={this.handleComissionchange}
          value={singletvlt20}
          type='number'
          required/>
          <TextField
          id="singletvlt100"
          name="singletvlt100"
          floatingLabelText="Volume>=20, Volume<=99"
          onChange={this.handleComissionchange}
          value={singletvlt100}
          type='number'
          required/>
          <TextField
          id="singletvlt300"
          name="singletvlt300"
          floatingLabelText="Volume>=100, Volume<=299"
          onChange={this.handleComissionchange}
          value={singletvlt300}
          type='number'
          required/>
          <TextField
          id="singletvlt1000"
          name="singletvlt1000"
          floatingLabelText="Volume>=300, Volume<=999"
          onChange={this.handleComissionchange}
          value={singletvlt1000}
          type='number'
          required/>
          <TextField
          id="singletvlt2500"
          name="singletvlt2500"
          floatingLabelText="Volume>=1000, Volume<=2499"
          onChange={this.handleComissionchange}
          value={singletvlt2500}
          type='number'
          required/>
          <TextField
          id="singletvgt2500"
          name="singletvgt2500"
          floatingLabelText="Volume>=2500"
          onChange={this.handleComissionchange}
          value={singletvgt2500}
          type='number'
          required/>
          <h4>Single Play (PHONE) Comissions</h4>
          <TextField
          id="singlephonelt20"
          name="singlephonelt20"
          floatingLabelText="Volume>0, Volume<=19"
          onChange={this.handleComissionchange}
          value={singlephonelt20}
          type='number'
          required/>
          <TextField
          id="singlephonelt100"
          name="singlephonelt100"
          floatingLabelText="Volume>=20, Volume<=99"
          onChange={this.handleComissionchange}
          value={singlephonelt100}
          type='number'
          required/>
          <TextField
          id="singlephonelt300"
          name="singlephonelt300"
          floatingLabelText="Volume>=100, Volume<=299"
          onChange={this.handleComissionchange}
          value={singlephonelt300}
          type='number'
          required/>
          <TextField
          id="singlephonelt1000"
          name="singlephonelt1000"
          floatingLabelText="Volume>=300, Volume<=999"
          onChange={this.handleComissionchange}
          value={singlephonelt1000}
          type='number'
          required/>
          <TextField
          id="singlephonelt2500"
          name="singlephonelt2500"
          floatingLabelText="Volume>=1000, Volume<=2499"
          onChange={this.handleComissionchange}
          value={singlephonelt2500}
          type='number'
          required/>
          <TextField
          id="singlephonegt2500"
          name="singlephonegt2500"
          floatingLabelText="Volume>=2500"
          onChange={this.handleComissionchange}
          value={singlephonegt2500}
          type='number'
          required/>
          <h4>Single Play (INTERNET) Comissions</h4>
          <TextField
          id="singleintlt20"
          name="singleintlt20"
          floatingLabelText="Volume>0, Volume<=19"
          onChange={this.handleComissionchange}
          value={singleintlt20}
          type='number'
          required/>
          <TextField
          id="singleintlt100"
          name="singleintlt100"
          floatingLabelText="Volume>=20, Volume<=99"
          onChange={this.handleComissionchange}
          value={singleintlt100}
          type='number'
          required/>
          <TextField
          id="singleintlt300"
          name="singleintlt300"
          floatingLabelText="Volume>=100, Volume<=299"
          onChange={this.handleComissionchange}
          value={singleintlt300}
          type='number'
          required/>
          <TextField
          id="singleintlt1000"
          name="singleintlt1000"
          floatingLabelText="Volume>=300, Volume<=999"
          onChange={this.handleComissionchange}
          value={singleintlt1000}
          type='number'
          required/>
          <TextField
          id="singleintlt2500"
          name="singleintlt2500"
          floatingLabelText="Volume>=1000, Volume<=2499"
          onChange={this.handleComissionchange}
          value={singleintlt2500}
          type='number'
          required/>
          <TextField
          id="singleintgt2500"
          name="singleintgt2500"
          floatingLabelText="Volume>=2500"
          onChange={this.handleComissionchange}
          value={singleintgt2500}
          type='number'
          required/>
          <Divider style={{marginTop:20,}}/>
          
          {this.state.updateflag ? <RaisedButton 
                      label="UPDATE"
                      onClick={this.updateComission}
                      primary={true}
                      style={{marginTop:16}}
                    /> : <RaisedButton 
                      label="SUBMIT"
                      type="submit"
                      primary={true}
                      style={{marginTop:16}}
                      disabled={this.state.manager == null && this.state.team == null}
                    />}
          <RaisedButton 
            label="CLEAR"
            secondary={true}
            onClick={this.clearForm}            
            style={{marginTop:16, marginLeft:16}}
          />
        </form>
      </div> 
    );
  }
}

export default adminsettings;
