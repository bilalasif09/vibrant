import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TableHeader,
  TableHeaderColumn,
  TableRowColumn,
} from 'material-ui/Table';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Moment from 'react-moment'
import moment from 'moment'
import axios from 'axios'
import Divider from '@material-ui/core/Divider';

class Compareorders extends Component {

  constructor(props) {
    super(props);
    this.state = {
      alldata: [],
      allorders: [],
      fileheader: [],
      cancelorders: [],
      ordersToRender: [],
      indextosearch: '',
    }
    this.handleFileSubmit = this.handleFileSubmit.bind(this)
  }


  componentDidMount(){
      // fetch("http://localhost:8008/api/exceltest")
      // .then(res => res.json())
      // .then((result) => {
      //     // console.log("STATE FROM ALL CUSTOMERS:-----", result)
      //     this.setState({
      //       alldata: result.data[0].data,
      //       fileheader: result.data[0].data[0]
      //     }, () => {
      //       // console.log("DATA FROM EXCEL SHEET:-----", this.state.alldata, "\nFile Header:---", this.state.fileheader)
      //       // console.log("EXCEL SHEET HEADER:-----", this.state.alldata.data[0])
      //       fetch("http://localhost:8008/api/allorderentry")
      //       .then(res => res.json())
      //       .then((result) => {
      //           this.setState({
      //             allorders: result[0].orders
      //           }, () => {
      //             console.log("ORDERS FROM ALL ORDERS:-----", this.state.allorders)
      //             for (let i = 0; i<this.state.fileheader.length; i++){
      //               if (this.state.fileheader[i].toLowerCase() === 'account'){
      //                 // console.log("FOUND AT INDESZ:----", i, "\n", this.state.fileheader[i].toLowerCase())
      //                 this.setState({indextosearch: i}, () => {
      //                   this.searchOrders()
      //                 })
      //               }
      //             }
      //             // console.log("DATA FROM ALL ORDERS:-----", this.state.allorders)
      //             // console.log("EXCEL SHEET HEADER:-----", this.state.alldata.data[0])
      //           })
      //         },
      //         (error) => {
      //           console.log(error)
      //         }
      //       )
      //     })
      //   },
      //   (error) => {
      //     console.log(error)
      //   }
      // )

  }

  handleFileSubmit(e) {
    e.preventDefault()

    let formData = new FormData()

    console.log("Files", this.fileInput.files[0])
    let file = this.fileInput.files[0]
    console.log("Files input", file)
    
    formData.append('UploadFile', file, file.name)

    console.log("Form data", formData)

    axios.post("http://localhost:8008/api/attachment", formData)
      // .then(res => res.json())
      .then((result)=>{
        console.log("UPLOAD result", result)
        console.log("Files result", result.data)
        this.setState({
          alldata: result.data.filedata,
          fileheader: result.data.filedata[0]
        }, () => {
          // console.log("DATA FROM EXCEL SHEET:-----", this.state.alldata, "\nFile Header:---", this.state.fileheader)
          // console.log("EXCEL SHEET HEADER:-----", this.state.alldata.data[0])
          fetch("http://localhost:8008/api/allorderentry")
          .then(res => res.json())
          .then((result) => {
              this.setState({
                allorders: result[0].orders
              }, () => {
                console.log("ORDERS FROM ALL ORDERS:-----", this.state.allorders)
                for (let i = 0; i<this.state.fileheader.length; i++){
                  if (this.state.fileheader[i].toLowerCase() === 'account'){
                    // console.log("FOUND AT INDESZ:----", i, "\n", this.state.fileheader[i].toLowerCase())
                    this.setState({indextosearch: i}, () => {
                      this.searchOrders()
                      // e.target.value = null
                    })
                  }
                }
                // console.log("DATA FROM ALL ORDERS:-----", this.state.allorders)
                // console.log("EXCEL SHEET HEADER:-----", this.state.alldata.data[0])
              })
            },
            (error) => {
              console.log(error)
            }
          )
        })
      }),
      (error) => {
        console.log(error)
      }

  }

  searchOrders(){
    
    const {allorders, indextosearch, alldata} = this.state
    
    let cancelOrders = []
    let ordersToRender = []
    let fileState = null
    
    let nowDate = new Date()
    let day = nowDate.getDate()
    let month = nowDate.getMonth()+1
    let year = nowDate.getFullYear()



    let date = moment((year+'-'+month+'-'+day), 'YYYY-MM-DD').subtract(2, 'days').format()
    // console.log("JAVA SCRIPT DATE FUNCTION:--", year+'-0'+month+'-'+day)
    // console.log("MOMENT DATE:--", moment((year2+'-'+month2+'-'+day2), 'YYYY-MM-DD'))
    let date2 = moment(date, 'YYYY-MM-DD')
    
    let day2 = date2.format('DD')
    let month2 = date2.format('MM')
    let year2 = date2.format('YYYY')
    
    let installdate = year2+'-'+month2+'-'+day2

    let dateinstall = null
    let datecancel = null
    
    let repeatInstallOrder = []
    let repeatCancelOrder = []

    if (alldata[1].toString().toLowerCase().indexOf("cancel") > -1 ){
      fileState = "cancel"
    }
    if (alldata[1].toString().toLowerCase().indexOf("complete") > -1 ){
      fileState = "installed"
    }
    if (alldata[1].toString().toLowerCase().indexOf("pending") > -1 ){
      fileState = "pending"
    }

    console.log("ORDERS TO SEARCH:---", allorders, 
      "\nFILE HEADER:---", this.state.fileheader,
      "\nSEARCH FROM DOCS:---", alldata,
      "\nORDER INDEX TO LOOK:---", indextosearch,
      "\nFILE TYPE:---", fileState)
    
    if (fileState == "cancel"){
      for (let i=0; i<allorders.length; i++) {
        if (allorders[i].status != "cancel"){
          for (let j=1; j<alldata.length; j++) {
            let key = alldata[j]
            let obj = key[indextosearch]
            if (allorders[i].customeracc == obj){
              console.log("ENTRY MATCHED", allorders[i], "\tKEY FROM DATA:---", key, j)
              allorders[i].status = "cancel"
              allorders[i].installdate = null
              allorders[i].canceldate = installdate
              if (allorders[i].noofunits == 3){
                j = j+2
              }else if (allorders[i].noofunits == 2){
                j = j+1
              }
              ordersToRender.push(allorders[i])
              cancelOrders.push(allorders[i]._id)
            }
          }        
        }
      }
    }else if (fileState == "installed"){
      

      for (let i=0; i<allorders.length; i++) {
        if (allorders[i].status != "cancel"){
          for (let j=1; j<alldata.length; j++) {
            
            let key = alldata[j]
            let obj = key[indextosearch]
            
            if (allorders[i].customeracc == obj){

              let closedate = key[2].toString()
              let closekeydate = new Date(1900, 0, closedate - 1)
              closekeydate = moment(closekeydate, 'YYYY-MM-DD').format()
              let duedate = key[3].toString()
              let duekeydate = new Date(1900, 0, duedate - 1)
              duekeydate = moment(duekeydate, 'YYYY-MM-DD').format()

              console.log("ENTRY MATCHED", allorders[i], 
                          "\nKEY FROM DATA:---", key,
                          "\nCLOSE KEY DATE:---",closekeydate,
                          "\nDUE KEY DATE:---",duekeydate,
                          "\nINDEX ON EXCEL:---", j)
              // allorders[i].status = "installed"
              // allorders[i].installdate = installdate
              // allorders[i].canceldate = null
              if (allorders[i].noofunits == 3){
                j = j+2
              }else if (allorders[i].noofunits == 2){
                j = j+1
              }
              ordersToRender.push(allorders[i])
              
              if (allorders[i].status == 'installed'){
                
                let filterdate = moment(allorders[i].installdate, 'YYYY-MM-DD')
                
                console.log("TODAYS DATE:---", date2,
                           "\nORDER OWN DATE:----", filterdate,
                           "\nINSTALL DATE DATE:----", installdate,
                           "\nDATE DIFFERENCE:----", date2.diff(filterdate, 'days'),
                           )
                
                if ((date2.diff(filterdate, 'days') >= 0) && (date2.diff(filterdate, 'days') <= 30)){
                  repeatCancelOrder.push(allorders[i])
                }else{
                  repeatInstallOrder.push(allorders[i])
                }
              }else{
                cancelOrders.push(allorders[i]._id)
              }

            }
          }        
        }

      }
      console.log("REPEAT INSTALL CHARGEBACK ENTRIES:---", repeatInstallOrder,
                  "\nREPEAT CANCEL CHARGEBACK ENTRIES:---", repeatCancelOrder,)
    }else if (fileState == "pending"){
      for (let i=0; i<allorders.length; i++) {
        if (allorders[i].status != "cancel"){
          for (let j=1; j<alldata.length; j++) {
            let key = alldata[j]
            // let keydate = alldata[2]
            var keydate = new Date(1900, 0, alldata[2])
            let obj = key[indextosearch]
            if (allorders[i].customeracc == obj){
              console.log("ENTRY MATCHED", allorders[i], 
                          "\nKEY FROM DATA:---", key,
                          "\nKEY DATE:---",keydate,
                          "\nINDEX ON EXCEL:---", j)
              allorders[i].status = "pending"
              if (allorders[i].noofunits == 3){
                j = j+2
              }else if (allorders[i].noofunits == 2){
                j = j+1
              }
              ordersToRender.push(allorders[i])
              cancelOrders.push(allorders[i]._id)
            }
          }        
        }
      }
    }
    this.setState({cancelorders: cancelOrders, ordersToRender: ordersToRender}, () => {
      let install = null
      let cancel = null
      if (fileState == 'installed'){
        install = installdate
        cancel = null
      }else if (fileState == 'cancel'){
        install = null
        cancel = installdate
      }else{
        install = null
        cancel = null
      }
      console.log("ALL CANCEL ORDERS:---", this.state.cancelorders, 
                  "\nALL ORDERS TO HTML:---", this.state.ordersToRender,
                  "\nINSTALL DATE:---", install,
                  "\nCANCEL DATE:---", cancel)
      fetch("http://localhost:8008/api/updateorders", {
        method: 'post',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({updateRequests: this.state.cancelorders, 
                              status: fileState, installdate:install, 
                              canceldate: cancel})
      })
      .then(res => res.json())
      .then((result) => {
        console.log("RESULT FROM UPDATING USERS:-----", result)
        // this.ref.fileInput.value = null
        // this.getAllRequests()
      }),(error) => {
        console.log(err)
      }

      fetch("http://localhost:8008/api/setcancelchargeback", {
        method: 'post',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({updateRequests: repeatCancelOrder, 
                              status: "cancel", 
                              canceldate: installdate})
      })
      .then(res => res.json())
      .then((result) => {
        console.log("RESULT FROM UPDATING CANCEL CHARGEBACK ORDERS:-----", result)
        // this.ref.fileInput.value = null
        // this.getAllRequests()
      }),(error) => {
        console.log(err)
      }


      fetch("http://localhost:8008/api/setinstallchargeback", {
        method: 'post',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({updateRequests: repeatInstallOrder, 
                              status: fileState, 
                              canceldate: installdate})
      })
      .then(res => res.json())
      .then((result) => {
        console.log("RESULT FROM UPDATING INSTALL CHARGEBACK ORDERS:-----", result)
        // this.ref.fileInput.value = null
        // this.getAllRequests()
      }),(error) => {
        console.log(err)
      }



    })
  }


  render() {
    return (
      <div>
        Compare Orders
        <Divider style={{marginTop: 20, marginBottom: 20, }}/>
        <form onSubmit={this.handleFileSubmit}>
          <label>
                Upload file:
                <input
                  type="file"
                  ref={input => {
                    this.fileInput = input;
                  }}
                />
              </label>
          <button type="submit"> Submit </button>
        </form>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Date</TableCell>

              <TableCell>Name</TableCell>
              <TableCell>Address</TableCell>
              <TableCell>Number</TableCell>
              <TableCell>State</TableCell>
              <TableCell>Zip</TableCell>
              <TableCell>City</TableCell>
              <TableCell>Account</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.ordersToRender.map((n, i) => {
              return (
                <TableRow key={i}>
                  <TableCell component="th" scope="row"><Moment format="YYYY/MM/DD">{n.created_at}</Moment></TableCell>
                  <TableCell component="th" scope="row">
                    {n.customername}
                  </TableCell>

                  <TableCell component="th" scope="row">{n.address}</TableCell>
                  <TableCell component="th" scope="row">{n.customernumber}</TableCell>
                  <TableCell component="th" scope="row">{n.state}</TableCell>
                  <TableCell component="th" scope="row">{n.zip}</TableCell>
                  <TableCell component="th" scope="row">{n.city}</TableCell>
                  <TableCell component="th" scope="row">{n.customeracc}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>

      </div>
    );
  }
}

export default Compareorders;
