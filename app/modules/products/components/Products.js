import React, { Component, PropTypes } from 'react';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
  customWidth: {
    width: '100%',
  },
};


class Products extends Component {

  constructor(props) {
      super(props);
      this.state = {
        product: null,
        price: '',
      }
      this.productChange = this.productChange.bind(this)
      this.priceChange = this.priceChange.bind(this)
  }

  productChange(event, index, value){
    this.setState({
      product: value
    })
  }

  priceChange(event){
    this.setState({
      price: event.target.value
    })
  }

  render() {
    
    const { product, price } = this.state
    
    return (
      <div>
        <h3>Product Settings</h3>
        <SelectField
            floatingLabelText="Select Product"
            value={product}
            onChange={this.productChange}
            style={styles.customWidth}
          >
            <MenuItem value={null} primaryText="" />
            <MenuItem value={'tv'} primaryText="TV" />
            <MenuItem value={'hsi'} primaryText="HIGH SPEED INTERNET" />
            <MenuItem value={'phone'} primaryText="PHONE" />
        </SelectField>
        <TextField
          id="price"
          type="number"
          floatingLabelText="Price"
          onChange={this.priceChange}
          value={price}
          style={styles.customWidth}
          required
        />
      </div>
    );
  }
}

export default Products;
