import React, { Component, PropTypes } from 'react';
import {withRouter} from 'react-router-dom'
import {AppTemplate} from '../../../commons/ui/components/AppTemplate';
import Link from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';
import Toggle from 'material-ui/Toggle';
import Divider from 'material-ui/Divider';
import moment from 'moment';

const checkboxStyles = {
  block: {
    maxWidth: '100%',
  },
  radioButton: {
    marginBottom: 16,
    marginTop: 16,
  },
};

const styles = {
  customWidth: {
    width: '100%',
  },
};

const phoneInputStyle = {
  width: '50%',
}

const addressStyle = {
  width: '33.3%',
}



class Orderentry extends Component {

  constructor(props) {
      super(props);
      const minDate = new Date();
      this.state = {
        minDate: minDate,
        repfirstname: '',
        allreps: [],
        vendorname: '',
        customername: '',
        customernumber: '',
        customeracc: '',
        internet: false,
        tv: false,
        phone: false,
        trippleplay: false,
        doubleplay: false,
        singleplay: false,
        repflag: false,
        noofunits: '',
        orderref: '',
        priceperunit: '',
        address: '',
        city: '',
        state: '',
        zip: '',
        rep: '',
        managerid: '',
        submitid: '',
      }
      this.handleSubmit = this.handleSubmit.bind(this)
      this.handlerepchange = this.handlerepchange.bind(this)
      this.handlerepnamechange = this.handlerepnamechange.bind(this)
      this.handlevendorchange = this.handlevendorchange.bind(this)
      this.clearForm = this.clearForm.bind(this)
      this.loggedUser = JSON.parse(localStorage.getItem('loggeduser'))
  }

  componentDidMount(){
    console.log("Logged in user:----", this.loggedUser)
    let day = this.state.minDate.getDate()
    let month = this.state.minDate.getMonth()
    let year = this.state.minDate.getFullYear()
    let fullDate = null
    if (month+1 < 10){
      fullDate = year+'-0'+(month+1)+'-'+day
    }else{
      fullDate = year+'-'+(month+1)+'-'+day
    }
    let created_at = moment(fullDate, 'YYYY-MM-DD').format();
    console.log("DATE/TIME STAMP:-----", fullDate,"\n", created_at,"\n", this.state.minDate, "\n", moment().week())
    if (this.loggedUser.type === 'Manager'){
      fetch("http://localhost:8008/api/managerreps", {
      method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({manager: this.loggedUser._id})
      })
      .then(res => res.json())
      .then((result)=>{
        // console.log("ALL MANAGER REPS AVAILABLE:----", result[0].users)
        this.setState({
          allreps: result[0].users
        }, () => {
          console.log("ALL MANAGER REPS AVAILABLE:------", this.state.allreps)
        })
      }),
      (error) => {
        console.log(error)
      }
    }


  }

  clearForm(){
    const minDate = new Date()
    this.setState({
      minDate: minDate,
      repfirstname: '',
      vendorname: '',
      customername: '',
      customernumber: '',
      customeracc: '',
      internet: false,
      tv: false,
      phone: false,
      trippleplay: false,
      doubleplay: false,
      singleplay: false,
      repflag:false,
      address: '',
      city: '',
      state: '',
      zip: '',
      rep: '',
      noofunits: '',
      priceperunit: '',
      _person: null,
      orderref: '',
      submitid: '',
    })
  }



  handlerepchange(event, index, value){
    this.setState({rep: value}, () => {
      console.log("REP ID FROM SELECT:----", this.state.rep)
      if (this.state.rep !== ''){
        this.setState({repflag:true}, () => {
          console.log("REP FLAG TRUE:----", this.state.repflag)
          
        })
      }else{
        this.setState({repflag:false}, () => {
          console.log("REP FLAG FALSE:----", this.state.repflag)
          
        })
      }
    });

  }

  handlerepnamechange(event){
    this.setState({repfirstname: event.target.value});

  }

  handlevendorchange(event){
    let name = event.target.name
    this.setState({[name]: event.target.value});

  }


  updatetvchecked(){
     this.setState((oldState) => {
      return {
        tv: !oldState.tv,
      };
    });

  }

  updateinternetchecked(){
     this.setState((oldState) => {
      return {
        internet: !oldState.internet,
      };
    });

  }

  updatephonechecked(){
     this.setState((oldState) => {
      return {
        phone: !oldState.phone,
      };
    });

  }

  prepareData(){
    if (this.state.tv === true && 
      this.state.phone === true && 
      this.state.internet === true){
      this.state.trippleplay = true
      this.state.doubleplay = false
      this.state.singleplay = false
      this.state.noofunits = 3
      console.log("SALE DESCRIPTION:----\n", "TRIPPLE PLAY:----", this.state.trippleplay, this.state.noofunits)
    }else if(this.state.tv === true && 
             this.state.phone === true){
      this.state.doubleplay = true
      this.state.singleplay = false
      this.state.trippleplay = false
      this.state.noofunits = 2
      console.log("SALE DESCRIPTION:----\n", "DOUBLE PLAY:----", this.state.doubleplay, this.state.noofunits)
    }else if (this.state.tv === true && 
              this.state.internet === true){
      this.state.doubleplay = true
      this.state.singleplay = false
      this.state.trippleplay = false
      this.state.noofunits = 2
      console.log("SALE DESCRIPTION:----\n", "DOUBLE PLAY:----", this.state.doubleplay, this.state.noofunits)
    }else if (this.state.phone === true && 
              this.state.internet === true){
      this.state.doubleplay = true
      this.state.singleplay = false
      this.state.trippleplay = false
      this.state.noofunits = 2
      console.log("SALE DESCRIPTION:----\n", "DOUBLE PLAY:----", this.state.doubleplay, this.state.noofunits)
    }else if (this.state.tv === true && 
              this.state.internet === false && 
              this.state.phone === false){
      this.state.doubleplay = false
      this.state.singleplay = true
      this.state.trippleplay = false
      this.state.noofunits = 1
      console.log("SALE DESCRIPTION:----\n", "SINGLE PLAY:----", this.state.singleplay, this.state.noofunits)
    }else if (this.state.tv === false && 
              this.state.internet === true && 
              this.state.phone === false){
      this.state.doubleplay = false
      this.state.singleplay = true
      this.state.trippleplay = false
      this.state.noofunits = 1
      console.log("SALE DESCRIPTION:----\n", "SINGLE PLAY:----", this.state.singleplay, this.state.noofunits)
    }else if (this.state.tv === false && 
              this.state.internet === false && 
              this.state.phone === true){
      this.state.doubleplay = false
      this.state.singleplay = true
      this.state.trippleplay = false
      this.state.noofunits = 1
      console.log("SALE DESCRIPTION:----\n", "SINGLE PLAY:----", this.state.singleplay, this.state.noofunits)
    }


    this.customer = {
      name: this.state.customername,
      number: this.state.customernumber,
      account: this.state.customeracc,
      address: this.state.address,
      city: this.state.city,
      state: this.state.state,
      zip: this.state.zip,
    }
    console.log("CUSTOMER INFO:-----", this.customer)
    
    let craete_date = moment().format('YYYY-MM-DD');
    console.log("CREATE DATE:-----", craete_date)

    let day = this.state.minDate.getDate()
    let month = this.state.minDate.getMonth()
    let year = this.state.minDate.getFullYear()
    let fullDate = null
    if (month+1 < 10){
      fullDate = year+'-0'+(month+1)+'-'+day
    }else{
      fullDate = year+'-'+(month+1)+'-'+day
    }
    let created_at = moment(fullDate, 'YYYY-MM-DD').format();
    console.log("DATE/TIME STAMP:-----", fullDate, created_at, this.state.minDate)

    fetch("http://localhost:8008/api/addorderentry", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({repid: this.state.rep,
                              managerid: this.state.managerid,
                              submitid: this.state.submitid,
                              repfirstname: this.state.repfirstname,
                              vendorname: this.state.vendorname,
                              customername: this.state.customername,
                              customernumber: this.state.customernumber,
                              customeracc: this.state.customeracc,
                              address: this.state.address,
                              city: this.state.city,
                              state: this.state.state,
                              zip: this.state.zip,
                              tv: this.state.tv,
                              internet: this.state.internet,
                              phone: this.state.phone,
                              date: created_at,
                              trippleplay: this.state.trippleplay,
                              doubleplay: this.state.doubleplay,
                              singleplay: this.state.singleplay,
                              filterdate: fullDate,
                              person: this.state._person,
                              noofunits: this.state.noofunits})
    })
    .then(res => res.json())
    .then((result)=>{
      if (result.length != 0){
        console.log(result)
        console.log("ORDER ID TO STORE:---", result[0].result._id)
        fetch("http://localhost:8008/api/addcustomer", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            body: JSON.stringify({name: this.state.customername,
                                  number: this.state.customernumber,
                                  account: this.state.customeracc,
                                  address: this.state.address,
                                  city: this.state.city,
                                  state: this.state.state,
                                  zip: this.state.zip,
                                  orderref: result[0].result._id})
        })
        .then(res => res.json())
        .then((result)=>{
          console.log(result)
          this.clearForm()
        }),
        (error) => {
          console.log(error)
        }    
      }
    }),
    (error) => {
      console.log(error)
    }
  }

  handleSubmit(e){
    e.preventDefault()
    console.log("REP FLAG", this.state.repflag)
    let _person = null
    if (this.loggedUser.type === 'Manager'){
      if (this.state.repflag === false){
        this.setState({rep: this.loggedUser._id, managerid: this.loggedUser._id, _person: 'man', submitid: this.loggedUser._id}, () => {
              
            console.log("ORDER TYPE:---", this.state.value, "\n",
                "REP ID:---", this.state.rep, "\n",
                "REP FIRST NAME:---", this.state.repfirstname, "\n",
                "VENDOR NAME:---", this.state.vendorname, "\n",
                "CUSTOMER NAME:---", this.state.customername, "\n",
                "CUSTOMER NUMBER:---", this.state.customernumber, "\n",
                "CUSTOMER ACCOUNT NUMBER:---", this.state.customeracc, "\n",
                "ADDRESS:---", this.state.address, "\n",
                "CITY:---", this.state.city, "\n",
                "STATE:---", this.state.state, "\n",
                "XIP:---", this.state.zip, "\n",
                "TV:---", this.state.tv, "\n",
                "INTERNET:---", this.state.internet, "\n",
                "PHONE:---", this.state.phone, "\n",
                "DATE:---", this.state.minDate, "\n",
                "Manager ID:---", this.state.managerid, "\n",)
            this.prepareData()
            // let craete_date = moment().format('YYYY-MM-DD');
            // console.log("CREATE DATE:-----", craete_date)
        })
      }
      if (this.state.repflag === true){
        this.setState({managerid: this.loggedUser._id, _person: 'man', submitid: this.loggedUser._id}, () => {
              console.log("ORDER TYPE:---", this.state.value, "\n",
                "REP ID:---", this.state.rep, "\n",
                "REP FIRST NAME:---", this.state.repfirstname, "\n",
                "VENDOR NAME:---", this.state.vendorname, "\n",
                "CUSTOMER NAME:---", this.state.customername, "\n",
                "CUSTOMER NUMBER:---", this.state.customernumber, "\n",
                "CUSTOMER ACCOUNT NUMBER:---", this.state.customeracc, "\n",
                "ADDRESS:---", this.state.address, "\n",
                "CITY:---", this.state.city, "\n",
                "STATE:---", this.state.state, "\n",
                "XIP:---", this.state.zip, "\n",
                "TV:---", this.state.tv, "\n",
                "INTERNET:---", this.state.internet, "\n",
                "PHONE:---", this.state.phone, "\n",
                "DATE:---", this.state.minDate, "\n",
                "Manager ID:---", this.state.managerid, "\n",)
                this.prepareData()
                // let craete_date = moment().format('YYYY-MM-DD');
                // console.log("CREATE DATE:-----", craete_date)
        })
      }
    }
    if (this.loggedUser.type === 'Representative'){
      this.setState({rep: this.loggedUser._id, managerid: this.loggedUser.manager, _person: 'rep', submitid: this.loggedUser._id}, () => {
          console.log("ORDER TYPE:---", this.state.value, "\n",
                "REP ID:---", this.state.rep, "\n",
                "REP FIRST NAME:---", this.state.repfirstname, "\n",
                "VENDOR NAME:---", this.state.vendorname, "\n",
                "CUSTOMER NAME:---", this.state.customername, "\n",
                "CUSTOMER NUMBER:---", this.state.customernumber, "\n",
                "CUSTOMER ACCOUNT NUMBER:---", this.state.customeracc, "\n",
                "ADDRESS:---", this.state.address, "\n",
                "CITY:---", this.state.city, "\n",
                "STATE:---", this.state.state, "\n",
                "XIP:---", this.state.zip, "\n",
                "TV:---", this.state.tv, "\n",
                "INTERNET:---", this.state.internet, "\n",
                "PHONE:---", this.state.phone, "\n",
                "DATE:---", this.state.minDate, "\n",
                "Manager ID:---", this.state.managerid, "\n",)
                this.prepareData()
                // let craete_date = moment().format('YYYY-MM-DD');
                // console.log("CREATE DATE:-----", craete_date)

      })
    }



  }

  render() {
    const { value, rep, repfirstname, vendorname, address, city, state, zip, customername, customernumber, customeracc } = this.state
    return (
      <div>
        <h4>Order Entry Form</h4>
        <form onSubmit={this.handleSubmit}>
          <DatePicker
            autoOk={false}
            floatingLabelText="Date"
            defaultDate={this.state.minDate}
            disableYearSelection={false}
            disabled={true}
          />

          {(this.loggedUser.type === 'Manager') ? 
          <div>
            <SelectField
              floatingLabelText="Rep. ID"
              value={rep}
              onChange={this.handlerepchange}
              style={phoneInputStyle}
              id="repid"
            >
              <MenuItem value={''} />
              {this.state.allreps.map((obj, i) => {
                return (obj.contracted ? <MenuItem key={i} value={obj._id} primaryText={obj.username} /> : '')
              })}
            </SelectField>
            <TextField
              id="repfirstname"
              floatingLabelText="Rep. First Name"
              onChange={this.handlerepnamechange}
              value={repfirstname}
              style={phoneInputStyle}
            />
          </div> : ''}
          <TextField
            id="vendorname"
            name="vendorname"
            floatingLabelText="Vendor Name"
            onChange={this.handlevendorchange}
            value={vendorname}
            style={phoneInputStyle}
            required
          />
          <TextField
            id="customername"
            name="customername"
            floatingLabelText="Customer Name"
            onChange={this.handlevendorchange}
            value={customername}
            style={phoneInputStyle}
            required
          />
          <TextField
            id="customernumber"
            name="customernumber"
            floatingLabelText="Customer Number"
            onChange={this.handlevendorchange}
            value={customernumber}
            style={phoneInputStyle}
            required
          />
          <TextField
            id="customeracc"
            name="customeracc"
            floatingLabelText="Customer Account Number"
            onChange={this.handlevendorchange}
            value={customeracc}
            style={styles.phoneInputStyle}
            required
          />


          <TextField
            id="address"
            name="address"
            floatingLabelText="Physical Address"
            onChange={this.handlevendorchange}
            value={address}
            style={styles.customWidth}
            required
          />
          <TextField
            id="city"
            name="city"
            floatingLabelText="City"
            onChange={this.handlevendorchange}
            value={city}
            style={addressStyle}
            required
          />
          <TextField
            id="state"
            name="state"
            floatingLabelText="State"
            onChange={this.handlevendorchange}
            value={state}
            style={addressStyle}
            required
          />
          <TextField
            id="zip"
            name="zip"
            floatingLabelText="Zip"
            onChange={this.handlevendorchange}
            value={zip}
            style={addressStyle}
            required
          />

          <h4>Order Description</h4>
          <Divider />
          <div style={checkboxStyles.block}>
            <Checkbox
              label="High Speed Internet"
              checked={this.state.internet}
              onCheck={this.updateinternetchecked.bind(this)}
              style={checkboxStyles.radioButton}
            />
            <Checkbox
              label="TV"
              checked={this.state.tv}
              onCheck={this.updatetvchecked.bind(this)}
              style={checkboxStyles.radioButton}
            />
            <Checkbox
              label="Phone"
              checked={this.state.phone}
              onCheck={this.updatephonechecked.bind(this)}
              style={checkboxStyles.radioButton}
            />

          </div>
          <Divider />
          <RaisedButton 
            label="SUBMIT"
            type="submit"
            primary={true}
            style={{marginTop:16}}

          />
          <RaisedButton 
            label="CLEAR"
            secondary={true}
            onClick={this.clearForm}            
            style={{marginTop:16, marginLeft:16}}
          />
        </form>
      </div>
    );
  }

}

export default withRouter(Orderentry);
