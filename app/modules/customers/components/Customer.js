import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TableHeader,
  TableHeaderColumn,
  TableRowColumn,
} from 'material-ui/Table';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Moment from 'react-moment'

const styles = {
  root: {
    width: '100%',
    marginTop: 16,
    overflowX: 'auto',
  },
  table: {
    minWidth: '100%',
  },
}


class Customer extends Component {

    constructor(props) {
        super(props);
        this.state = {
          allcustomers: [],
        }
    }

    componentDidMount(){

      fetch("http://localhost:8008/api/allcustomers")
      .then(res => res.json())
      .then((result) => {
            console.log("STATE FROM ALL CUSTOMERS:-----", result)
            this.setState({
              allcustomers: result[0].customers
            }, () => {
              console.log("STATE FROM ALL CUSTOMERS:-----", this.state.allcustomers)
            })
        },
        (error) => {
          console.log(error)
        }
      )
    }

    render() {
      return (
        <div>
          <h3>All Customers</h3> 


          <Table style={styles.root}>
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Address</TableCell>
                <TableCell>Number</TableCell>
                <TableCell>State</TableCell>
                <TableCell>Zip</TableCell>
                <TableCell>City</TableCell>
                <TableCell>Account</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.allcustomers.map(n => {
                return (
                  <TableRow key={n._id}>
                    <TableCell component="th" scope="row"><Moment format="YYYY/MM/DD">{n.created_at}</Moment></TableCell>
                    <TableCell component="th" scope="row">
                      {n.name}
                    </TableCell>
                    <TableCell component="th" scope="row">{n.address}</TableCell>
                    <TableCell component="th" scope="row">{n.number}</TableCell>
                    <TableCell component="th" scope="row">{n.state}</TableCell>
                    <TableCell component="th" scope="row">{n.zip}</TableCell>
                    <TableCell component="th" scope="row">{n.city}</TableCell>
                    <TableCell component="th" scope="row">{n.account}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </div>
      );
    }
}

export default Customer;
