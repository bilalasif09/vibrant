import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import AddBtn from 'material-ui/svg-icons/action/done';

export default class Addcampaign extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      name: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    // let name = event.target.name
    var name = event.target.name
    this.setState({
      [name]: event.target.value
    });
  };


  handleSubmit(event) {
    event.preventDefault()
    fetch("http://localhost:8008/api/addcampaign", {
      method: 'post',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({name: this.state.name})
    })
    .then(res => res.json())
    .then((result) => {
      console.log("RESULT FROM ADDING CAMPAIGN:---", result)
      this.setState({
        name: '',
      })
    }),(error) => {
      console.log(err)
    }
  };



  render() {
    return (
      <div>
        <h2>ADD CAMPAIGN</h2>
        <form onSubmit={this.handleSubmit}>
          <TextField
          	id="name"
          	floatingLabelText="Name"
            name="name"
          	value={this.state.name}
          	onChange={this.handleChange}
          />
          <RaisedButton
          	label="ADD"
    		    labelPosition="before"
            type="submit"
       		  primary={true}
        		icon={<AddBtn />}
      	  />
        </form>
      </div>
    );
  }
}
