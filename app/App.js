import React, { Component } from 'react';
import { render } from 'react-dom';
import { withRouter, BrowserRouter as Router, Route, Switch, Link, NavLink, Provider, store } from "react-router-dom";
import { HashRouter } from "react-router";
import AppTemplate from './commons/ui/components/AppTemplate';
import LoggedOutTemplate from './commons/ui/components/LoggedOutTemplate';
import AsyncComponent from './AsyncComponent';
import basePath from './commons/utils/basePath';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MiniDrawer from './modules/appdrawer/components/Appdrawer'
import "./style/app.scss";

const home = () => import(/* webpackChunkName: "home" */ './modules/home/index');
const projects = () => import(/* webpackChunkName: "projects" */ './modules/projects/index');
const contact = () => import(/* webpackChunkName: "contact" */ './modules/contact/index');
const login = () => import(/* webpackChunkName: "login" */ './modules/login/index');
const dashboard = () => import(/* webpackChunkName: "dashboard" */ './modules/dashboard/index');
const userdashboard = () => import(/* webpackChunkName: "userdashboard" */ './modules/userdashboard/index');
const register = () => import(/* webpackChunkName: "register" */ './modules/register/index');
const addvendor = () => import(/* webpackChunkName: "addvendor" */ './modules/addvendor/index');
const addcampaign = () => import(/* webpackChunkName: "addcampaign" */ './modules/addcampaign/index');
const allregisters = () => import(/* webpackChunkName: "allregisters" */ './modules/allregisters/index');
const adduser = () => import(/* webpackChunkName: "adduser" */ './modules/adduser/index');
const allusers = () => import(/* webpackChunkName: "allusers" */ './modules/allusers/index');
const allorders = () => import(/* webpackChunkName: "allorders" */ './modules/allorders/index');
const newhire = () => import(/* webpackChunkName: "newhire" */ './modules/newhire/index');
const orderentry = () => import(/* webpackChunkName: "orderentry" */ './modules/orderentry/index');
const products = () => import(/* webpackChunkName: "products" */ './modules/products/index');
const ordertype = () => import(/* webpackChunkName: "ordertype" */ './modules/ordertype/index');
const customers = () => import(/* webpackChunkName: "allcustomers" */ './modules/customers/index');
const team = () => import(/* webpackChunkName: "team" */ './modules/team/index');
const compareorders = () => import(/* webpackChunkName: "compareorders" */ './modules/compareorders/index');
const uploads = () => import(/* webpackChunkName: "uploads" */ './modules/uploads/index');
const adminsettings = () => import(/* webpackChunkName: "adminsettings" */ './modules/adminsettings/index');
const comissions = () => import(/* webpackChunkName: "comissions" */ './modules/comissions/index');
const minidrawer = () => import(/* webpackChunkName: "minidrawer" */ './modules/appdrawer/index');

class App extends Component {

    constructor(props) {
      super(props);


      this.loggedOutMenu = [
        { title: 'Login', link: '/login', id: 0 },
        { title: 'Register', link: '/register', id: 1 }
      ];
      this.loggedUserNav = []

      if (localStorage.getItem('navbar'))
        this.loggedUserNav = JSON.parse(localStorage.getItem('navbar'))


      console.log("NAV--", this.loggedUserNav)

      this.loggedAdminNav = []

      this.userId = localStorage.getItem('userId');
      this.loggedUser = JSON.parse(localStorage.getItem('loggeduser'))
      console.log("LOGGED USER:---", this.loggedUser)
      this.state = { menuNode : [] };

      this.changeDetect = this.changeDetect.bind(this);

    }

    componentWillMount() {
      this.setState({ menuNode: (this.userId ? this.loggedUserNav : this.loggedOutMenu) })
    }

    changeDetect() {

      console.log("Change Detect App.js", this.state.menuNode);

      this.userId = localStorage.getItem('userId');
      if (this.userId) {
        fetch("http://localhost:8008/api/getnavbar")
        .then(res => res.json())
        .then((result) => {

            let loggedUser = JSON.parse(localStorage.getItem('loggeduser'))
            let navbar = []
            console.log("NAV BAR AVAILABLE:-----", result)
            for (let obj of result[0].navbar){
              // if (loggedUser.role === 'user'){
                if (obj.role === 'user' && loggedUser.role === 'user'){
                  navbar.push(obj)
                }
                else if (obj.role === 'admin' && loggedUser.role === 'admin') {
                  navbar.push(obj)
                }
              // }
            }
            if (loggedUser.type === 'Manager'){
              let navItem = {"title":"Team Orders","link":"/team","role":"user", "icon":"ti-files", "id":2}
              navbar.push(navItem)
            }
            localStorage.setItem("navbar", JSON.stringify(navbar))
            this.setState({ menuNode: (this.userId ? navbar : this.loggedOutMenu) });

          },
          (error) => {
            console.log(error)
          }
        )
      } else {
        this.setState({ menuNode: this.loggedOutMenu });
      }



    }

    setNav() {



    }
    appTemplate() {

      return <div className="flex-container">
        <nav>
        <div className="nav">
        <div className="logo">
          <img src="http://www.vibrantdirectsales.com/wp-content/uploads/2017/11/75.png" />
        </div>
          {
            this.state.menuNode.map((menu, i) => <NavLink exact key={menu.id} activeClassName="active" data-id={i} to={menu.link}> {menu.title} </NavLink>)

          }
          </div>
        </nav>
        <div className="container">
          <div className="card">

            <Route path={basePath``} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={login} />} />
            <Route path={basePath`login`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={login} />} />
            <Route path={basePath`home`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={home} />} />
            <Route path={basePath`projects`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={projects} />} />
            <Route path={basePath`contact`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={contact} />} />
            <Route path={basePath`orderentry`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={orderentry} />} />
            <Route path={basePath`register`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={register} />} />

            <Route path={basePath`ordertype`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={ordertype} />} />
            <Route path={basePath`dashboard`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={dashboard} />} />
            <Route path={basePath`userdashboard`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={userdashboard} />} />
            <Route path={basePath`products`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={products} />} />
            <Route path={basePath`customers`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={customers} />} />
            <Route path={basePath`team`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={team} />} />
            <Route path={basePath`compareorders`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={compareorders} />} />
            <Route path={basePath`adminsettings`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={adminsettings} />} />
            <Route path={basePath`comissions`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={comissions} />} />
            <Route path={basePath`minidrawer`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={minidrawer} />} />

            <Route path={basePath`addvendor`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={addvendor} />} />
            <Route path={basePath`addcampaign`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect}moduleProvider={addcampaign} />} />
            <Route path={basePath`allregisters`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={allregisters} />} />
            <Route path={basePath`allorders`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={allorders} />} />
            <Route path={basePath`adduser`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={adduser} />} />
            <Route path={basePath`allusers`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={allusers} />} />
            <Route path={basePath`newhire`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={newhire} />} />
            <Route path={basePath`uploads`} exact={true} component={() => <AsyncComponent changeDetect={this.changeDetect} moduleProvider={uploads} />} />

          </div>
        </div>
        <hr/>
        <footer>
          <span><b>Created by Eyetea Corporation</b></span>
          <a href="http://github.com/rubenspgcavalcante/react-webpack-lazy-loading">All Rights Reserved</a>
        </footer>
      </div>
    }

  render(){
    console.log("Render App")
    return(
      <MuiThemeProvider>
            <Router>
         { this.appTemplate() }
            </Router>

      </MuiThemeProvider>
    );
  }
}

render(<App />, document.getElementById("app"))

export default withRouter(App);
