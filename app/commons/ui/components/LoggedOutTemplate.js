import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import basePath from 'commons/utils/basePath';

export default class loggedOutTemplate extends Component {

  constructor(props) {
    super(props);
    
    this.loggedInMenu = [
      { title: 'Home', link: '', id: 0 }, 
      { title: 'Projects', link: '/projects', id: 1 }, 
      { title: 'Contact', link: '/contact', id: 2 }, 
      { title: 'Add Vendor', link: '/addvendor', id: 3 },
      { title: 'All Registers', link: '/allregisters', id: 4 },
      { title: 'New Hire', link: '/newhire', id: 5 },
      { title: 'All Users', link: '/allusers', id: 6 }
    ];
      
    this.loggedOutMenu = [
      { title: 'Login', link: '/login', id: 0 }, 
      { title: 'Register', link: '/register', id: 1 }
    ];
    
    this.menuNode = this.loggedOutMenu;
    
    this.userId = localStorage.getItem('userId')
    console.log("AppTemplate Component userId", this.userId) 
    // if (this.userId) {
    //   this.menuNode = this.loggedInMenu;
    //   console.log("loggedInMenu hi")
    // }
    // else {
    //   this.menuNode = this.loggedOutMenu;
    //   console.log("loggedOutMenu here")
    // }
  }

  render() {
    return (
      <div className="flex-container">
        <nav>
          {this.menuNode.map((menu) => <Link key={menu.id} to={menu.link}> {menu.title} </Link>)}
        </nav>
        <div className="container">
          <div className="card">
            {this.props.children}
          </div>
        </div>
        <hr/>
        <footer>
          <span><b>Created by Eyetea Corporation</b></span>
          <a href="http://github.com/rubenspgcavalcante/react-webpack-lazy-loading">All Rights Reserved</a>
        </footer>
      </div>
    );
  }
}

