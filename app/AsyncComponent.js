import React, { PureComponent } from 'react';
import LoadingIndicator from 'commons/ui/components/LoadingIndicator';

export default class extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      Component: null
    }
    this.changeDetect = this.changeDetect.bind(this);
  }

  componentDidMount() {
    if(!this.state.Component) {
      this.props.moduleProvider().then( ({Component}) => this.setState({ Component }));
    }
  }

  changeDetect() {
    console.log("Change detected Async");
    this.props.changeDetect();
  }



  render() {
    const { Component } = this.state;

    //The magic happens here!
    return (
      <div className="con">
        {Component ? <Component changeDetect={this.changeDetect} /> : <LoadingIndicator />}
      </div>
    );
  }
};
